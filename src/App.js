import React, { Component } from "react";
import { HashRouter,BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "./App.scss";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Containers
const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

// Pages
const Login = React.lazy(() => import("./views/Pages/Login"));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route
              exact
              path="/login"
              name="Login Page"
              render={props =>
                localStorage.getItem("token") ? (
                  <Redirect to="/" />
                ) : (
                  <Login {...props} />
                )
              }
            />

            <Route
              path="/"
              name="Home"
              render={props =>
                localStorage.getItem("token") ? (
                  <DefaultLayout {...props} />
                ) : (
                  <Redirect to="/login" />
                )
              }
            />

            {/* <Route
              path="/"
              name="Home"
              render={props => <DefaultLayout {...props} />}
            /> */}
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
