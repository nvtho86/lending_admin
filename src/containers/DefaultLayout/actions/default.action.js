import * as types from '../constants/ActionTypes';
import apiCaller from '../../../_helpers/apiCaller';

export const getCategoryFilter = () => {
    return (dispatch) => {
        return apiCaller('customer', 'customer_filter_category', 'POST',
            JSON.stringify(
                {}
            )
        ).then(response => {
            if (response && response.data) {
                const { data } = response
                console.log(response)
                if (data && data.code === 200) 
                    dispatch(fetchCategoryFilter(data.payload))
                return {success: true}
            }
            else
                return {success: false}
        })
    }
}

export const getOperatorsCompare = () => {
    return (dispatch) => {
        return apiCaller('customer', 'compare_type', 'POST',
            JSON.stringify(
                {}
            )
        ).then(response => {
            if (response && response.data) {
                const { data } = response
                if (data.code === 200)
                    dispatch(fetchOperatorsCompare(data.payload))
                return {success: true}
            }
            else
                return {success: false}
        })
    }
}

export const fetchCategoryFilter = data => {
    return {
      type: types.CATEGORY_FILTER,
      data
    }
}

export const fetchOperatorsCompare = data => {
    return {
      type: types.COMPARE_TYPE,
      data
    }
}