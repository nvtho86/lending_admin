import * as types from '../constants/ActionTypes';

const initialState = {
  customerFilterCategory: [],
  compareType: []
};

var defaultData = (state = initialState, action) => {
  switch (action.type) {
    case types.CATEGORY_FILTER:
        return {
            ...state,
            customerFilterCategory: action.data
        }
    case types.COMPARE_TYPE:
        return {
            ...state,
            compareType: action.data
        }
    default:
      return state;
  }
}
export default defaultData;
