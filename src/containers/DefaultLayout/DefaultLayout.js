import React, { Component, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import * as router from "react-router-dom";
import { Container } from "reactstrap";
import { ApiProvider } from "../../ContextAPI/contextAPI";

import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav
} from "@coreui/react";
// sidebar nav config
import navigation from "../../_nav";
// routes config
import routes from "../../routes";
import * as actions from "./actions/default.action";
import { connect } from "react-redux";

const DefaultAside = React.lazy(() => import("./DefaultAside"));
const DefaultFooter = React.lazy(() => import("./DefaultFooter"));
const DefaultHeader = React.lazy(() => import("./DefaultHeader"));

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datasArr: [],
      stateNewUpdate: ""
    };
  }

  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  signOut(e) {
    e.preventDefault();
    localStorage.removeItem("token");
    localStorage.removeItem("idUser");
    localStorage.removeItem("name");
    this.props.history.push("/login");
  }

  componentDidMount() {
    this.props.onLoadDataDefault();
  }

  // handleTypesOfIncentives = datas => {
  //   this.setState({ datasTypesOfIncentives: datas });
  // };

  handleDatasArr = (datas, stateNewUpdate) => {
    this.setState({ datasArr: datas, stateNewUpdate });
  };

  render() {
    const objContextApi = {
      stateNewUpdate: this.state.stateNewUpdate,
      datasArr: this.state.datasArr,
      handleDatasArr: this.handleDatasArr
    };

    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader onLogout={e => this.signOut(e)} />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav
                navConfig={navigation}
                {...this.props}
                router={router}
              />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} router={router} />
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <ApiProvider value={objContextApi}>
                            <route.component {...props} />
                          </ApiProvider>
                        )}
                      />
                    ) : null;
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </Suspense>
            </Container>
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    defaultData: state.defaultData
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onLoadDataDefault: () => {
      dispatch(actions.getCategoryFilter());
      dispatch(actions.getOperatorsCompare());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultLayout);
