import * as types from './../constants/ActionTypes';


var findIndex = (users, id) => {
  var result = -1;
  users.users.forEach((user, index) => {
    if (user.id === id) {
      result = index
    }
  })
  return result;
}

var initialState =
{
  users: [],
  keyword: '',
  roleId: '',
  department: '',
  modal: false,
  selectUser: undefined,
  cursor:''
}
  ;

var userStaff = (state = initialState, action) => {
  switch (action.type) {
    case types.LIST_USER:
      return { ...state, 
        users: action.users,
        cursor:action.cursor 
      }
    case types.UPDATE_USER:
      let _users = state.users.map(item => {
        if (item.id === action.user.id) return action.user;
        else return item;
      });
      return { ...state, users: _users, selectUser: undefined, modal: !state.modal };
    case types.EDIT_USER:
      return { ...state, selectUser: action.user, modal: !state.modal }
    case types.CLOSE_MODAL:
      return { ...state, selectUser: undefined, modal: !state.modal }
    case types.DELETE_USER:
      var id = action.id;
      var index = findIndex(state, id);
      state.splice(index, 1)
      // localStorage.setItem('users', JSON.stringify(state))
      return { ...state };
    case types.SEARCH_USER:
      let { users, keyword, roleId, department } = action;
      return { ...state, users: users, keyword, roleId, department };

    case types.RESET_SEARCH_USER:
      console.log(action.users)
      return { ...state,users: action.users};
    default:

      return { ...state };
  }
}
export default userStaff;
