import React, { Component, Suspense } from 'react';
import { Table, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import UserItem from './UserItem'
import { connect } from 'react-redux';
import { atcListAllResquest } from './../actions/user.action';
import removeAccents from 'remove-accents';
import apiCaller from './../../../_helpers/apiCaller';
import PaginationList from './PaginationList';



class UserList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      newsPerPage: 3,
      prevPages:[],
      counter:0,
     
    }
  }
  componentDidMount() {
    apiCaller('staff','list', "POST", JSON.stringify({
      "payload": {
        "role" : "",
        "pageNum": 1,
        "pageSize": 10,
        "direction":"DESC",
        "department":"",
        "key":""
      }
    })).then(res => {
      if(res){
        this.props.fetchAllUsers(res.data.payload.list,res.data.payload.count)
      }
      // else{
      //    localStorage.clear();
      //    this.props.history.push(`/login`);
         
      // }
      
    })
         

  }
  chosePage = (event) => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  select = (event) => {
    this.setState({
      newsPerPage: event.target.value
    })
  }
  onCloseModal = () =>{
    this.props.onCloseModal(this.state);
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  // onNextPage = (cursor) => {
  //   if(cursor!=null){
  //     this.props.onNextPage(cursor);
  //     this.state.prevPages.push(cursor)
  //     this.setState({
  //       counter:this.state.counter+1
  //     });
  //   }

  // }
  // onPrevPage = () => {
  //   const {prevPages, counter} = this.state;
  //   if(counter>0){
  //     this.setState({
  //       counter:this.state.counter-1
  //     })
  //     this.props.onPrevPage(prevPages[counter]);
  //   }

  // }

  render() {
    
    // var { prevPage,currentPage,todosPerPage, counter} = this.state;
    var { users, keyword, roleId, department, cursor } = this.props;
    const currentPage = this.state.currentPage;
    const newsPerPage = this.state.newsPerPage;
    const indexOfLastNews = currentPage * newsPerPage;
    const indexOfFirstNews = indexOfLastNews - newsPerPage;
    const currentTodos = users.slice(indexOfFirstNews, indexOfLastNews);
    
    

    if (users.length) {
      var itemUser = currentTodos.map((user, index) => {
        return <UserItem
          stt={index + 1 + (currentPage - 1)*newsPerPage} 
          key={index}
          index={index}
          user={user}
          onToggle={this.props.onToggle} />
      });
    }
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(users.length / newsPerPage); i++) {
      pageNumbers.push(i);
    }

    return (
      <div>
        <Table className="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th scope="col" className="text-center">STT</th>
              <th scope="col">Email</th>
              <th scope="col">Tên người dùng</th>
              <th scope="col">Phòng ban</th>
              <th scope="col">Vai trò</th>
              <th scope="col">Quyền</th>
            </tr>
          </thead>
          <Suspense fallback={this.loading()}>
          <tbody>
            {itemUser}
          </tbody></Suspense>
          
        </Table>
        {/* <PaginationList users = {users} todosPerPage={todosPerPage}/> */}
        <Pagination>
            {
              pageNumbers.map(number => {
                if (this.state.currentPage === number) {
                  return (
                    <PaginationItem key={number} active>
                      <PaginationLink tag="button" key={number} id={number} >
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  )
                }
                else {
                  return (
                    <PaginationItem key={number} >
                      <PaginationLink key={number} id={number} onClick={this.chosePage} tag="button">
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  )
                }
              })
            }
          </Pagination>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    users: state.userStaff.users,
    keyword: state.userStaff.keyword,
    roleId: state.userStaff.roleId,
    department: state.userStaff.department,
    cursor: state.userStaff.cursor,

  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllUsers: () => {
      dispatch(atcListAllResquest())
    },
    onNextPage: (cursor) => {
      dispatch(atcListAllResquest(cursor))
    },
    onPrevPage: (prevPage) => {
      dispatch(atcListAllResquest(prevPage))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserList);
