import React from 'react';
import {
  Button, CustomInput, Modal, ModalHeader, ModalBody, ModalFooter, Input, FormGroup,
  label
} from 'reactstrap';
import * as actions from './../actions/user.action';
import { connect } from 'react-redux';
import apiCaller from './../../../_helpers/apiCaller';
class ModalUser extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isStaff: false,
      isSurveyor: false,
      isApproval: false,
      roleId: ''
    }
  }

  onToggleCloseModal = () => {
    this.props.onToggleCloseModal(this.props.modal);
  }
  componentWillMount() {
    /** kiem tra cap nhat */
    if (this.props.user) {
      this.setState({
        isStaff: this.props.user.isStaff,
        isSurveyor: this.props.user.isSurveyor,
        isApproval: this.props.user.isApproval,
        roleId: this.props.user.roleId,
      });
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps && nextProps.user) {
      this.setState({
        isStaff: nextProps.user.roleId === 1000 ? true : false,
        isSurveyor: nextProps.user.roleId === 2 ? true : false,
        isApproval: nextProps.user.roleId === 1001 ? true : false,
        roleId: nextProps.user.roleId
      })
    }
  }

  toggleChangeStaff = (event) => {

    this.setState(prevState => ({
      isStaff: !prevState.isStaff,
      isSurveyor: false,
      isApproval: false,
      roleId: 1000
    }));

  }

  toggleChangeSurveyor = (event) => {

    this.setState(prevState => ({
      isStaff: false,
      isSurveyor: !prevState.isSurveyor,
      isApproval: false,
      roleId: 2
    }));
  }

  toggleChangeApproval = (event) => {

    this.setState(prevState => ({
      isStaff: false,
      isSurveyor: false,
      isApproval: !prevState.isApproval,
      roleId: 1001
    }));
  }

  onSubmitUpdate = () => {
    apiCaller('staff','set_role', "POST",
      JSON.stringify({
        payload: {
          role: this.state.roleId,
          uuid: this.props.user.id
        }

      })).then(res => {
        this.props.onSubmitUpdate({ ...this.props.user, ...this.state })

      })

  }

  render() {

    var { modal, user } = this.props;
    if (!user) return (<div></div>);
    return (
      <div>
        <Modal
          isOpen={modal}
          toggle={this.toggle}
          size="lg">
          <ModalHeader toggle={this.onToggleCloseModal}>Phân quyền nhân viên</ModalHeader>
          <ModalBody>

            <form>
              <div className="form-group">
                <legend>Phân quyền người dùng</legend>
              </div>
              <div className="form-group">
                <div className="row">
                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <span className="label">Họ tên: </span><strong>{user.fullName}</strong><br />
                    <span className="label">Email: </span><strong>{user.email}</strong>

                  </div>
                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <span className="label">Phòng ban: </span><strong>{user.department}</strong><br />
                    <span className="label">Vai trò hiện tại: </span><strong className="label"> {user.roleId}</strong>

                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row border-bottom">
                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h6 className="">Vui lòng chọn 1 trong những vai trò bên dưới để thay đổi quyền</h6>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <CustomInput type="radio"
                      className="mt-3"
                      name="roleId"
                      value="1000"
                      onChange={this.toggleChangeStaff}
                      checked={this.state.isStaff}
                      id="inputstaff" label="Nhân viên" />{this.state.isStaff}

                    <CustomInput type="radio"
                      name="roleId"
                      value="2"
                      onChange={this.toggleChangeSurveyor}
                      checked={this.state.isSurveyor}
                      id="inputstaff2" label="Khách hàng" />{this.state.isSurveyor}

                    <CustomInput type="radio"
                      name="roleId"
                      value="1001"
                      onChange={this.toggleChangeApproval}
                      checked={this.state.isApproval}
                      id="inputstaff3" label="Nhân viên duyệt hồ sơ" />{this.state.isApproval}

                  </div>
                </div>
              </div>
            </form>

          </ModalBody>
          <ModalFooter>
            <Button type="submit" color="primary" className="btn btn-primary mr-2" onClick={this.onSubmitUpdate}>Cập nhật</Button>{' '}
            <Button color="secondary" onClick={this.onToggleCloseModal}>Hủy</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    modal: state.userStaff.modal,
    user: state.userStaff.selectUser,
  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onToggleCloseModal: () => {
      dispatch(actions.closeModal());
    },
    onSubmitUpdate: (user) => {
      dispatch(actions.updateUser(user))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalUser);
