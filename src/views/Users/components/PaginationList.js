import React from 'react';
import {Pagination,PaginationItem,PaginationLink} from 'reactstrap';
import * as actions from './../actions/user.action';
import { connect } from 'react-redux';
class PaginationList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      todosPerPage: 10,

    }
  }
  render() {
    var { users,todosPerPage } = this.props;
    // const currentTodos = users.slice(indexOfFirstTodo, indexOfLastTodo);
    return (
      <Pagination aria-label="Page navigation example">
         <PaginationItem>
           <PaginationLink previous
            onClick={() => {
              this.state.currentPage > 1 ?
                this.setState({ currentPage: this.state.currentPage - 1 }) : this.setState({ currentPage: this.state.currentPage })
            }} />
        </PaginationItem>
        <PaginationItem>
          <PaginationLink next
            onClick={() => {
              this.state.currentPage >= (users.length / todosPerPage) ?
                this.setState({ currentPage: this.state.currentPage }) : this.setState({ currentPage: this.state.currentPage + 1 })
            }} />
        </PaginationItem>
      </Pagination>
    );
  }
}

export default PaginationList;
