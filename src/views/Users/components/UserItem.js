import React from 'react';
import { Badge } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/user.action';




class UserItem extends React.Component {


  onUpdateStatus = () => {
    this.props.onUpdateStatus(this.props.user.id);
  }
  onDeleteUser = () => {
    if(confirm('Bạn chắc chắn muốn xóa ?')){ //eslint-disable-line
    this.props.onDeleteUser(this.props.user.id);
    }

  }
  onSetRole = () => {
      this.props.onSetRole(this.props.user.id);
  }

  showStatusElement = () => {
    return (
      <span
        className={this.props.user.status ? 'btn btn-danger label label-danger' : 'btn btn-info label label-info'}
        onClick={this.onUpdateStatus}
      >
        {this.props.user.status === true ? "Kính hoạt" : "Ẩn"}
      </span>
    );
  }
  showDeleteElement = () => {
    return (
      <span
        className="ml-2 btn btn-danger label label-danger"
        onClick={this.onDeleteUser}
      >
        Xóa
      </span>
    );
  }
  render() {
    var { user, index , stt} = this.props;
    const userLink = `/users/${user.id}`;
    const roleId = (roleId) =>{
      return roleId===1000?'Nhân viên':
      roleId===2?'Khách hàng':
      roleId===1001?'Nhân viên duyệt hồ sơ':''
    }


    return (
      <tr>
        <td className="text-center">{stt}</td>
        <td>{user.email}</td>
        <td>{user.fullName}</td>
        <td>{user.department} - {roleId(user.roleId)}</td>
        <td>
          <span size="sm" className="outline text-primary" >
              {roleId(user.roleId)}
            </span>
          {/* {this.showStatusElement()} */}

        </td>
        <td className="text-center"> <span size="sm"  className="btn btn-sm mr-2 btn-edit"  onClick={this.onSetRole} >
        <i className="fa fa-edit"></i>
            </span>
            {/* {this.showDeleteElement()} */}
        </td>
      </tr>
    );
  }
}
const mapStateToProps = (state) => {
  return {

  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateStatus: (id) => {
      dispatch(actions.onUpdateStatus(id));
    },
    onDeleteUser: (id) => {
      dispatch(actions.deleteUser(id));
    },
    onSetRole: (id) => {
      dispatch(actions.actEditUserResquest(id));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserItem);
