import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/user.action';
import apiCaller from './../../../_helpers/apiCaller';



class UserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      name: '',
      email: '',
      department:'',
      role:'',
      privilege:'',
      status: false,
    }
  }
  componentWillMount() {
    /** kiem tra cap nhat */
    if (this.props.userStaff) {
      this.setState({
        id: this.props.userStaff.id,
        name: this.props.userStaff.name,
        address: this.props.userStaff.address,
        status: this.props.userStaff.status,
      });
    }
  }
  /** push data to field */
  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.userStaff) {
      this.setState({
        id: nextProps.userStaff.id,
        name: nextProps.userStaff.name,
        address: nextProps.userStaff.address,
        status: nextProps.userStaff.status,
      });
    }
  }
  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === 'status') {
      value = target.value === 'true' ? true : false;
    }
    this.setState({
      [name]: value
    });
  }
   s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
   generatesID = () => {
    return this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4();
  }
  onSubmit = (event) => {
    event.preventDefault();
    var {name,email,department,role,status} = this.state;
    apiCaller('staff','payload','POST', {
      entities:[
        {
          id:this.generatesID(),
          name:name,
          email:email,
          role:role,
          status:status
        }
      ]

    })
    // this.props.onAddUser(this.state)
    this.setState({
      id: '',
      name: '',
      email: '',
      department:'',
      role:'',
      privilege:'',
      status: false,
    });
  }
  render() {
    var { id } = this.state;
    return (

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <form onSubmit={this.onSubmit}>
            <fieldset className="border p-2">
              <legend className="w-auto">{id !== '' ? 'Cập nhật' : 'Thêm người dùng mới'}</legend>
              <div className="row col-md-12 form-group">
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Tên người dùng:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Tên người dùng" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Email:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="email"
                      value={this.state.email}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Email" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Trạng thái:</label>
                  <div className="col-sm-12">
                    <select name="status"
                      value={this.state.status}
                      onChange={this.onChange}
                      id="inputstatus" className="form-control">
                      <option value={true} >Kích hoạt</option>
                      <option value={false}>Ẩn</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="row col-md-12 form-group">
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Phòng ban:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="department"
                      value={this.state.department}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Phòng ban" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Vai trò:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="role"
                      value={this.state.role}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Vai trò" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Quyền:</label>
                  <div className="col-sm-12">
                    <select name="privilege"
                      value={this.state.privelege}
                      onChange={this.onChange}
                      id="inputstatus" className="form-control">
                      <option value="staff" >Nhân viên</option>
                      <option value="surveyor">Kiểm soát viên</option>
                      <option value="approval">Duyệt hồ sơ</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-12 col-sm-offset-2 text-right">
                  <button type="submit" className="btn btn-primary mr-2">{id !== '' ? 'Cập nhật' : 'Thêm mới'}</button>
                  <button type="submit" className="btn btn-secondary mr-2">Hủy</button>
                  {/* <button type="button" className="btn  btn-danger" onClick={this.onGeneratecontract}>Generate data</button> */}
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
   return {

   }
};
const mapDispatchToProps = (dispatch,props) => {
   return {
      onAddUser:(user)=>{
        dispatch(actions.AddUser(user))
      }
   }
}

export default connect(mapStateToProps,mapDispatchToProps)(UserForm);
