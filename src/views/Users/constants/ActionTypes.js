export const LIST_USER = 'LIST_USER';
export const UPDATE_STATUS_USER = 'UPDATE_STATUS_USER';
export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const EDIT_USER = 'EDIT_USER';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const DELETE_USER = 'DELETE_USER';
export const SEARCH_USER = 'SEARCH_USER';
export const FILTER_TABLE = 'FILTER_TABLE';
export const RESET_SEARCH_USER = 'RESET_SEARCH_USER';


