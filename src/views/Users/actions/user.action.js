import * as types from './../constants/ActionTypes';
import axios from 'axios';
import apiCaller from './../../../_helpers/apiCaller';

// const apiUrl = 'http://192.168.75.5:8000/api/v1/staff';
// const apiUrl = 'http://localhost:3000';

export const atcListAllResquest = (cursor) => {
  return (dispatch) => {
    return apiCaller('staff','list', 'POST',
      JSON.stringify({
        "payload": {
          "role" : "",
          "pageNum": 1,
          "pageSize": 10,
          "direction":"DESC",
          "department":"",
          "key":""
      }
      })
    ).then(res=>{
      if(res){
        dispatch(actFetchUsers(res.data.payload.list,res.data.payload.count))
      }
    })
  };
}
export const actFetchUsers = (users,cursor) => {
  return {
    type: types.LIST_USER,
    users,
    cursor
  }
};
export const AddUser = (user) => {
  return {
    type: types.ADD_USER,
    user
  }
}
export const openModal = () => {
  return {
    type: types.OPEN_MODAL,
  }
}
export const closeModal = () => {
  return {
    type: types.CLOSE_MODAL,
  }
}
export const onUpdateStatus = (id) => {
  return {
    type: types.UPDATE_STATUS_USER,
    id
  }
}
export const actEditUserResquest = (id)=> {
  return (dispatch) => {
    return apiCaller('staff','find', 'POST',
      JSON.stringify({
          "payload": {
            "uuid": id
          }
      })
    ).then(res=>{
      if(res){
      dispatch(editUser(res.data.payload))
      }
    })
  };
}
export const editUser = (user) => {
  return {
    type: types.EDIT_USER,
    user
  }
}
export const updateUser = (user) => {
  return {
    type: types.UPDATE_USER,
    user
  }
}
export const actSearchUserResquest = (keyword,roleId,department) => {
  return (dispatch) => {
    return apiCaller('staff','list', 'POST',
      JSON.stringify({
        "payload": {
          "role" : roleId,
          "pageNum": 1,
          "pageSize": 5,
          "direction":"DESC",
          "department":department,
          "key":keyword
      }
      })
    ).then(res=>{
      if(res){
      dispatch(atcSearchUser(res.data.payload.list,keyword,roleId,department))
      }
    })
  };
}

export const atcSearchUser = (users,keyword,roleId,department) => {
  return {
    type: types.SEARCH_USER,
    users,
    keyword,
    roleId,
    department
  }
}
export const atcDeleteUserRequest = (id) => {
  return (dispatch) => {
    return apiCaller('staff','list', 'POST',
      JSON.stringify({
        "payload": {
          "role" : "",
          "pageNum": 1,
          "pageSize": 5,
          "direction":"DESC",
          "department":"",
          "key":""
      }
      })
    ).then(res=>{
      if(res){
        
        dispatch(atcSearchUser(res.data.payload.entities))
      }
    })
  };
}
export const deleteUser = (id) => {
  return {
    type: types.DELETE_USER,
    id
  }
}
export const filterUser = (filter) => {
  return {
    type: types.FILTER_TABLE,
    filter
  }
}
export const atcSearchUserResquest = () => {
  return (dispatch) => {
    return apiCaller('staff','list', 'POST',
      JSON.stringify({
        "payload": {
          "role" : "",
          "pageNum": 1,
          "pageSize": 5,
          "direction":"DESC",
          "department":"",
          "key":""
      }
      })
    ).then(res=>{
      if(res){

        dispatch(actResetSearchUser(res.data.payload.list))
      }
    })
  };
}
export const actResetSearchUser = (users) => {
  return {
    type: types.RESET_SEARCH_USER,
    users
  }
}
