import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody,Label, FormGroup, CardHeader, Col, Row, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import * as actions from './actions/user.action';
import { connect } from "react-redux";
import ModalUser from './components/ModalUser';
import UserForm from './components/UserForm';
import UserList from './components/UserList'

class Users extends Component {

  constructor(props) {
    super(props);
    this.state = {
      keyword: '',
      department: '',
      roleId: ''
    };
  }
  onToggleModal = () => {
    this.props.onToggleModal();
  }
  onHandlechange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  }
  onSearch = (e) => {
    let { keyword, roleId, department } = this.state;
    this.props.onSearch(keyword, roleId, department)
  }
  onReset = () => {
    this.setState({
      keyword: '',
      department: '',
      roleId: ''
    });
    this.props.onReset(this.state);
  }

  render() {
    var { modal, keyword, roleId, department } = this.props;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <h3>Danh sách nhân viên</h3>
              </CardHeader>
              <CardBody>
              <fieldset className=" p-2 form-group">
              {/* <legend className="border-bottom"><Label>Tìm kiếm</Label></legend> */}
                  {/* <legend className="w-auto border-bottom">Tìm kiếm </legend> */}
                  <div className="row form-group">
                    <div className="col-md-12">
                      <div className="row">
                        <label className="col-sm-2 control-label text-right">Từ khóa</label>
                        <div className="col-sm-7">
                          <input type="text" className="form-control" placeholder="Từ khóa"
                            value={this.state.keyword}
                            onChange={this.onHandlechange}
                            name="keyword" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-md-6">
                      <div className="row">
                        <label className="col-sm-4 control-label text-right">Phòng ban</label>
                        <div className="col-sm-6">
                          <select name="department" id="input" onChange={this.onHandlechange} value={this.state.department} className="form-control" required="required">
                            <option value="">Tất cả</option>
                            <option value="IT">IT</option>
                            <option value="Sales">Kinh doanh</option>
                            <option value="Finance">Dự án/Tài chính</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="row">
                        <label className="col-md-2 control-label">Vai trò:</label>
                        <div className="col-md-4">
                          <select name="roleId" id="input" onChange={this.onHandlechange} value={this.state.roleId} className="form-control" required="required">
                            <option value="">Tất cả</option>
                            <option value="1000">Nhân viên</option>
                            <option value="2">Kiểm soát viên</option>
                            <option value="1001">Duyệt hồ sơ</option>
                          </select>
                        </div>
                        <div className="col-md-6">
                          <Button type="submit" onClick={this.onSearch} color="success" className="btn mr-2">Tìm kiếm</Button>
                          <Button type="reset" onClick={this.onReset} color="info" className="btn btn-secondary">Reset</Button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row form-group">
                   
                  </div>
                </fieldset>
                {/* <UserForm
                onSubmit={this.onSubmit}
              /> */}

                {/* <div className="row">
                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <i className="fa fa-align-justify"></i> Danh sách quản lý người dùng
                  </div>

                </div> */}
                <UserList
                  onEdit={this.onEdit}
                  onToggle={this.onToggleModal}
                  onChangeStatus={this.onChangeStatus}
                  onDelete={this.onDelete}
                  {...this.props}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <ModalUser openModal={modal} />
      </div>

    )
  }
}
const mapStateToProps = (state) => {
  return {
    modal: state.userStaff.modal,
    keyword: state.userStaff.keyword,
    roleId: state.userStaff.roleId,
    department: state.userStaff.department
  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onToggleModal: () => {
      dispatch(actions.openModal());
    },
    onSearch: (keyword, roleId, department) => {
      dispatch(actions.actSearchUserResquest(keyword, roleId, department))
    },
    onReset: () => {
      dispatch(actions.atcSearchUserResquest())
    },

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
