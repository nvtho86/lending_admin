import React, { Component } from 'react'
import {
  Card, CardBody, CardHeader,
  Button, Table, Input, FormGroup
} from 'reactstrap'
import { connect } from 'react-redux'
import * as actions from './actions/preferential.action'
import { Link } from 'react-router-dom'
import PreferentialDeleteForm from './components/PreferentialDeleteForm'
import moment from "moment"


class PreferentialInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      preferentialList: [],
      isOpenModalDelete: false,
      dataProps: {},
      file: null,
      infoSearch: 'Ưu đãi riêng',
      typeSearch: 'Xe máy',
      keySearch: '',
      listPagination: [],
      isOpenModalDelete: false,
      propsDelete: {},
      pageSize: 5
    };
  }

  componentDidMount() {
    const data = {
      direction: "",
      keyWord: "",
      oderBy: "",	
      pageNum: 1,
      pageSize: 5,
      type: 0
    }
    this.props.onLoadPreferential(data)
  }

  componentWillReceiveProps(nextProps) {
    const { preferentialData } = nextProps
    const { pageNum, total, preferentialList } = preferentialData
    const { pageSize } = this.state
    if (this.props.preferentialData.pageNum !== pageNum)
      this.onInit(pageNum)
    else {
      if (preferentialData && preferentialList)
        this.setState({ preferentialList: preferentialList })
    }
    const totalPage = Math.round(total / pageSize)
    let list = new Set()
    for (let i = 1; i <= totalPage; i++) {
      list.add(i)
    }
    this.setState({
      listPagination:list
    })
  }

  onInit(pageNum = 1, keyWordSearch = '', type = 0) {
    const data = {
      direction: "",
      keyWord: keyWordSearch,
      oderBy: "",
      pageNum: pageNum,
      pageSize: this.state.pageSize,
      type: type
    }
    this.props.onLoadPreferential(data)
  }

  onCloseModal = () => {
    this.toggleDelete(false)
  }

  onHandlechange = (event) => {
    var { name, value } = event.target
    this.setState({
      [name]: value
    });
  }

  /* modal delete toggle */
  toggleDelete = state => {
    this.setState({
      isOpenModalDelete: state
    });
  }

  onDelete = data => () => {
    this.setState({
      propsDelete: data
    }, () => {
      this.toggleDelete(true)
    })
  }

  submitDeletePrefential = async () => {
    await actions.deletePreferential(this.state.propsDelete.id).then(res => {
      if (res) {
        this.onInit(1)
        this.toggleDelete(false)
      }
    })
  }

  searchPreferential = () => {
    // const { pageNum }= this.props.newsData
    // const { keyWordSearch, typeSearch } = this.state
    // this.onInit(pageNum, keyWordSearch, typeSearch)
  }

  render() {
    const { preferentialList, keySearch , infoSearch, typeSearch } = this.state
    var preferentialItems = preferentialList.map((preferential, index) => {
      const stripContent = preferential.content.replace(/(<([^>]+)>)/ig,"");
      const link = `/preferential-information/${preferential.id}`
      return (
        <tr key={index + 1}>
          <td className="text-center">{index + 1}</td>
          <td >{preferential.title}</td>
          <td >{stripContent}</td>
          <td className="text-center">{preferential.interestRate} %</td>
          <td className="text-center">{preferential.type}</td>
          <td className="text-center">{moment(preferential.startDate).format('DD/MM/YYYY')}</td>
          <td className="text-center">{moment(preferential.endDate).format('DD/MM/YYYY')}</td>
          <td className="text-center">
            <Link className="btn-edit" to={link}><i className="fa fa-edit"></i></Link>
          </td>
          <td className="text-center">
            <button type="button" className="btn mr-2 btn-danger" onClick={this.onDelete(preferential)}><i className="fa fa-trash-o"></i></button>
          </td>
        </tr>
      )
    })
    
    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>
            <h3 className="check">Thông tin ưu đãi</h3>
          </CardHeader>
          <CardBody className="pb-0">
            <FormGroup className="form-search">
              <div className="type-search">
                <label>Từ khóa:</label>
                <Input value={keySearch} onChange={this.onHandlechange} type="text" name="keySearch"></Input>
              </div>
              <div className="type-search input-search">
                <Input value={infoSearch} name="infoSearch" onChange={this.onHandlechange} type="select">
                  <option>Ưu đãi riêng</option>
                  <option>Ưu đãi chung</option>
                  <option>Tất cả</option>
                </Input>
              </div>
              <div className="type-search input-search">
                <Input value={typeSearch} name="typeSearch" onChange={this.onHandlechange} type="select">
                  <option>Xe máy</option>
                  <option>Điện máy</option>
                  <option>Tiền mặt</option>
                  <option>Tất cả</option>
                </Input>
              </div>
              <div className="btn-link">
                <Button color="success" onClick={this.searchPreferential}>Tìm kiếm</Button>
                <Link className="btn-create" to="/preferential-information/create">Thêm mới</Link>
              </div>
            </FormGroup>
            <Table className="table-outline table-list-preferential" bordered hover responsive striped variant="dark">
              <thead>
                <tr>
                  <th className="text-center" >STT</th>
                  <th className="text-center" >Tiêu đề</th>
                  <th className="text-center" >Nội dung</th>
                  <th className="text-center" >Lãi suất</th>
                  <th className="text-center" >Loại</th>
                  <th className="text-center" >Ngày bắt đầu</th>
                  <th className="text-center" >Ngày kết thúc</th>
                  <th className="text-center" >Sửa</th>
                  <th className="text-center">Xóa</th>
                </tr>
              </thead>
              <tbody>
                {preferentialItems}
              </tbody>
            </Table>
          </CardBody>
        </Card>

        <PreferentialDeleteForm
          isOpenModalDelete={this.state.isOpenModalDelete}
          itemDelete={this.state.propsDelete}
          onCloseModal={this.onCloseModal}
          onDeleteNews={this.submitDeletePrefential}
        >
        </PreferentialDeleteForm>
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    preferentialData: state.preferentialData,
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onLoadPreferential:(data) => {
      dispatch(actions.getListPreferential(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PreferentialInformation)