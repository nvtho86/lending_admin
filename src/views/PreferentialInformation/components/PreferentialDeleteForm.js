import React, { Component } from 'react';
import {
  Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter,
  Input
} from 'reactstrap';

const txtAlertDelete = { 
  color: 'red',
  textAlign: 'center',
  fontSize: '20px',
  fontWeight: '500'
}

const infoItem = {
  color: 'blue',
  fontWeight: '500'
}

class PreferentialDeleteForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			
		}
	}


	render () {
		const itemDelete = this.props.itemDelete

		return (
				<Modal isOpen={this.props.isOpenModalDelete} size='lg'
					className={'modal-lg'}>
					<ModalHeader toggle={this.toggleLarge}>Xác nhận xóa thông tin ưu đãi</ModalHeader>
					<ModalBody>
						<Row>
              <Col md={3}>
                <label>Tiêu đề</label>
              </Col>
              <Col md={9}>
                <p style={infoItem}>{itemDelete.title}</p>
              </Col>
              <Col md={3}>
                <label>Nội dung</label>
              </Col>
              <Col md={9}>
                <p style={infoItem}>{itemDelete.contentBrief}</p>
              </Col>
            </Row>
            <div style={txtAlertDelete}>
              <span>Xác nhận xóa thông tin ưu đãi</span>
            </div>
					</ModalBody>
					<ModalFooter>
							<Button color="primary" onClick={this.props.onCloseModal}>Hủy bỏ</Button>
							<Button color="secondary" onClick={this.props.onDeleteNews}>Lưu</Button>
					</ModalFooter>
				</Modal>
		)
	}
}

export default PreferentialDeleteForm;