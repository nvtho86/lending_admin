import React, { Component } from 'react'
import {
	Row, Col, Button, Card, CardHeader, CardBody, CardFooter,
	Input, Label, CustomInput, FormGroup
} from 'reactstrap'
import "react-datepicker/dist/react-datepicker.css"
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { convertFromHTML, ContentState, EditorState, convertToRaw } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import { connect } from 'react-redux'
import DatePicker from 'react-datepicker'
import * as actions from '../actions/preferential.action'

class PreferentialHandleForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			dataEdit: {
				id: "",
				title: "",
				contentBrief: "",
				content: "",
				imagePath: "",
				access: 1,
				status: 1,
				type: 1,
				isFeatured: 0,
				modifiedBy: null,
				startDate: new Date(),
				endDate: new Date(),
				statusNotification: 0,
				interestRate: 0.0,
				promotionEndDate: new Date(),
				filterCustomers: []
			},
			offerFor: '',
			file: '',
			editorState: EditorState.createEmpty()
		}
	}

	async componentDidMount() {
		const listPath = this.props.location.pathname.split('/')
        const id = listPath[listPath.length - 1]
        await actions.getDetailPreferential(id).then(data => {
			if (data) {
				this.setState({
					dataEdit: data,
					offerFor: data.filterCustomers && data.filterCustomers.length > 0 ? '1' : '0',
					editorState: data.content ?
							EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML('<p>' + data.content + '</p>'))) :
							EditorState.createEmpty()
				})
			}
        })
	}

	componentWillReceiveProps() {
		
	}

	// Add to list of filterList
	addFilterCustomer = () => {
        let { filterCustomers } = this.state.dataEdit
        const { customerFilterCategory } = this.props.defaultData
        if (filterCustomers.length == customerFilterCategory.length)
            return
        let listFilter = filterCustomers.map(item => {
            return item.key
        })
        let list =  customerFilterCategory.filter(item => {
            return !listFilter.includes(item.key)
        })
        if (list.length > 0) {
            filterCustomers.push({
                key: list[0].key,
                compare: '=',
                value: '',
                id: 0
            })
            this.setState(prevState => ({
                dataEdit: {
                    ...prevState.dataEdit,
                    filterCustomers: filterCustomers
                }
            }))
        }
	}

	// Detete one of list of filterList
	deleteFilterCustomer = index => () => {
        var dataTemp = this.state.dataEdit.filterCustomers
        dataTemp.splice(index, 1)
		
		this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                filterCustomers: dataTemp
            }
		}))
	}

	// Event change offer of Preferential
	changeOffer = (event) => {
		this.setState({
			offerFor: event.target.value
		})
	}

	// Add to list of filterList
	addPropsFilter = () => {
		var newItem = {
			condition: 'Tuổi',
			typeOfCompare: '=',
			value: ''
		}
		var dataTemp = this.state.filterForOffer;
		dataTemp.push(newItem);
		this.setState({
			filterList: dataTemp
		})
	}

	// Detete one of list of filterList
	deletePropsFilter = (event) => {
		var dataTemp = this.state.filterForOffer;
		dataTemp.splice(event.target.id, 1);
		this.setState({
			filterList: dataTemp
		})
	}

	onEditorChange = (editorState) => {
		const content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
		this.setState(prevState => ({
			editorState,
			dataEdit: {
				...prevState.dataEdit,
				content: content
			}
		}))
	}

	renderEditor = () => {
		return { __html: '<span>' + this.state.dataEdit.content + '</span>' };
	}

	handleChange = event => {
		var { name, value } = event.target;
		
		this.setState({
			[name]: value
		});
	}

	handleChangeDataEdit = e => {
        const { value, name, checked, type } = e.target
        this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                [name]: type != 'checkbox' ? value : (checked ? 1 : 0)
            }
        }))
    }

	onChangeImage = (event) => {
		if (event && event.target.files[0]) {
			var reader = new FileReader();
			var file = event.target.files[0];
			
			reader.onload = upload => {
				this.setState({
					file: upload.target.result
				})
			}
			reader.readAsDataURL(file);
		}
	}

	handleChangeFilterDataProps = key => e => {
        const { value, name } = e.target
        let { filterCustomers } = this.state.dataEdit
        filterCustomers.forEach(item => {
            if (key == item.key) {
                item[name] = value
            }
        })
        this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                filterCustomers: filterCustomers
            }
        }))
	}

	handleChangeDate = (field, date) => {
		this.setState(prevState => ({
			dataEdit: {
				...prevState.dataEdit,
				[field]: date
			}
		}))
	}
	
	onUpdatePreferential = async () => {
		const { dataEdit, file } = this.state
        let imagePath = ''
        if (file) {
            await actions.onUploadImage(file).then(async res => {
                if (res) {
                    imagePath = res
                }
            })
        }
        const filters = dataEdit.filterCustomers
        const param = {
            filters: filters,
            promotion: {
                ...dataEdit,
                imagePath: imagePath
            }
        }

        delete param.promotion.filterCustomers

        await actions.onUpdatePreferential(param).then(res => {
            if (res) {
                this.props.history.push("/preferential-information")
            } 
        })
	}

	render() {
		const { offerFor, dataEdit, editorState, file } = this.state
		const { customerFilterCategory, compareType } = this.props.defaultData

		const startDate = dataEdit.startDate ? new Date(dataEdit.startDate) : new Date()
		const endDate = dataEdit.endDate ? new Date(dataEdit.endDate) : new Date()
		const promotionEndDate = dataEdit.promotionEndDate ? new Date(dataEdit.promotionEndDate) : new Date()


		return (
			<Card className="animated fadeIn">
				<CardHeader toggle={this.toggleLarge}><h3 className="check">Chỉnh sửa thông tin ưu đãi</h3></CardHeader>
				<CardBody>
					<Row>
						<Col md={8}>
							<Row className="rowEdit normal">
								<Col md={3}>Title</Col>
								<Col md={9}>
									<Input onChange={this.handleChangeDataEdit} name="title" type="text" value={dataEdit.title} />
								</Col>
							</Row>

							<Row className="rowEdit normal">
								<Col md={3}>Nội dung ngắn gọn</Col>
								<Col md={9}>
									<Input onChange={this.handleChangeDataEdit} name="contentBrief" type="text" value={dataEdit.contentBrief} />
								</Col>
							</Row>

							<Row className="rowEdit normal">
								<Col md={3}>Nội dung đầy đủ</Col>
								<Col md={9}>
									<Editor
										editorState={editorState}
										onEditorStateChange={this.onEditorChange}
										toolbar={{
											options: ['inline', 'fontSize', 'fontFamily', 'list',
												'textAlign', 'colorPicker', 'link', 'image'],
											link: {
												defaultTargetOption: '_blank',
												popupClassName: "mail-editor-link"
											},
											image: {
												urlEnabled: true,
												uploadEnabled: true,
												uploadCallback: this.uploadImageCallBack,
												alignmentEnabled: true,
												defaultSize: {
													height: 'auto',
													width: 'auto',
												},
												inputAccept: 'application/pdf,text/plain,application/vnd.openxmlformatsofficedocument.wordprocessingml.document,application/msword,application/vnd.ms-excel'
												,
												colorPicker: {
													className: undefined,
													component: undefined,
													popupClassName: undefined,
													colors: ['rgb(97,189,109)', 'rgb(26,188,156)', 'rgb(84,172,210)', 'rgb(44,130,201)',
														'rgb(147,101,184)', 'rgb(71,85,119)', 'rgb(204,204,204)', 'rgb(65,168,95)', 'rgb(0,168,133)',
														'rgb(61,142,185)', 'rgb(41,105,176)', 'rgb(85,57,130)', 'rgb(40,50,78)', 'rgb(0,0,0)',
														'rgb(247,218,100)', 'rgb(251,160,38)', 'rgb(235,107,86)', 'rgb(226,80,65)', 'rgb(163,143,132)',
														'rgb(239,239,239)', 'rgb(255,255,255)', 'rgb(250,197,28)', 'rgb(243,121,52)', 'rgb(209,72,65)',
														'rgb(184,49,47)', 'rgb(124,112,107)', 'rgb(209,213,216)']
												}
											}
										}}
										editorClassName="form-control editorClassName"
									/>
								</Col>
							</Row>

							<Row className="rowEdit normal centerInline">
								<Col md={3}>Loại ưu đãi </Col>
								<Col md={3}>
									<Input name="type" onChange={this.handleChangeDataEdit} value={dataEdit.type} type="select">
										<option value="0">Điện máy</option>
										<option value="1">Tiền mặt</option>
									</Input>
								</Col>

								<Col md={3}>Lãi suất ưu đãi</Col>
								<Col md={3}>
									<Input type="text" name="interestRate" onChange={this.handleChangeDataEdit} value={dataEdit.interestRate} />
								</Col>
							</Row>

							<Row className="rowEdit normal">
								<Label md={3}>Ngày bắt đầu hiển thị</Label>
								<Col md={3}>
									<DatePicker
										className="form-control"
										name="startDate"
										selected={startDate}
										onChange={date => this.handleChangeDate('startDate', date)}
										showYearDropdown
										dateFormatCalendar="MMMM"
										scrollableYearDropdown
										yearDropdownItemNumber={15}
										dateFormat="dd/MM/yyyy"
									/>
								</Col>
								<Label md={3}>Ngày kết thúc hiển thị</Label>
								<Col md={3}>
									<DatePicker
										className="form-control"
										name="endDate"
										selected={endDate}
										onChange={date => this.handleChangeDate('endDate', date)}
										showYearDropdown
										dateFormatCalendar="MMMM"
										scrollableYearDropdown
										yearDropdownItemNumber={15}
										dateFormat="dd/MM/yyyy"
									/>
								</Col>
							</Row>

							<Row className="rowEdit normal">
								<Label md={3}>Ngày hết hạn ưu đãi</Label>
								<Col md={3}>
									<DatePicker
										className="form-control"
										name="endDate"
										selected={promotionEndDate}
										onChange={date => this.handleChangeDate('promotionEndDate', date)}
										showYearDropdown
										dateFormatCalendar="MMMM"
										scrollableYearDropdown
										yearDropdownItemNumber={15}
										dateFormat="dd/MM/yyyy"
									/>
								</Col>
								<Label md={3}>Hình ảnh</Label>
								<Col md={3}>
									<label className="file-button" htmlFor="file">
										Chọn hình
										<Input hidden name="file" id="file" onChange={this.onChangeImage} type="file" accept="image/*" />
									</label>
								</Col>
							</Row>

							<Row className="rowEdit normal">
								<Label md={3}>
									Tùy chọn
								</Label>
								<Col md={3} className="d-flex align-items-center">
									<CustomInput type="checkbox" id="isFeatured" checked={dataEdit.isFeatured == 1 ? true : false} value={dataEdit.isFeatured == 1 ? true : false} name="isFeatured" onChange={this.handleChangeDataEdit} label="Ưu đãi nổi bật" />
								</Col>
								
								<Col md={3} className="d-flex align-items-center">
									<CustomInput type="checkbox" id="statusNotification" checked={dataEdit.statusNotification == 1 ? true : false} value={dataEdit.statusNotification == 1 ? true : false} name="statusNotification" onChange={this.handleChangeDataEdit} label="Gửi thông báo" />
								</Col>
							</Row>

							<Row className="rowEdit normal">
								<Label md={3}>Ưu đãi dành cho</Label>
								<Col md={3}>
									<CustomInput type="radio" value="0" id="0" name="offerFor" checked={offerFor == '0'} onChange={this.handleChange} label="Toàn bộ" />
								</Col>
								<Col md={3}>
									<CustomInput type="radio" value="1" id="1" name="offerFor" checked={offerFor == '1'} onChange={this.handleChange} label="Chỉ 1 nhóm người" />
								</Col>
							</Row>
							{   
                                offerFor == '1' &&
                                <FormGroup className="animated fadeIn">
                                    <Row className="mb-2">
                                        <Label className="mb-2" md={12}>Lọc danh sách tài khoản nhận tin tức</Label>
                                        <Label md={{size: 3, offset: 3}}>
                                            Điều kiện
                                        </Label>
                                        <Label md={3}>
                                            So sánh
                                        </Label>
                                        <Label md={3}>
                                            Giá trị
                                        </Label>
                                    </Row>
                                    {
                                        dataEdit.filterCustomers.map((itemFilter, index) => (
                                        <FormGroup row key={index} >
                                            <Col className="d-flex align-items-center justify-content-end" md={3}>
                                                <Button className="btn-filter" onClick={this.deleteFilterCustomer(index)} ><i className="fa fa-minus"/></Button>
                                            </Col>
                                            <Col md={3}>
                                                <Input type="select" name="key" value={itemFilter.key} onChange={this.handleChangeFilterDataProps(itemFilter.key)}>
                                                    {
                                                        customerFilterCategory.map((fillterName, index) => {
                                                            return (
                                                                <option key={index} value={fillterName.key}>{ fillterName.label }</option>
                                                            )
                                                        })
                                                    }
                                                </Input>
                                            </Col>
                                            <Col md={3}>
                                                <Input type="select" name="compare" value={itemFilter.compare} onChange={this.handleChangeFilterDataProps(itemFilter.key)}>
                                                    {
                                                        compareType.map((compareValue) => {
                                                            return (
                                                                <option key={compareValue.id} value={compareValue.name}>{ compareValue.name }</option>
                                                            )
                                                        })
                                                    }
                                                </Input>
                                            </Col>
                                            <Col md={3}>
                                                <Input type="text" name="value" value={ itemFilter.value }
                                                onChange={this.handleChangeFilterDataProps(itemFilter.key)}  />
                                            </Col>
                                        </FormGroup>
                                    ))}
                                    {   
                                        dataEdit.filterCustomers.length != customerFilterCategory.length &&
                                        <Row>
                                            <Col className="d-flex align-items-center justify-content-end" md={3}>
                                                <Button className="btn-filter" onClick={this.addFilterCustomer} ><i className="fa fa-plus"/></Button>
                                            </Col>
                                        </Row>
                                    }
                                </FormGroup>
                            }
						</Col>
						<Col md={4}>
							<div className="review-frame">
								<div className="review-avatar">
									<img alt={file} src={file} />
								</div>
								<h4 className="review-title">
									{dataEdit.title}
								</h4>
								<h5 className="review-short-description">
									{dataEdit.shortDescription}
								</h5>
								<div dangerouslySetInnerHTML={this.renderEditor()}></div>
							</div>
						</Col>
					</Row>
				</CardBody>
				<CardFooter className="btn-footer">
					<Button color="primary" onClick={() => this.props.history.goBack()} name="handle">Quay lại</Button>
					<Button color="danger" onClick={this.onUpdatePreferential} name="handle">Lưu</Button>
				</CardFooter>
			</Card>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		defaultData: state.defaultData
	}
}
const mapDispatchToProps = (dispatch, props) => {
	return {
		
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PreferentialHandleForm)