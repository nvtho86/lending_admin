import * as types from './../constants/ActionTypes';
import apiCaller from './../../../_helpers/apiCaller'
// import apiCaller from '../utils/apiCaller'

export const getListPreferential = (param) => {
  return (dispatch) => {
      return apiCaller('promotion', 'list', 'POST',
          JSON.stringify(
              {
                  payload: param
              }
          )
      ).then(response => {
          if (response && response.data)
            dispatch(fetchListPreferential({list: response.data.payload.list, total: response.data.payload.total}))
      })
  };
}

export const onUpdatePreferential = (param) => {
  return apiCaller('promotion', 'update', 'POST',
      JSON.stringify(
          {
              payload: param
          }
      )
  ).then(response => {
      if (response && response.data) 
          return response.data.code == 200
  })
}

export const onCreatePreferential = (param) => {
  return apiCaller('promotion', 'post', 'POST',
      JSON.stringify(
          {
              payload: param
          }
      )
  ).then(response => {
      if (response && response.data) 
          return response.data.code == 200
  })
}

export const onUploadImage = (data) => {
  return apiCaller('promotion', 'upload_file', 'POST',
      JSON.stringify(
          {
              payload: {
                  files: [
                      {
                          data: data
                      }
                  ]
              }
          }
      )
  ).then(response => {
      if (response && response.data)
      {
          const { data } = response
          if (data.code == 200)
              return data.payload.files[0].data
      }
      return []
  })
}

export const getDetailPreferential = id => {
    return apiCaller('promotion', 'detail', 'POST',
        JSON.stringify(
            {
                payload: {
                    id: id
                }
            }
        )
    ).then(response => {
        if (response && response.data)
            return response.data.payload
        return {}
    })
}

export const deletePreferential = (id) => {
  return apiCaller('promotion', 'delete', 'POST',
      JSON.stringify(
          {
              payload: {
                  id: id
              }
          }
      )
  ).then(response => {
      if (response && response.data)
          return response.data.code == 200
  })
}

export const fetchListPreferential = (data) => {
  return {
    type: types.LIST_ALL_PREFERENTIAL,
    data
  }
}

export const changePagination = (pageNum) => {
  return {
    type: types.CHANGE_PAGINATION,
    pageNum
  }
}