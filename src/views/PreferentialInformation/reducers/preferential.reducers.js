import * as types from '../constants/ActionTypes';

const initialState = {
  preferentialList: [],
  pageNum: 1,
  total: 8
};

var preferentialData = (state = initialState, action) => {
  switch (action.type) {
    case types.LIST_ALL_PREFERENTIAL:
      return {
        ...state,
        preferentialList: action.data.list
      }
    case types.CHANGE_PAGINATION:
      return {
        ...state,
        pageNum: action.pageNum
      }
    default:
      return state;
  }
}
export default preferentialData