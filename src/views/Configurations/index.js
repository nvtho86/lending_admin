import Role from './Role';
import CheckProfiles from './CheckProfiles';
import Config from './Config';
import TypesOfIncentives from './TypesOfIncentives';
import Templates from './Templates';
import SmsTemplates from './SmsTemplates';

export {
  Role,CheckProfiles,Config,Templates,TypesOfIncentives, SmsTemplates,
};
