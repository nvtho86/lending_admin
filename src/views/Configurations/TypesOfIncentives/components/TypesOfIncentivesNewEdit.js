import React, { useState, useEffect, useContext } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg
} from "reactstrap";
import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";
import axios from "axios";

function TypesOfIncentivesNewEdit() {
  const ctxApi = useContext(contextAPI);

  const [redirectMain, setRedirectMain] = useState(false);
  const [updateState, setUpdateState] = useState("");
  const [fileName, setFileName] = useState("");
  const [promotionType, setPromotionType] = useState("");
  const [promotionName, setPromotionName] = useState("");
  const [backgroupImageLink, setBackgroupImageLink] = useState("");

  useEffect(() => {
    setUpdateState(ctxApi.stateNewUpdate);

    ctxApi.datasArr.filter(item => {
      if (item.id == ctxApi.stateNewUpdate) {
        setPromotionType(item.promotionType);
        setPromotionName(item.promotionName);
        setBackgroupImageLink(item.backgroupImageLink);
      }
    });
  }, []);

  if (redirectMain) {
    return <Redirect to={"/configurations/types-of-incentives"} />;
  }

  const onChangeImage = event => {
    if (event && event.target.files[0]) {
      var reader = new FileReader();
      var file = event.target.files[0];

      reader.onload = upload => {
        setBackgroupImageLink("");
        setFileName(upload.target.result);
        axios({
          method: "post",
          url: "http://192.168.75.205:8000/api/v1/authorize/upload_file",
          headers: {
            "Content-Type": "application/json",
            "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
            Authorization: "Bearer " + localStorage.getItem("token"),
            "x-environment": "WEB-ADMIN"
          },
          data: {
            payload: {
              files: [
                {
                  data: upload.target.result.replace(
                    "data:image/png;base64,",
                    ""
                  )
                }
              ]
            }
          }
        })
          .then(response => {
            setBackgroupImageLink(response.data.payload.files[0].data);
          })
          .catch(error => {});
      };
      reader.readAsDataURL(file);
    }
  };

  const handleChangePromotionName = event => {
    setPromotionName(event.target.value);
  };

  const submitHandle = () => {
    console.log("id : " + updateState);
    console.log("type : " + promotionType);
    console.log("name : " + promotionName);
    console.log("link : " + backgroupImageLink);
    axios({
      method: "post",
      url:
        "http://192.168.75.205:8000/api/v1/config_contract_type_background/image",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          id: parseInt(updateState),
          promotionType: parseInt(promotionType),
          promotionName,
          backgroupImageLink
        }
      }
    })
      .then(response => {
        setRedirectMain(true);
      })
      .catch(error => {});
  };

  return (
    <Card>
      <CardHeader>
        <h4>Cập nhật loại ưu đãi</h4>
      </CardHeader>
      <CardBody>
        <Form style={{ backgroundColor: "white" }}>
          <FormGroup row>
            <Label sm={2}>Loại ưu đãi</Label>
            <Col sm={10}>
              <Input
                value={promotionName}
                onChange={handleChangePromotionName}
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label sm={2}> Chọn hình đại diện</Label>
            <Col sm={2}>
              <label className="file-button" htmlFor="file">
                Chọn hình
                <Input
                  hidden
                  name="file"
                  id="file"
                  onChange={onChangeImage}
                  type="file"
                  accept="image/*"
                />
              </label>
            </Col>
            <Col sm={8}>{backgroupImageLink}</Col>
          </FormGroup>
          <FormGroup row>
            <Col sm={2}>
              <CardImg
                src={fileName === "" ? backgroupImageLink : fileName}
                id="img_upload_id"
                width="150px"
                height="150px"
              />
            </Col>
          </FormGroup>
        </Form>
      </CardBody>
      <CardFooter>
        <Button color="primary" onClick={submitHandle}>
          Lưu
        </Button>
        <Button
          color="secondary"
          className="ml-2"
          onClick={() => {
            setRedirectMain(true);
          }}
        >
          Hủy
        </Button>
      </CardFooter>
    </Card>
  );
}

export default TypesOfIncentivesNewEdit;
