import React, { useState, useEffect, useContext } from "react";
import { Table, Card, CardHeader, CardBody, Button } from "reactstrap";
import { Redirect } from "react-router-dom";
import axios from "axios";
import contextAPI from "../../../../ContextAPI/contextAPI";

function ListTypesOfIncentives() {
  const ctxApi = useContext(contextAPI);

  const [redirectNew, setRedirectNew] = useState(false);
  const [idUpdate, setIdUpdate] = useState(null);
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    // console.log("Use Effect...");
    axios({
      method: "post",
      url:
        "http://192.168.75.205:8000/api/v1/config_contract_type_background/list",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {}
      }
    })
      .then(response => {
        // console.log("Use Effect...");
        setDatas(response.data.payload);
      })
      .catch(error => {});
  }, []);

  if (redirectNew) {
    return <Redirect to={"/configurations/types-of-incentives-new"} />;
  }

  if (idUpdate) {
    return (
      <Redirect to={"/configurations/types-of-incentives-edit/" + idUpdate} />
    );
  }

  const incenLst = datas.map((data, index) => {
    return (
      <tr key={data.id}>
        <td className="text-center">{index + 1}</td>
        <td className="text-center">{data.promotionName}</td>
        <td className="text-center">
          <img width="40" height="40" src={data.backgroupImageLink} />
        </td>
        <td className="text-center">
          <Button
            color="warning"
            className="btn-edit"
            onClick={() => {
              setIdUpdate(data.id);
              ctxApi.handleDatasArr(datas, data.id);
            }}
          >
            <i className="fa fa-edit"></i>
          </Button>
        </td>
      </tr>
    );
  });

  return (
    <Card>
      <CardHeader>
        <h3 className="check">Cấu hình danh mục loại ưu đãi</h3>
      </CardHeader>
      <CardBody className="pb-0">
        <Table className="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th className="text-center" style={{ width: "5%" }}>
                STT
              </th>
              <th className="text-center" style={{ width: "60%" }}>
                Ưu đãi
              </th>
              <th className="text-center" style={{ width: "25%" }}>
                Hình ảnh
              </th>
              <th className="text-center" style={{ width: "10%" }}>
                Sửa
              </th>
            </tr>
          </thead>
          <tbody>{incenLst}</tbody>
        </Table>
      </CardBody>
    </Card>
  );
}
export default ListTypesOfIncentives;
