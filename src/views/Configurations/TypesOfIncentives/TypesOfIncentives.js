import React, { Component } from "react";
import ListTypesOfIncentives from "./components/ListTypesOfIncentives";

class TypesOfIncentives extends Component {
  render() {
    return (
      <div>
        <ListTypesOfIncentives />
      </div>
    );
  }
}

export default TypesOfIncentives;
