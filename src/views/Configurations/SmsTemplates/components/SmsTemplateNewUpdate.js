import React, { useState, useContext, useEffect } from "react";
import {
  Input,
  Button,
  Col,
  Row,
  Form,
  Label,
  FormGroup,
  Card,
  CardHeader,
  CardBody,
  CardFooter
} from "reactstrap";
import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";
import axios from "axios";

function SmsTemplateNewUpdate() {
  const ctxApi = useContext(contextAPI);

  const [redirectMain, setRedirectMain] = useState(false);
  const [smsType, setSmsType] = useState("");
  const [content, setContent] = useState("");
  const [status, setStatus] = useState("");
  const [updateState, setUpdateState] = useState("");

  useEffect(() => {
    // console.log("Use Effect...");
    if (ctxApi.stateNewUpdate !== "") {
      setUpdateState(ctxApi.stateNewUpdate);

      ctxApi.datasArr.filter(item => {
        if (item.id == ctxApi.stateNewUpdate) {
          setSmsType(item.smsType);
          setContent(item.content);
          setStatus(item.status);
        }
      });
    }
  }, []);

  const handleChangeSmsType = event => {
    setSmsType(event.target.value);
  };

  const handleChangeContent = event => {
    setContent(event.target.value);
  };

  const handleChangeExplain = event => {
    setStatus(event.target.value);
  };

  const handleSubmit = () => {
    if (updateState === "") {
      axios({
        method: "post",
        url: "http://192.168.75.205:8000/api/v1/sms/template/create",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
          Authorization: "Bearer " + localStorage.getItem("token"),
          "x-environment": "WEB-ADMIN"
        },
        data: {
          payload: {
            content: content,
            smsType: smsType,
            status: status,
            langCode: "vi"
          }
        }
      })
        .then(response => {
          setRedirectMain(true);
        })
        .catch(error => {});
    } else {
      axios({
        method: "post",
        url: "http://192.168.75.205:8000/api/v1/sms/template/update",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
          Authorization: "Bearer " + localStorage.getItem("token"),
          "x-environment": "WEB-ADMIN"
        },
        data: {
          payload: {
            uuid: updateState,
            content: content,
            type: smsType,
            status: status,
            langCode: "vi"
          }
        }
      })
        .then(response => {
          setRedirectMain(true);
        })
        .catch(error => {});
    }
  };

  if (redirectMain) {
    return <Redirect to={"/configurations/sms-template"} />;
  }

  return (
    <Card>
      <CardHeader>
        <h4>{updateState === "" ? "Thêm" : "Cập nhật"} nội dung SMS mẫu</h4>
      </CardHeader>
      <CardBody>
        <Form>
          <Col md={12}>
            <FormGroup>
              <Row>
                <Col md={2}>
                  <Label>Mã SMS</Label>
                </Col>
                <Col md={10}>
                  <Input
                    value={smsType}
                    onChange={handleChangeSmsType}
                  />
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Row>
                <Col md={2}>
                  <Label>Nội dung SMS</Label>
                </Col>
                <Col md={10}>
                  <Input
                    value={content}
                    onChange={handleChangeContent}
                  />
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Row>
                <Col md={2}>
                  <Label>Diễn giải</Label>
                </Col>
                <Col md={10}>
                  <Input
                    value={status}
                    onChange={handleChangeExplain}
                  />
                </Col>
              </Row>
            </FormGroup>
          </Col>
        </Form>
        <CardFooter>
          <Button className="mr-2" color="primary" onClick={handleSubmit}>
            Lưu
          </Button>
          <Button
            color="secondary"
            onClick={() => {
              setRedirectMain(true);
            }}
          >
            Hủy
          </Button>
        </CardFooter>
      </CardBody>
    </Card>
  );
}

export default SmsTemplateNewUpdate;
