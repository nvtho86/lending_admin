import React, { useState, useContext, useEffect } from "react";
import {
  Table,
  Button,
  Col,
  Row,
  Card,
  CardHeader,
  CardBody
} from "reactstrap";
import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";
import axios from "axios";

function SmsTemplateList() {
  const ctxApi = useContext(contextAPI);

  const [redirectNew, setRedirectNew] = useState(false);
  const [redirectUpdate, setRedirectUpdate] = useState(null);
  const [datas, setDatas] = useState([]);
  const [pageNum, setPageNum] = useState(1);
  const [pageSize, setPageSize] = useState(5);

  useEffect(() => {
    // console.log("Use Effect...");
    axios({
      method: "post",
      url: "http://192.168.75.205:8000/api/v1/sms/template/list",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          pageNum,
          pageSize: pageSize,
          direction: "DESC"
        }
      }
    })
      .then(response => {
        setDatas(response.data.payload.list);
        // console.log("Use Effect...");
      })
      .catch(error => {});
  }, [pageNum, pageSize]);

  const item = datas.map((data, index) => {
    return (
      <tr key={index}>
        <td style={{ width: "5%" }} className="text-center">
          {pageSize * (pageNum - 1) + (index + 1)}
        </td>
        <td style={{ width: "15%" }} className="text-center">
          {data.smsType}
        </td>
        <td style={{ width: "35%" }}>{data.content}</td>
        <td style={{ width: "35%" }}>{data.status}</td>
        <td style={{ width: "10%" }} className="text-center">
          <Button
            color="warning"
            className="btn-edit"
            onClick={() => {
              setRedirectUpdate(data.id);
              ctxApi.handleDatasArr(datas, data.id);
            }}
          >
            <i className="fa fa-edit"></i>
          </Button>
        </td>
      </tr>
    );
  });

  if (redirectNew) {
    return <Redirect to="/configurations/sms-template-new" />;
  }

  if (redirectUpdate) {
    return (
      <Redirect to={"/configurations/sms-template-edit/" + redirectUpdate} />
    );
  }

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xl={12}>
          <Card>
            <CardHeader>
              <h3 className="check">Nội dung SMS mẫu</h3>
            </CardHeader>
            <CardBody className="pb-0">
              <Row>
                <Col md={12} className="text-right mb-2">
                  <Button
                    color="primary"
                    onClick={() => {
                      setRedirectNew(true);
                      ctxApi.handleDatasArr(datas, "");
                    }}
                  >
                    Thêm mới
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div>
                    <Table className="table table-bordered table-striped table-hover">
                      <thead>
                        <tr>
                          <th className="text-center">STT</th>
                          <th className="text-center">Mã SMS</th>
                          <th className="text-center">Nội dung mẫu</th>
                          <th className="text-center">Diễn giải</th>
                          <th className="text-center">Sửa</th>
                        </tr>
                      </thead>
                      <tbody>{item}</tbody>
                    </Table>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default SmsTemplateList;
