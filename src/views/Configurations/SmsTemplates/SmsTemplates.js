import React, { Component } from "react";
import SmsTemplateList from "./components/SmsTemplateList";

class SmsTemplates extends Component {
  render() {
    return <SmsTemplateList />;
  }
}

export default SmsTemplates;
