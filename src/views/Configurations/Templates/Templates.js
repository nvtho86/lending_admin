import React, { Component } from "react";
import TemplateList from "./components/TemplateList";

class Templates extends Component {
  render() {
    return <TemplateList />;
  }
}

export default Templates;
