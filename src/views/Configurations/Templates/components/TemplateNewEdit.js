import React, { useState, useEffect, useContext } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button
} from "reactstrap";
import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";
import axios from "axios";

function TemplateNewEdit() {
  const ctxApi = useContext(contextAPI);

  const [redirectMain, setRedirectMain] = useState(false);
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [status, setStatus] = useState("");

  useEffect(() => {
    // console.log(ctxApi.stateNewUpdate);

    if (ctxApi.stateNewUpdate !== "") {
      ctxApi.datasArr.filter(item => {
        if (item.id == ctxApi.stateNewUpdate) {
          setTitle(item.title);
          setBody(item.body);
          setStatus(item.status);
        }
      });
    }
  }, []);

  if (redirectMain) {
    return <Redirect to={"/configurations/template"} />;
  }

  const handleChangeTitle = event => {
    setTitle(event.target.value);
  };

  const handleChangeBody = event => {
    setBody(event.target.value);
  };

  const handleChangeStatus = event => {
    setStatus(event.target.value);
  };

  const handleSubmit = () => {
    if (ctxApi.stateNewUpdate === "") {
      axios({
        method: "post",
        url: "http://192.168.75.205:8000/api/v1/notification/template/create",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
          Authorization: "Bearer " + localStorage.getItem("token"),
          "x-environment": "WEB-ADMIN"
        },
        data: {
          payload: {
            body,
            title,
            status: parseInt(status)
          }
        }
      })
        .then(response => {
          setRedirectMain(true);
        })
        .catch(error => {});
    } else {
      axios({
        method: "post",
        url: "http://192.168.75.205:8000/api/v1/notification/template/update",
        headers: {
          "Content-Type": "application/json",
          "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
          Authorization: "Bearer " + localStorage.getItem("token"),
          "x-environment": "WEB-ADMIN"
        },
        data: {
          payload: {
            id: ctxApi.stateNewUpdate,
            body,
            title,
            status: parseInt(status)
          }
        }
      })
        .then(response => {
          setRedirectMain(true);
        })
        .catch(error => {});
    }
  };

  return (
    <Card>
      <CardHeader>
        <h4>
          {ctxApi.stateNewUpdate !== "" ? "Cập nhật" : "Thêm mới"} nội dung
          thông báo mẫu
        </h4>
      </CardHeader>
      <CardBody>
        <Form>
          <Col md={12}>
            <FormGroup>
              <Row>
                <Col md={2}>
                  <Label>Tiêu đề</Label>
                </Col>
                <Col md={10}>
                  <Input value={title} onChange={handleChangeTitle} />
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Row>
                <Col md={2}>
                  <Label>Nội dung mẫu</Label>
                </Col>
                <Col md={10}>
                  <Input value={body} onChange={handleChangeBody} />
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Row>
                <Col md={2}>
                  <Label>Trạng thái</Label>
                </Col>
                <Col md={10}>
                  <Input value={status} onChange={handleChangeStatus} />
                </Col>
              </Row>
            </FormGroup>
          </Col>
        </Form>
      </CardBody>
      <CardFooter>
        <Button color="primary" className="mr-2" onClick={handleSubmit}>
          Lưu
        </Button>
        <Button
          color="secondary"
          onClick={() => {
            setRedirectMain(true);
          }}
        >
          Hủy
        </Button>
      </CardFooter>
    </Card>
  );
}

export default TemplateNewEdit;
