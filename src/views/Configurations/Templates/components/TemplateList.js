import React, { useState, useEffect, useContext } from "react";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  CardText
} from "reactstrap";

import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";
import axios from "axios";

function TemplateList() {
  const ctxApi = useContext(contextAPI);

  const [redirectNew, setRedirectNew] = useState(false);
  const [idUpdate, setIdUpdate] = useState(null);
  const [datas, setDatas] = useState([]);
  const [pageNum, setPageNum] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [wait, setWait] = useState(false);

  useEffect(() => {
    // console.log("Use Effect...");

    axios({
      method: "post",
      url: "http://192.168.75.205:8000/api/v1/notification/template/list",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          pageNum,
          pageSize: pageSize,
          direction: "DESC"
        }
      }
    })
      .then(response => {
        // console.log("Use Effect...");
        setDatas(response.data.payload.list);
        setWait(false);
      })
      .catch(error => {});
  }, [pageNum, pageSize]);

  if (redirectNew) {
    return <Redirect to={"/configurations/template-new"} />;
  }

  if (idUpdate) {
    return <Redirect to={"/configurations/template-edit/" + idUpdate} />;
  }

  const item = datas.map((data, index) => {
    return (
      <tr key={index}>
        <td className="text-center">
          {pageSize * (pageNum - 1) + (index + 1)}
        </td>
        <td>{data.title}</td>
        <td>{data.body}</td>
        <td className="text-center">{data.status}</td>
        <td className="text-center">
          <Button
            color="warning"
            className="btn-edit"
            onClick={() => {
              setIdUpdate(data.id);
              ctxApi.handleDatasArr(datas, data.id);
            }}
          >
            <i className="fa fa-edit"></i>
          </Button>
        </td>
      </tr>
    );
  });

  const listPagination = [1, 2, 3].map((val, index) => {
    return (
      <PaginationItem>
        <PaginationLink
          onClick={() => {
            if (val !== pageNum) {
              setPageNum(val);
              setWait(true);
            }
          }}
        >
          {val}
        </PaginationLink>
      </PaginationItem>
    );
  });

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xl={12}>
          <Card>
            <CardHeader>
              <h3 className="check">Nội dung thông báo mẫu</h3>
            </CardHeader>
            <CardBody className="pb-0">
              <Row>
                <Col md={12} className="text-right mb-2">
                  <Button
                    color="primary"
                    onClick={() => {
                      setRedirectNew(true);
                      ctxApi.handleDatasArr(datas, "");
                    }}
                  >
                    Thêm mới
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <div>
                    <Table className="table table-bordered table-striped table-hover">
                      <thead>
                        <tr>
                          <th style={{ width: "5%" }} className="text-center">
                            STT
                          </th>
                          <th style={{ width: "30%" }} className="text-center">
                            Tiêu đề
                          </th>
                          <th style={{ width: "30%" }} className="text-center">
                            Nội dung mẫu
                          </th>
                          <th style={{ width: "30%" }} className="text-center">
                            Trạng thái
                          </th>
                          <th style={{ width: "5%" }} className="text-center">
                            Sửa
                          </th>
                        </tr>
                      </thead>
                      <tbody>{item}</tbody>
                    </Table>{" "}
                    {wait && (
                      <CardText
                        style={{
                          color: "blue",
                          fontSize: "16px",
                          marginTop: "2%",
                          marginBottom: "2%"
                        }}
                      >
                        Đang lấy dữ liệu...
                      </CardText>
                    )}
                  </div>
                </Col>
              </Row>
              <Pagination aria-label="Page navigation example">
                <PaginationItem>
                  <PaginationLink
                    previous
                    onClick={() => {
                      if (pageNum > 1) {
                        setPageNum(pageNum - 1);
                        setWait(true);
                      }
                    }}
                  />
                </PaginationItem>
                {listPagination}
                <PaginationItem>
                  <PaginationLink
                    next
                    onClick={() => {
                      if (pageNum < datas.length / pageNum) {
                        setPageNum(pageNum + 1);
                        setWait(true);
                      }
                    }}
                  />
                </PaginationItem>
              </Pagination>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default TemplateList;
