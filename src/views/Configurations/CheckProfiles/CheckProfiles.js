import React, { useState, useEffect } from "react";
import {
  Table,
  CustomInput,
  Button,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody
} from "reactstrap";
import "../style.css";
import axios from "axios";

function ApprovalSchedule() {
  const [datas, setDatas] = useState([]);
  const [wait, setWait] = useState(false);

  useEffect(() => {
    axios({
      method: "post",
      url: "http://192.168.75.205:8000/api/v1/config_adjustment_contract/list",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {}
      }
    })
      .then(response => {
        setDatas(response.data.payload);
      })
      .catch(error => {});
  }, []);

  const onToggleCheckDoc = index => {
    let newDatas = [...datas];
    if (newDatas[index].isCheckDocument === 0) {
      newDatas[index].isCheckDocument = 1;
    } else newDatas[index].isCheckDocument = 0;

    setDatas(newDatas);
  };

  const onToggleEditDoc = index => {
    let newDatas = [...datas];
    if (newDatas[index].isAdjustmentDocument === 0) {
      newDatas[index].isAdjustmentDocument = 1;
    } else newDatas[index].isAdjustmentDocument = 0;

    setDatas(newDatas);
  };

  const resetFunc = () => {
    let newDatas = [...datas];

    newDatas.forEach(data => {
      data.isCheckDocument = 0;
      data.isAdjustmentDocument = 0;
    });

    setDatas(newDatas);
  };

  const submmitData = () => {
    setWait(true);
    axios({
      method: "post",
      url:
        "http://192.168.75.205:8000/api/v1/config_adjustment_contract/update",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          list: datas
        }
      }
    })
      .then(response => {
        setWait(false);
      })
      .catch(error => {});
  };

  const renderTodos = datas.map((todo, index) => {
    return (
      <tr key={index}>
        <td className="text-center">{index + 1}</td>
        <td>{todo.name}</td>
        <td className="text-center">
          <CustomInput
            type="checkbox"
            id={"chkbox_id_" + todo.id}
            name={"chkbox_name_" + todo.id}
            checked={todo.isCheckDocument === 1 ? true : false}
            onChange={() => onToggleCheckDoc(index)}
          />
        </td>
        <td className="text-center">
          <CustomInput
            type="checkbox"
            id={"chkbox_id_1_" + todo.id}
            name={"chkbox_name_1_" + todo.id}
            checked={todo.isAdjustmentDocument === 1 ? true : false}
            onChange={() => onToggleEditDoc(index)}
          />
        </td>
      </tr>
    );
  });

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xl={12}>
          <Card>
            <CardHeader>
              <h3>Cấu hình kiểm tra hồ sơ</h3>
            </CardHeader>
            <CardBody>
              <Table bordered striped hover>
                <thead className="text-center">
                  <tr>
                    <th style={{ width: "5%" }}>STT</th>
                    <th style={{ width: "45%" }}>Thông tin</th>
                    <th style={{ width: "20%" }}>Trưởng kiểm tra hồ sơ</th>
                    <th style={{ width: "30%" }}>
                      Trưởng điều chỉnh phụ lục hợp đồng
                    </th>
                  </tr>
                </thead>
                <tbody>{renderTodos}</tbody>
              </Table>
              <div>
                <div className="text-right">
                  {wait && (
                    <div
                      className="text-center"
                      style={{
                        color: "blue",
                        fontSize: "16px",
                        marginBottom: "2%"
                      }}
                    >
                      Đang lưu dữ liệu...
                    </div>
                  )}
                  &nbsp;
                  <Button
                    color="primary"
                    onClick={submmitData}
                    className="ml-2 mr-2"
                  >
                    Lưu
                  </Button>
                  &nbsp;
                  <Button type="reset" onClick={resetFunc} color="info">
                    Reset
                  </Button>
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default ApprovalSchedule;
