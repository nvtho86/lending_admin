import * as types from '../constants/ActionTypes';
import apiCaller from './../utils/apiCaller';

export const getListConfig = () => {
    return (dispatch) => {
        return apiCaller('list', 'POST',
            JSON.stringify(
                {
                    "payload": {
                    }
                }
            )
        ).then(response => {
            let data = {}
            if (response && response.data)
            {
                response.data.payload.forEach(item => {
                    Object.assign(data, {[item.key]: item})
                })
                dispatch(fetchListConfig(data))
            }
        })
    }
}

export const updateListConfig = (data) => {
    return (dispatch) => {
        return apiCaller('update', 'POST',
            JSON.stringify(
                {
                    "payload": data
                }
            )
        ).then(response => {
            let data = {}
            if (response && response.data)
            {
                response.data.payload.forEach(item => {
                    Object.assign(data, {[item.key]: item})
                })
                dispatch(fetchListConfig(data))
            }
        })
    }
}

export const fetchListConfig = (data) => {
    return {
      type: types.LIST_ALL_CONFIG,
      data
    }
}