import React, { Component } from "react"
import "react-datepicker/dist/react-datepicker.css"
import * as actions from './actions/config.action'
import { connect } from 'react-redux'
import {
  Col,
  Card,
  Row,
  Button,
  Label,
  Input,
  FormGroup,
  CardHeader,
  CardBody,
  CustomInput
} from "reactstrap";
import moment from "moment"
import "rc-time-picker/assets/index.css"
import ModalTimePicker from './components/ModalTimePicker'

class Config extends Component {
  constructor(props) {
    super(props);

    this.state = {
      configList: {
        esign_from: {
          id: 1,
          key: "esign_from",
          label: "Từ",
          value: "12:00",
          valuePara: "",
          timePara: "",
        },
        esign_to: {
          id: 2,
          key: "esign_to",
          label: "Đến",
          value: "12:00",
          valuePara: "",
          timePara: "",
        },
        otp_expired: {
          id: 3,
          key: "otp_expired",
          label: "Thời gian hết hạn OTP",
          value: "0",
          valuePara: "m",
          timePara: "m",
        },
        time_out_mobile_app: {
          id: 4,
          key: "time_out_mobile_app",
          label: "Mobile App",
          value: "0",
          valuePara: "mm",
          timePara: "d,mm",
        },
        time_out_web_admin: {
          id: 5,
          key: "time_out_web_admin",
          label: "Web Admin",
          value: "0",
          valuePara: "m",
          timePara: "m",
        },
        time_out_web_esign_portal: {
          id: 6,
          key: "time_out_web_esign_portal",
          label: "Esign Portal",
          value: "0",
          valuePara: "m",
          timePara: "m",
        },
        time_out_change_password: {
          id: 7,
          key: "time_out_change_password",
          label: "Thay đổi mật khẩu",
          value: "",
          valuePara: "mm",
          timePara: "d,mm",
        },
        customerinfo_adjustment_send_time: {
          id: 8,
          key: "customerinfo_adjustment_send_time",
          label: "Thời gian gửi mail",
          value: "12:00",
          valuePara: "",
          timePara: "",
        },
        customerinfo_adjustment_mail_list: {
          id: 9,
          key: "customerinfo_adjustment_mail_list",
          label: "Danh sách nhận mail",
          value: "",
          valuePara: "",
          timePara: "",
        },
        loan_sign_send_time: {
          id: 10,
          key: "loan_sign_send_time",
          label: "Thời gian gửi mail",
          value: "12:00",
          valuePara: "",
          timePara: "",
        },
        loan_sign_mail_list: {
          id: 11,
          key: "loan_sign_mail_list",
          label: "Danh sách nhận mail",
          value: "",
          valuePara: "",
          timePara: "",
        },
        contract_type_ed_begin_with: {
          id: 12,
          key: "contract_type_ed_begin_with",
          label: "Loại ED bắt đầu bằng các ký tự",
          value: "",
          valuePara: "",
          timePara: "",
        },
        contract_type_mc_begin_with: {
          id: 13,
          key: "contract_type_mc_begin_with",
          label: "Loại MC bắt đầu bằng các ký tự",
          value: "",
          valuePara: "",
          timePara: "",
        },
        contract_type_cl_begin_with: {
          id: 14,
          key: "contract_type_cl_begin_with",
          label: "Loại CL bắt đầu bằng các ký tự",
          value: "",
          valuePara: "",
          timePara: "",
        },
        contract_type_clo_begin_with: {
          id: 15,
          key: "contract_type_clo_begin_with",
          label: "Loại CLO bắt đầu bằng các ký tự",
          value: "",
          valuePara: "",
          timePara: "",
        }
      },

      isOpenTimeModal: false,
      timeModal: '',
      timeNow: moment().format('HH:mm'),
      tempNamePicker: '',
      listOtp: [1, 2, 3, 4],
      isLoaded: false,
      isDisableSaveBtn: false
    }
  }

  componentDidMount() {
    new Promise((resolve) => {
      this.props.onLoadConfig()
      resolve();
    })
    .then(() => {
      this.setState({
        isLoaded: true
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    const { configData } = nextProps
    const { configList } = configData
    if (configData && Object.entries(configList).length !== 0)
    {
      this.setState({
        configList: configList
      })
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.isLoaded) {
      if (JSON.stringify(this.state.configList) != JSON.stringify(nextState.configList)) {
        if (!this.state.isDisableSaveBtn) {
          this.setState({
            isDisableSaveBtn: true
          })
        }
      }
      else {
        if (this.state.isDisableSaveBtn) {
          this.setState({
            isDisableSaveBtn: false
          })
        }
      }
    }
  }

  handleChange = e => {
    let { name, value } = e.target;
    this.setState({
      [name]: value
    })
  }

  handleChangeConfigList = e => {
    let { name, value } = e.target
    const param = e.target.getAttribute("param")
    var property = param ? param : "value"
    this.setState(prevState => ({
      configList:{
        ...prevState.configList,
        [name]: {
          ...prevState.configList[name],
          [property]: value
        }
      }
    }))
  }

  onChangeTimePicker = (data) => {
    this.setState({displayStartTime: data})
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  handleInputKeydown = event => {
    const { value, name, type } = event.target
    if (event.key === 'Enter') {

      if (type && type == 'email') 
        if (!this.validateEmail(value))
          return
      
      let tags = this.state.configList[name].value
      if (!tags.includes(value)) {
        tags += `${tags ? ';' : ''}${value}`
        this.setState(prevState => ({
          configList: {
            ...prevState.configList,
            [name]: {
              ...prevState.configList[name],
              value: tags
            }
          }
        }))
      }
      event.target.value = ''
    }
  }

  deleteTags = (name, value) => {
    let val = this.state.configList[name].value
    if (val) {
      val = val.includes(`;${value}`)  ? val.replace(`;${value}`, '') 
          : val.includes(`${value};`)  ? val.replace(`${value};`, '')
          : val.includes(`${value}`)   ? val.replace(`${value}`, '') 
          : val

      this.setState(prevState => ({
        configList: {
          ...prevState.configList,
          [name]: {
            ...prevState.configList[name],
            value: val
          }
        }
      }))
    }
  }

  openTimePickerModal = () => {
    this.setState({
      isOpenTimeModal: true
    })
  }

  handleTimePicker = namePicker => () => {
    this.setState({
      tempNamePicker: namePicker,
      timeModal: this.state.configList[namePicker] ? this.state.configList[namePicker].value : this.state.timeNow
    }, () => this.setState({isOpenTimeModal: true}))
  }

  handleTimeFromModal(timeConvert) {
    const temp = timeConvert.split(' ')
    const type = temp[1]
    const time = temp[0].split(':')
    return (type.toLowerCase() == 'am' ? (time[0] == '12' ? '00' : ('0' + time[0]).slice(-2)) : (time[0] < 12 ? parseInt(time[0]) + 12 : time[0])) + ':' + time[1]
  }

  changeTimePicker = time => {
    const name = this.state.tempNamePicker
    const timeConvert = this.handleTimeFromModal(time)
    this.setState(prevState => ({
      configList:{
        ...prevState.configList,
        [name]: {
          ...prevState.configList[name],
          value: timeConvert
        }
      },
      timeModal: timeConvert
    }), () => this.setState({isOpenTimeModal: false}))
  }

  formatTo24Hour(timeConvert) {
    if (timeConvert)
    {
      const temp = timeConvert.split(':')
      const hours = temp[0]
      return hours < 10 && hours.split('')[0] != 0 ? '0' + timeConvert : timeConvert
    }
    return;
  }

  convertStringToListByName = (name, property) => {
    if (this.state.configList[name][property]) {
      let items = this.state.configList[name][property]
      return items.split(property === "value" ? ';' : ',')
    }
    return []
  }

  onUpdateConfig = () => {
    let temp = this.state.configList
    let list = Object.keys(this.state.configList).map(function(key) {
      return temp[key]
    })
    this.props.onUpdateConfig({list})
  }

  render() {
    const { 
      esign_from,
      esign_to,
      otp_expired,
      time_out_mobile_app,
      time_out_web_admin,
      time_out_web_esign_portal,
      time_out_change_password,
      customerinfo_adjustment_send_time,
      loan_sign_send_time,
      contract_type_ed_begin_with,
      contract_type_mc_begin_with,
      contract_type_cl_begin_with,
      contract_type_clo_begin_with
    } = this.state.configList

    const { listOtp } = this.state
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md={6}>
            <Card>
              <CardHeader>
                <strong>Thời gian ký Esign</strong>
              </CardHeader>
              <CardBody className="pb-0">
                <FormGroup row>
                  <Label className="pr-0" sm={1}>{esign_from.label}</Label>
                  <Col sm={5}>
                    <div className="box-time-picker">
                      <Input type="time" name="esign_from" value={this.formatTo24Hour(esign_from.value)} onChange={this.handleChangeConfigList} />
                      <Button onClick={this.handleTimePicker("esign_from")}><i className="fa fa-clock-o"></i></Button>
                    </div>
                  </Col>
                  <Label className="pr-0" sm={1}>{esign_to.label}</Label>
                  <Col sm={5}>
                    <div className="box-time-picker">
                      <Input type="time" name="esign_to" value={this.formatTo24Hour(esign_to.value)}  onChange={this.handleChangeConfigList} />
                      <Button onClick={this.handleTimePicker("esign_to")}><i className="fa fa-clock-o"></i></Button>
                    </div>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
            <Card>
              <CardHeader>
                <strong>{otp_expired.label}</strong>
              </CardHeader>
              <CardBody className="pb-0">
                <FormGroup row>
                  {  
                    listOtp.map((e, i) => {
                      return (
                          <Col sm={3} key={i}>
                            <CustomInput id={i} type="radio" name="otp_expired" value={e} label={e + (otp_expired && otp_expired.valuePara == "m" ? " Phút" : "Giờ")} checked={otp_expired && otp_expired.value ? e == otp_expired.value : false} onChange={this.handleChangeConfigList} />
                          </Col>
                        )
                    })
                  }
                </FormGroup>
              </CardBody>
            </Card>
            
          </Col>
          <Col md={6}>
            <Card>
              <CardHeader>
                <strong>Thiết lập thời gian timeout</strong>
              </CardHeader>
              <CardBody className="pb-2">
                <FormGroup row className="mb-2">
                  <Label sm={4}>
                    {time_out_mobile_app && time_out_mobile_app.label ? time_out_mobile_app.label : "Mobile App"}
                  </Label>
                  <Col sm={4}>
                    <Input
                      type="number"
                      name="time_out_mobile_app"
                      value={time_out_mobile_app.value}
                      onChange={this.handleChangeConfigList}
                    />
                  </Col>
                  <Col sm={4}>
                    <Input disabled={this.convertStringToListByName("time_out_mobile_app", "timePara").length <= 1} type="select" name="time_out_mobile_app" value={time_out_mobile_app.valuePara} onChange={this.handleChangeConfigList} param="valuePara">
                      {
                        this.convertStringToListByName("time_out_mobile_app", "timePara").map((e, i) => {
                          return <option key={i} value={e}>{e === "mm" ? "Tháng" : "Ngày"}</option>
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row className="mb-2">
                  <Label sm={4}>
                    {time_out_web_esign_portal.label || "Esign Portal"}
                  </Label>
                  <Col sm={4}>
                    <Input
                      type="number"
                      name="time_out_web_esign_portal"
                      onChange={this.handleChangeConfigList}
                      defaultValue={time_out_web_esign_portal.value}
                    />
                  </Col>
                  <Col sm={4}>
                    <Input disabled={this.convertStringToListByName("time_out_web_esign_portal", "timePara").length <= 1} type="select" onChange={this.handleChangeConfigList} defaultValue={time_out_web_esign_portal.valuePara} name="time_out_web_esign_portal">
                      {
                        this.convertStringToListByName("time_out_web_esign_portal", "timePara").map((e, i) => {
                          return <option key={i} value={e}>{e === "m" ? "Phút" : "Giờ"}</option>
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row className="mb-2">
                  <Label sm={4}>
                    {time_out_web_admin.label || "Web Admin"}
                  </Label>
                  <Col sm={4}>
                    <Input
                      type="number"
                      name="time_out_web_admin"
                      onChange={this.handleChangeConfigList}
                      defaultValue={time_out_web_admin.value}
                    />
                  </Col>
                  <Col sm={4}>
                    <Input disabled={this.convertStringToListByName("time_out_web_admin", "timePara").length <= 1} type="select" name="time_out_web_admin" defaultValue={time_out_web_admin.valuePara} onChange={this.handleChangeConfigList} param="valuePara">
                      {
                        this.convertStringToListByName("time_out_web_admin", "timePara").map((e, i) => {
                          return <option key={i} value={e}>{e === "m" ? "Phút" : "Giờ"}</option>
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row className="mb-2">
                  <Label className="pr-0" sm={4}>
                    {time_out_change_password.label || "Thời gian đổi mật khẩu"}
                  </Label>
                  <Col sm={4}>
                    <Input
                      type="number"
                      name="time_out_change_password"
                      onChange={this.handleChangeConfigList}
                      defaultValue={time_out_change_password.value}
                    />
                  </Col>
                  <Col sm={4}>
                    <Input disabled={this.convertStringToListByName("time_out_change_password", "timePara").length <= 1} type="select" name="time_out_change_password" value={time_out_change_password.valuePara} onChange={this.handleChangeConfigList} param="valuePara">
                      {
                        this.convertStringToListByName("time_out_change_password", "timePara").map((e, i) => {
                          return <option key={i} value={e}>{e === "mm" ? "Tháng" : "Ngày"}</option>
                        })
                      }
                    </Input>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col md={12}>
            <Card>
              <CardHeader>
                <strong>Cấu hình gửi báo cáo điều chỉnh thông tin khách hàng</strong>
              </CardHeader>
              <CardBody className="pb-0">
                <FormGroup row>
                  <Col sm={3}>
                    <Label>
                      Thời gian gửi mail
                    </Label>
                    <div className="box-time-picker">
                      <Input type="time" name="customerinfo_adjustment_send_time" value={this.formatTo24Hour(customerinfo_adjustment_send_time.value) || this.state.timeNow} onChange={this.handleChangeConfigList} />
                      <Button onClick={this.handleTimePicker("customerinfo_adjustment_send_time")}><i className="fa fa-clock-o"></i></Button>
                    </div>
                  </Col>
                  <Col sm={9}>
                    <Label>Danh sách nhận mail</Label>
                    <Label className="input-list-element">
                      {
                        this.convertStringToListByName("customerinfo_adjustment_mail_list", "value").map((e, i) => {
                          return (
                            <span key={i}>{e} <b onClick={() => this.deleteTags("customerinfo_adjustment_mail_list", e)}><i className="fa fa-close"></i></b></span>
                          )
                        })
                      }
                      <Input
                        className="input-config-constract"
                        name="customerinfo_adjustment_mail_list"
                        onKeyDown={this.handleInputKeydown}
                        type="email"
                      />
                    </Label>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col md={12}>
            <Card>
              <CardHeader>
                <strong>Cấu hình gửi thông tin đăng ký vay</strong>
              </CardHeader>
              <CardBody className="pb-0">
                <FormGroup row>
                  <Col sm={3}>
                    <Label>
                      Thời gian gửi mail
                    </Label>
                      <div className="box-time-picker">
                        <Input type="time" name="loan_sign_send_time" value={this.formatTo24Hour(loan_sign_send_time.value) || this.state.timeNow} onChange={this.handleChangeConfigList} />
                        <Button onClick={this.handleTimePicker("loan_sign_send_time")}><i className="fa fa-clock-o"></i></Button>
                      </div>
                  </Col>
                  <Col sm={9}>
                    <Label htmlFor="examplePassword">
                      Danh sách nhận mail
                    </Label>
                    <Label className="input-list-element">
                      {
                        this.convertStringToListByName("loan_sign_mail_list", "value").map((e, i) => {
                          return (
                            <span key={i}>{e} <b onClick={() => this.deleteTags("loan_sign_mail_list", e)}><i className="fa fa-close"></i></b></span>
                          )
                        })
                      }
                      <Input 
                        className="input-config-constract"
                        name="loan_sign_mail_list"
                        onKeyDown={this.handleInputKeydown}
                      />
                    </Label>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col md={12}>
            <Card>
              <CardHeader>
                <strong>Cấu hình mã hợp đồng</strong>
              </CardHeader>
              <CardBody className="pb-0">
                <FormGroup row>
                  <Label sm={3}>
                    {contract_type_ed_begin_with.label}
                  </Label>
                  <Col sm={9}>
                    <Label className="input-list-element">
                      {
                        this.convertStringToListByName("contract_type_ed_begin_with", "value").map((e, i) => {
                          return (
                            <span key={i}>{e} <b onClick={() => this.deleteTags("contract_type_ed_begin_with", e)}><i className="fa fa-close"></i></b></span>
                          )
                        })
                      }
                      <Input
                        className="input-config-constract"
                        name="contract_type_ed_begin_with"
                        onKeyDown={this.handleInputKeydown}
                      />
                    </Label>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={3}>
                    {contract_type_mc_begin_with.label}
                  </Label>
                  <Col sm={9}>
                    <Label className="input-list-element">
                      {
                        this.convertStringToListByName("contract_type_mc_begin_with", "value").map((e, i) => {
                          return (
                            <span key={i}>{e} <b onClick={() => this.deleteTags("contract_type_mc_begin_with", e)}><i className="fa fa-close"></i></b></span>
                          )
                        })
                      }
                      <Input
                        className="input-config-constract"
                        name="contract_type_mc_begin_with"
                        onKeyDown={this.handleInputKeydown}
                      />
                    </Label>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={3}>
                    {contract_type_cl_begin_with.label}
                  </Label>
                  <Col sm={9}>
                    <Label className="input-list-element">
                      {
                        this.convertStringToListByName("contract_type_cl_begin_with", "value").map((e, i) => {
                          return (
                            <span key={i}>{e} <b onClick={() => this.deleteTags("contract_type_cl_begin_with", e)}><i className="fa fa-close"></i></b></span>
                          )
                        })
                      }
                      <Input
                        className="input-config-constract"
                        name="contract_type_cl_begin_with"
                        onKeyDown={this.handleInputKeydown}
                      />
                    </Label>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label sm={3}>
                    {contract_type_clo_begin_with.label}
                  </Label>
                  <Col sm={9}>
                    <Label className="input-list-element">
                      {
                        this.convertStringToListByName("contract_type_clo_begin_with", "value").map((e, i) => {
                          return (
                            <span key={i}>{e} <b onClick={() => this.deleteTags("contract_type_clo_begin_with", e)}><i className="fa fa-close"></i></b></span>
                          )
                        })
                      }
                      <Input
                        className="input-config-constract"
                        name="contract_type_clo_begin_with"
                        onKeyDown={this.handleInputKeydown}
                      />
                    </Label>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
            <FormGroup className="text-right btn-form-config">
              <Button
                type="submit"
                onClick={this.onUpdateConfig}
                color="danger"
              >
                Lưu
              </Button>
            </FormGroup>
          </Col>
        </Row>
        <ModalTimePicker
          isOpenTimeModal={this.state.isOpenTimeModal}
          isCloseTimeModal={() => this.setState({isOpenTimeModal: !this.state.isOpenTimeModal})}
          timeProps = {this.state.timeModal}
          changeTimePicker={this.changeTimePicker}
        >
        </ModalTimePicker>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    configData: state.configData
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onLoadConfig: () => {
      dispatch(actions.getListConfig())
    },
    onUpdateConfig:(data) => {
      dispatch(actions.updateListConfig(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Config)
