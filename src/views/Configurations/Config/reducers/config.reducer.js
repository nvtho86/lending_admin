import * as types from './../constants/ActionTypes';

const initialState = {
  configList: {
    
  },
};

var ConfigData = (state = initialState, action) => {
  switch (action.type) {
    case types.LIST_ALL_CONFIG:
      return {
        ...state,
        configList: action.data
      }
    default:
      return state;
  }
}
export default ConfigData;
