import React, { Component } from 'react'
import {
    Button, Card, CardBody, CardFooter,
} from 'reactstrap'
import TimeKeeper from 'react-timekeeper'
import moment from "moment"

class ModalTimePicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timePicker: moment().startOf('day').format('LT')
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.timeProps)
        {
            const value = nextProps.timeProps
            const temp = value.split(':')
            const hour = temp[0] > 12 ? temp[0] - 12 : temp[0]
            const minute = temp[1]
            const type = temp[0] >= 12 ? 'PM' : 'AM'
            
            this.setState({
                timePicker: `${hour}:${minute} ${type}`
            })
        }
    }

    changeTimePickerModal = (value) => {
        this.setState({
            timePicker: value.formatted
        })
    }

    render() {
        const { isOpenTimeModal } = this.props   
        const { timePicker } = this.state
        return (
            <div className={`time-picker-modal ${isOpenTimeModal ? "open" : ""}`}>
                <Card>
                    <CardBody>
                        <TimeKeeper time={timePicker} onChange={this.changeTimePickerModal} />
                    </CardBody>
                    <CardFooter>
                        <Button color="danger" onClick={() => this.props.changeTimePicker(timePicker)}>Lưu</Button>
                        <Button className="float-right" color="primary" onClick={this.props.isCloseTimeModal}>Hủy bỏ</Button>
                    </CardFooter>
                </Card>
            </div>
        )
    }
}

export default ModalTimePicker;