import React, { useState, useEffect, useContext } from "react";
import {
  Table,
  Button,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Card,
  CardHeader,
  CardBody,
  CardText
} from "reactstrap";
import axios from "axios";
import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";

function ListRole() {
  const ctxApi = useContext(contextAPI);

  const [datas, setDatas] = useState([]);
  const [idNew, setIdNew] = useState(false);
  const [idUpdate, setIdUpdate] = useState(false);
  const [pageNum, setPageNum] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [wait, setWait] = useState(false);
  const [countElement, setCountElement] = useState(0);

  useEffect(() => {
    setWait(true);
    axios({
      method: "post",
      url: "http://192.168.75.205:8000/api/v1/role/pagination",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          pageNum,
          pageSize,
          direction: "ASC"
        }
      }
    })
      .then(response => {
        setDatas(response.data.payload[0].list);
        setCountElement(response.data.payload[0].count);
        setWait(false);
      })
      .catch(error => {});
  }, [pageNum, pageSize]);

  const editClick = id => {
    setIdUpdate(id);
  };

  if (idNew) {
    return <Redirect to={"/configurations/new_role"} />;
  }

  if (idUpdate) {
    return <Redirect to={"/configurations/edit_role/" + idUpdate} />;
  }

  const renderTodos = datas.map((todo, index) => {
    return (
      <tr key={index}>
        <td className="text-center">
          {pageSize * (pageNum - 1) + (index + 1)}
        </td>
        <td className="text-center">{todo.role}</td>
        <td>{todo.name}</td>
        <td className="text-center">
          <Button
            color="warning"
            className="btn-edit"
            onClick={() => {
              editClick(todo.id);
              ctxApi.handleDatasArr(datas, todo.id);
            }}
          >
            <i className="fa fa-edit"></i>
          </Button>
        </td>
        <td className="text-center">
          <Button color="danger" onClick={() => {}}>
            <i className="fa fa-trash-o"></i>
          </Button>
        </td>
      </tr>
    );
  });

  const listPagination = [pageNum].map((val, index) => {
    if (val === pageNum) {
      return (
        <PaginationItem active>
          <PaginationLink>{val}</PaginationLink>
        </PaginationItem>
      );
    }
    //  else {
    //   return (
    //     <PaginationItem>
    //       <PaginationLink
    //         onClick={() => {
    //           setPageNum(val);
    //           setWait(true);
    //         }}
    //       >
    //         {val}
    //       </PaginationLink>
    //     </PaginationItem>
    //   );
    // }
  });

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xl={12}>
          <Card>
            <CardHeader>
              <h3>Danh sách Phân quyền</h3>
            </CardHeader>

            <CardBody className="pb-0">
              <div className="text-right">
                <Button
                  className="mb-3"
                  color="primary"
                  onClick={() => {
                    setIdNew(true);
                    ctxApi.handleDatasArr(datas, "");
                  }}
                >
                  Thêm mới
                </Button>
              </div>
              <Table className="table table-bordered table-striped table-hover">
                <thead>
                  <tr className="text-center">
                    <th style={{ width: "5%" }}>STT</th>
                    <th style={{ width: "15%" }}>Mã</th>
                    <th style={{ width: "70%" }}>Tên</th>
                    <th style={{ width: "5%" }}>Sửa</th>
                    <th style={{ width: "5%" }}>Xóa</th>
                  </tr>
                </thead>
                <tbody>{renderTodos}</tbody>
              </Table>
              <Pagination aria-label="Page navigation example">
                <PaginationItem>
                  <PaginationLink
                    previous
                    onClick={() => {
                      if (pageNum > 1) {
                        setPageNum(pageNum - 1);
                        setWait(true);
                      }
                    }}
                  />
                </PaginationItem>
                {listPagination}
                <PaginationItem>
                  <PaginationLink
                    next
                    onClick={() => {
                      if (pageNum < countElement / pageSize) {
                        setPageNum(pageNum + 1);
                        setWait(true);
                      }
                    }}
                  />
                </PaginationItem>
                {wait && (
                  <PaginationItem className="ml-3">
                    <CardText
                      style={{
                        color: "blue",
                        fontSize: "16px",
                        marginTop: "2%",
                        marginBottom: "2%"
                      }}
                    >
                      Đang lấy dữ liệu...
                    </CardText>
                  </PaginationItem>
                )}
              </Pagination>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default ListRole;
