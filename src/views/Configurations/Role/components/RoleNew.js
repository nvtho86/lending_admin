import React, { useState } from "react";

import {
  Button,
  Form,
  FormGroup,
  Input,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardText
} from "reactstrap";
import { Redirect } from "react-router-dom";
import axios from "axios";

function RoleNew() {
  const [redirectMain, setRedirectMain] = useState(null);
  const [role, setRole] = useState("");
  const [name, setName] = useState("");
  const [wait, setWait] = useState(false);

  const handleCancleClick = () => {
    setRedirectMain(true);
  };

  const handleAddClick = () => {
    setWait(true);
    axios({
      method: "post",
      url: "http://192.168.75.205:8000/api/v1/role/post",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          role: role,
          name: name
        }
      }
    })
      .then(response => {
        setRedirectMain(true);
        setWait(false);
      })
      .catch(error => {});
  };

  if (redirectMain) {
    return <Redirect to="/configurations/role" />;
  }

  return (
    <Card>
      <CardHeader>
        <h4>Thêm thông tin vai trò</h4>
      </CardHeader>
      <CardBody>
        <Form>
          <FormGroup row>
            <Col sm={12}>
              <h5>Thông tin cập nhật</h5>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col sm={3}>
              <Input
                placeholder="Mã"
                name="role"
                onChange={e => setRole(e.target.value)}
                value={role}
              />
            </Col>
            <Col sm={3}>
              <Input
                placeholder="Tên"
                name="name"
                onChange={e => setName(e.target.value)}
                value={name}
              />
            </Col>
            <Col sm={2}>
              <Button color="primary" onClick={handleAddClick}>
                Lưu
              </Button>
              <Button
                className="ml-2"
                color="secondary"
                onClick={handleCancleClick}
              >
                Hủy
              </Button>
            </Col>
            <Col sm={2}>
              {wait && (
                <CardText
                  style={{
                    color: "blue",
                    fontSize: "16px",
                    marginTop: "2%",
                    marginBottom: "2%"
                  }}
                >
                  Đang lưu dữ liệu...
                </CardText>
              )}
            </Col>
          </FormGroup>
        </Form>
      </CardBody>
    </Card>
  );
}

export default RoleNew;
