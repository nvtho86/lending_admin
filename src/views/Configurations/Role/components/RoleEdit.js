import React, { useState, useEffect, useContext } from "react";
import {
  Table,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CustomInput,
  Row,
  CardText
} from "reactstrap";
import axios from "axios";
import { Redirect } from "react-router-dom";
import contextAPI from "../../../../ContextAPI/contextAPI";

function RoleEdit() {
  const ctxApi = useContext(contextAPI);

  const [role, setRole] = useState("");
  const [name, setName] = useState("");
  const [roleView, setRoleView] = useState("");
  const [nameView, setNameView] = useState("");
  const [redirectMain, setRedirectMain] = useState(null);
  const [roleAuthorizeRequest, setRoleAuthorizeRequest] = useState([]);
  const [wait, setWait] = useState(false);

  useEffect(() => {
    // console.log("Use Effect...");

    ctxApi.datasArr.filter(item => {
      if (item.id == ctxApi.stateNewUpdate) {
        setRole(item.role);
        setName(item.name);
        setRoleView(item.role);
        setNameView(item.name);

        axios({
          method: "post",
          url: "http://192.168.75.205:8000/api/v1/role_authorize/list",
          headers: {
            "Content-Type": "application/json",
            "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
            Authorization: "Bearer " + localStorage.getItem("token"),
            "x-environment": "WEB-ADMIN"
          },
          data: {
            payload: {
              roleCode: item.role,
              roleId: parseInt(ctxApi.stateNewUpdate),
              roleName: item.name
            }
          }
        })
          .then(response => {
            setRoleAuthorizeRequest(
              response.data.payload.listRoleAuthorizeRespon
            );
          })
          .catch(error => {});
      }
    });
  }, []);

  const updateClick = () => {
    setWait(true);
    axios({
      method: "post",
      url: "http://192.168.75.205:8000/api/v1/role_authorize/update",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
        Authorization: "Bearer " + localStorage.getItem("token"),
        "x-environment": "WEB-ADMIN"
      },
      data: {
        payload: {
          listRoleAuthorizeRespon: roleAuthorizeRequest,
          roleAuthorizeRequest: {
            roleId: ctxApi.stateNewUpdate,
            roleCode: role,
            roleName: name
          }
        }
      }
    })
      .then(response => {
        setRedirectMain(true);
        setWait(false);
      })
      .catch(error => {});
  };

  const onToggleRole = index => {
    let arrData = [...roleAuthorizeRequest];
    if (arrData[index].status === 0) {
      arrData[index].status = 1;
    } else arrData[index].status = 0;

    setRoleAuthorizeRequest(arrData);
  };

  const roleArr = roleAuthorizeRequest.map((data, index) => {
    return (
      <tr key={index}>
        <td className="text-center">{index}</td>
        <td>{data.menu}</td>
        <td>{data.crud}</td>
        <td className="text-center">
          <CustomInput
            type="checkbox"
            id={"chkbox_id_" + index}
            name={"chkbox_name_" + index}
            checked={data.status === 1 ? true : false}
            onChange={() => onToggleRole(index)}
          />
        </td>
      </tr>
    );
  });

  if (redirectMain) {
    return <Redirect to="/configurations/role" />;
  }

  return (
    <Card>
      <CardHeader>
        <h4>Cập nhật thông tin vai trò</h4>
      </CardHeader>
      <CardBody>
        <Form>
          <Row form>
            <Col md={4}>
              <FormGroup>
                <Row className="ml-0">
                  <h5>Thông tin vai trò</h5>
                </Row>
                <Row>
                  <Label sm={3} className="mb-2">
                    Mã
                  </Label>
                  <Label sm={9}>{roleView}</Label>
                </Row>
                <Row>
                  <Label sm={3} className="mb-2">
                    Tên
                  </Label>
                  <Label sm={9}>{nameView}</Label>
                </Row>
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Row className="ml-0">
                  <h5>Thông tin cập nhật</h5>
                </Row>
                <Row>
                  <Label sm={3} className="mb-2">
                    Mã
                  </Label>
                  <Col sm={9}>
                    <Input
                      name="role"
                      onChange={e => setRole(e.target.value)}
                      value={role}
                    />
                  </Col>
                </Row>
                <Row>
                  <Label sm={3} className="mb-2">
                    Tên
                  </Label>
                  <Col sm={9}>
                    <Input
                      name="name"
                      onChange={e => setName(e.target.value)}
                      value={name}
                    />
                  </Col>
                </Row>
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <h5>Thông tin quyền menu</h5>
          </FormGroup>
          <FormGroup>
            <Table className="table table-sm table-bordered table-striped table-hover">
              <thead>
                <tr className="text-center">
                  <th style={{ width: "10%" }}>STT</th>
                  <th style={{ width: "30%" }}>Menu</th>
                  <th style={{ width: "50%" }}>Hành động</th>
                  <th style={{ width: "10%" }}>Check</th>
                </tr>
              </thead>
              <tbody>{roleArr}</tbody>
            </Table>
          </FormGroup>
        </Form>
      </CardBody>
      <CardFooter>
        <div>
          {wait && (
            <CardText
              className="text-center"
              style={{
                color: "blue",
                fontSize: "16px",
                marginTop: "2%",
                marginBottom: "2%",
                marginRight: "2%"
              }}
            >
              Đang lưu dữ liệu...
            </CardText>
          )}
          <div className="text-right">
            <Button color="primary" className="mr-2" onClick={updateClick}>
              Lưu
            </Button>
            <Button
              color="secondary"
              onClick={() => {
                setRedirectMain(true);
              }}
            >
              Hủy
            </Button>
          </div>
        </div>
      </CardFooter>
    </Card>
  );
}

export default RoleEdit;
