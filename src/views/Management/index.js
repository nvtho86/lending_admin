import Contract from './Contract/contract-list';
import Administration from './Administration';
import Customer from './Customers/Customers';
import Notification from './Notification/Notification';
import Configuration from './Configuration';

export {
    Administration, Customer, Contract, Notification, Configuration
};
