const contractData = [
  {id: 1, name: 'Samppa Nori', registered: '2018/01/01', address: 'Member', status: 'Active'},
  {id: 2, name: 'Estavan Lykos', registered: '2018/02/01', address: 'Staff', status: 'Banned'},
  {id: 3, name: 'Chetan Mohamed', registered: '2018/02/01', address: 'Admin', status: 'Inactive'},
]

export default contractData
