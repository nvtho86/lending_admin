import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom';


class ContractForm extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      id:'',
      name:'',
      address:'',
      status:false,
    }
  }
  componentWillMount() {
    /** kiem tra cap nhat */
    if(this.props.contractEditing){
      this.setState({
        id:this.props.contractEditing.id,
        name:this.props.contractEditing.name,
        address:this.props.contractEditing.address,
        status:this.props.contractEditing.status,
      });
    }
  }
  /** push data to field */
  componentWillReceiveProps(nextProps){
    if(nextProps && nextProps.contractEditing){
      this.setState({
        id:nextProps.contractEditing.id,
        name:nextProps.contractEditing.name,
        address:nextProps.contractEditing.address,
        status:nextProps.contractEditing.status,
      });
    }
  }
  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if(name==='status'){
      value = target.value==='true'?true:false;
    }
    this.setState({
      [name]: value
    });
  }
  onSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state)
    this.setState({
      id:'',
      name:'',
      address:'',
      status:false
    });
  }
  render() {
    var { id } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <legend>{id!==''?'Cập nhật':'Thêm hợp đồng mới'}</legend>
              </div>
              <div className="row col-md-12 form-group">
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Tên hợp đồng:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Tên hợp đồng" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Địa chỉ:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="address"
                      value={this.state.address}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Địa chỉ" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Trạng thái:</label>
                  <div className="col-sm-12">
                    <select name="status"
                      value={this.state.status}
                      onChange={this.onChange}
                      id="inputstatus" className="form-control">

                      <option value={true} >Kích hoạt</option>
                      <option value={false}>Ẩn</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-12 col-sm-offset-2 text-right">
                  <button type="submit" className="btn btn-primary mr-2">{id!==''?'Cập nhật':'Thêm mới'}</button>
                  <button type="submit" className="btn btn-secondary mr-2">Hủy</button>
                  {/* <button type="button" className="btn  btn-danger" onClick={this.onGeneratecontract}>Generate data</button> */}
                </div>
              </div>
            </form>
    );
  }
}


export default ContractForm;
