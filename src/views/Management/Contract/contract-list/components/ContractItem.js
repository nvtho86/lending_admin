import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom';


class ContractItem extends Component {

  onUpdateStatus = () =>{
    this.props.onUpdateStatus(this.props.contract.id)
  }
  onEdit = (id) =>{
    this.props.onEdit(id)
  }
  onDelete = (id) =>{
    this.props.onDelete(id)
  }
  render() {

    var { contract, index } = this.props;
    return (
      <tr>
        <td className="text-center">{index+1}</td>
        <td >{contract.name}</td>
        <td >{contract.name}</td>
        <td >{contract.address}</td>
        <td className="text-center">
          <Badge
            onClick={this.onUpdateStatus}
            className={ contract.status===true?"btn btn-success":"btn btn-danger" } >
            {contract.status===true?'Kích hoạt':'Ẩn'}
          </Badge>
        </td>
        <td className="text-center">
          <button type="button" className="btn mr-2 btn-warning" onClick={() => this.onEdit(contract.id)} >Sửa</button>
          <button type="button" className="btn btn-danger" onClick={() => this.onDelete(contract.id)}>Xóa</button>
        </td>
      </tr>
    );
  }
}


export default ContractItem;
