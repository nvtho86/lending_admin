import React, { Component } from 'react';
import ContractItem from './ContractItem';
import { Table } from 'reactstrap';

class ContractList extends Component {

  render() {
    var { contracts } = this.props;
    var elmContracts = contracts.map((contract, index) => {
      return <ContractItem
        key={contract.id}
        index={index}
        contract={contract}
        onUpdateStatus={this.props.onUpdateStatus}
        onEdit={this.props.onEdit}
        onDelete={this.props.onDelete}
        />
    })
    return (
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th className="text-center" >#</th>
            <th className="text-center" >Tên hợp đồng</th>
            <th className="text-center" >Khách hàng</th>
            <th className="text-center" >Địa chỉ</th>
            <th className="text-center" >Trạng thái</th>
            <th className="text-center">Hành động</th>
          </tr>
        </thead>
        <tbody>
          {elmContracts}
        </tbody>
      </Table>
    );
  }
}


export default ContractList;
