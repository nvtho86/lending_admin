import React, { Component } from 'react';
import { Row, Col, Table } from 'reactstrap';
import * as actions from './../../../actions';
import { connect } from "react-redux";
import ContractList from './components/ContractList';
import contracts from '../../../reducers/contracts.reducer';


// import TrackList from './components/TrackList';
// import {configureStore} from './store';
// import * as actions from './actions';

class Contract extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contracts: [],
      id: '',
      name: '',
      address: '',
      status: false,

    }
  }
  componentWillMount() {

    if (localStorage && localStorage.getItem('contracts')) {
      var contracts = JSON.parse(localStorage.getItem('contracts'));
      this.setState({
        contracts: contracts
      });
    }

    /** kiem tra cap nhat */
    if(this.props.contractEditing){

      this.setState({
        id:this.props.contractEditing.id,
        name:this.props.contractEditing.name,
        address:this.props.contractEditing.address,
        status:this.props.contractEditing.status,
      });

    }

  }
  // onGeneratecontract = () => {
  //   var contracts = [
  //     { id: 1, name: 'Samppa Nori', registered: '2018/01/01', address: 'Member', status: 'Active' },
  //     { id: 2, name: 'Estavan Lykos', registered: '2018/02/01', address: 'Staff', status: 'Banned' },
  //     { id: 3, name: 'Chetan Mohamed', registered: '2018/02/01', address: 'Admin', status: 'Inactive' },
  //   ];
  //   this.setState({ contracts: contracts });
  //   localStorage.setItem('contracts', JSON.stringify(contracts));
  // }

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  generatesID() {
    return this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4();
  }

  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  }

  /** add new */
  onSubmit = (event) => {
    event.preventDefault();
    var {contracts}  = this.state;
    const data = {
      id:this.generatesID(),
      name:this.state.name,
      address:this.state.address,
      status:this.state.status === 'true' ? true : false
    }
    contracts.push(data);
    this.setState({
      ...this.state,
      contracts:contracts
    });
    localStorage.setItem('contracts', JSON.stringify(contracts));
    this.setState({
      id: '',
      name: '',
      address: '',
      status: false
    });
  }

  /** edit */
  onEdit = (id) =>{
    var { contracts } = this.state;
    var index =  this.findIndex(id);
    var contractEditing = contracts[index];
    this.setState({
      contractEditing: contractEditing
    });
  }

  /** delete */
  onDelete = (id) =>{
    var { contracts } = this.state;
    var index =  this.findIndex(id);
    if(index!==-1){
      contracts.splice(index,1)
      this.setState({contracts:contracts});
      localStorage.setItem('contracts', JSON.stringify(contracts));
    }
  }
  /** update status */
  onUpdateStatus = (id) =>{
    var { contracts } = this.state;
    var index =  this.findIndex(id);
    if(index!==-1){
      contracts[index].status = !contracts[index].status;
      this.setState({contracts:contracts});
      localStorage.setItem('contracts', JSON.stringify(contracts));
    }
  }
  findIndex = (id) =>{
    var { contracts } = this.state;
    var result = -1;
    contracts.forEach((contract, index) =>{
      if(contract.id === id){
        result =  index
      }
    })
    return result;
  }

  render() {
    var { contracts,contractEditing,id } = this.state;
    // console.log(this.state);
    return (
      <div>
        <Row >
          <Col xl={12}>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <legend>{id!=''?"Cập nhật hợp đồng":"Thêm hợp đồng mới"}</legend>
              </div>
              <div className="row col-md-12 form-group">

                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Tên hợp đồng:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Tên hợp đồng" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Địa chỉ:</label>
                  <div className="col-sm-12">
                    <input type="text"
                      name="address"
                      value={this.state.address}
                      onChange={this.onChange}
                      id="inputname"
                      className="form-control"
                      placeholder="Địa chỉ" />
                  </div>
                </div>
                <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                  <label className="col-sm-12 control-label">Trạng thái:</label>
                  <div className="col-sm-12">
                    <select name="status"
                      value={this.state.status}
                      onChange={this.onChange}
                      id="inputstatus" className="form-control">

                      <option value={true} >Kích hoạt</option>
                      <option value={false}>Ẩn</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-12 col-sm-offset-2 text-right">
                  <button type="submit" className="btn btn-primary mr-2">Thêm</button>
                  <button type="submit" className="btn btn-secondary mr-2">Hủy</button>
                  {/* <button type="button" className="btn  btn-danger" onClick={this.onGeneratecontract}>Generate data</button> */}
                </div>
              </div>
            </form>
          </Col>
        </Row>
        <Row>
          <ContractList
          contracts={contracts}
          onEdit={this.onEdit}
          onUpdateStatus = {this.onUpdateStatus}
          onDelete = {this.onDelete} />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    contracts: state.contracts
  };
}
// const mapDispatchToProps = (dispatch, props) => {
//   return {
//     dispatch(actions.listAll());
//   }
// }

export default connect(mapStateToProps, null)(Contract);

