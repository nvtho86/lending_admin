import React, { Component } from 'react';
import {
    Row, Col, FormGroup, Input
} from 'reactstrap';

class FilterAccountForm extends Component {

    render() {
        return (
            <div className="animated fadeIn">
                {this.props.data.map((a) => (
                    <FormGroup key={a.id}>
                        <Row>
                            <Col md={1}>
                                <i className="fa fa-minus" onClick={() => this.props.decreaseFilterForm(a)} />
                            </Col>
                            <Col>
                                <select name="status"
                                    id="inputstatus" className="form-control">
                                    <option value={true} >Tuổi</option>
                                    <option value={false}>Giới tính</option>
                                </select>
                            </Col>
                            <Col>
                                <select name="status"
                                    id="inputstatus" className="form-control">
                                    <option value={true} label="=" />
                                    <option value={true} label=">=" />
                                    <option value={true} label="<=" />
                                </select>
                            </Col>
                            <Col>
                                <Input />
                            </Col>
                        </Row>
                    </FormGroup>
                ))}
            </div>
        );
    }

}


export default FilterAccountForm;
