import React, { Component } from 'react';
import {
    Row, Col, Label, FormGroup
} from 'reactstrap';

class ReviewFormNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: props.file
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps && nextProps.file) {
            this.setState({
                file: nextProps.file
            });
        }
    }

    render() {
        console.log(this.state)
        return (
            <div className="animated fadeIn">
                <Col md={4}>
                    <FormGroup className="shadowBox review">
                        <Row>
                            <Col className="text-center">
                                <Label className="font-weight-bold text-center">Xem nội dung trước</Label>
                            </Col>
                        </Row>

                        <Row>
                            <Col className="text-center">
                                <img src={this.state.file} className="imgFrame" />
                            </Col>
                        </Row>

                        <Row>
                            <Col className="text-center">
                                <Label style={{ 'wordWrap': 'normal', maxWidth: '325px', fontSize: 'large' }}><strong>{this.props.title}</strong></Label>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <div dangerouslySetInnerHTML={this.props.renderEditor()}
                                    style={{
                                        margin: 'auto', maxWidth: '325px',
                                        height: '380px', 'overflowX': 'hidden', 'overflowY': 'hidden'
                                    }}>
                                </div>
                            </Col>
                        </Row>
                    </FormGroup>
                </Col>
            </div>
        );
    }

}


export default ReviewFormNews;
