import React, { Component } from 'react';
import {
  Row, Col, Card, CardBody, CardHeader,
  Button, Table
} from 'reactstrap';
import ModalDeleteNotification from './ModalDeleteNotification';
import ModalEditNotification from './ModalEditNotification';
import axios from 'axios';

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delete: false,
      edit: false,
      detail: false,
      listItem : [],
    };
  }

  toggleDelete = () => {
    this.setState({
      delete: !this.state.delete
    });
  }

  toggleEdit = () => {
    this.setState({
      edit: !this.state.edit
    });
  }

  toggleDetail = () => {
    this.setState({
      detail: !this.state.detail
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <h3 className="check">Thông báo</h3>
              </CardHeader>
              <CardBody className="pb-0">
                <div className="row form-group">
                  <div className="col-md-6">
                    <div className="row">
                      <label className="col-sm-4 control-label text-right">Từ khóa</label>
                      <div className="col-sm-8">
                        <input type="text" className="form-control" placeholder="Từ khóa"
                          name="keyword" />
                      </div>
                    </div>
                  </div>

                  <div className="col-sm-3">
                    <select name="status"
                      id="inputstatus" className="form-control">

                      <option value={true} >Khuyến mãi</option>
                      <option value={false}>Ẩn</option>
                    </select>
                  </div>

                  <div className="col-sm-3">
                    <Button color="primary">Tìm kiếm</Button>
                  </div>
                </div>
                <Table bordered striped hover>
                  <thead className="text-center">
                    <tr>
                      <th>STT</th>
                      <th>Tiêu đề</th>
                      <th>Nội dung</th>
                      <th>Hình ảnh</th>
                      <th>Loại</th>
                      <th>Sửa</th>
                      <th>Xóa</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="text-center">STT</td>
                      <td>Tiêu đề</td>
                      <td>aaa</td>
                      <td className="text-center"><img src={"./logo.jpeg"} /></td>
                      <td className="text-center">Loại</td>
                      <td className="text-center">
                        <Button color="primary" className="mr-1" onClick={this.toggleEdit}>Sửa</Button>
                        <ModalEditNotification modal={this.state.edit} toggleEdit={this.toggleEdit} />
                      </td>
                      <td className="text-center">
                        <Button color="primary" onClick={this.toggleDelete} className="mr-1">Xóa</Button>
                        <ModalDeleteNotification modal={this.state.delete} toggleDelete={this.toggleDelete} />
                      </td>
                    </tr>
                    <tr>
                      <td className="text-center">STT</td>
                      <td>Tiêu đề</td>
                      <td>bbb</td>
                      <td className="text-center"><img src={"./logo.jpeg"} /></td>
                      <td className="text-center">Loại</td>
                      <td className="text-center">
                        <Button color="primary" className="mr-1" onClick={this.toggleEdit}>Sửa</Button>
                        <ModalEditNotification modal={this.state.edit} toggleEdit={this.toggleEdit} />
                      </td>
                      <td className="text-center">
                        <Button color="primary" onClick={this.toggleDelete} className="mr-1">Xóa</Button>
                        <ModalDeleteNotification modal={this.state.delete} toggleDelete={this.toggleDelete} />
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div >
    );
  }
}

export default Notification;
