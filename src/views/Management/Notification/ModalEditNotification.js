import React, { Component } from 'react';
import {
  Row, Col, Button, Label, Modal, ModalBody, ModalFooter, Input, FormGroup
} from 'reactstrap';
import FilterAccountForm from './FilterAccountForm';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import "./custom.css"
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertToRaw } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import ReviewFormNews from './ReviewFormNews'

export class ModalEditNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: props.modal,
      file: null,
      title: '',
      fullContent: '',
      display: 'none',
      filterForm: [{
        condition: '',
        compare: '',
        result: '',
        id: this.uuidv4()
      }],
      startDate: Date.now(),
      editorHTML: ''
    };
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleChangeDate = (date) => {
    this.setState({ startDate: date });
  }

  onEditorChange = (editorState) => {
    const editorHTML = draftToHtml(convertToRaw(editorState.getCurrentContent()))
    this.setState({
      editorState,
      editorHTML: editorHTML
    });
  };

  onchangeImage = (event) => {
    this.setState({
      file: URL.createObjectURL(event.target.files[0])
    })
  }

  onchangeRadio = (event) => {
    this.setState({
      display: event.target.value
    })
  }

  increaseFilterForm = () => {
    this.state.filterForm.push({
      condition: '',
      compare: '',
      result: '',
      id: this.uuidv4()
    })
    this.setState({
      filterForm: this.state.filterForm
    })
  }

  decreaseFilterForm = (elem) => {
    let index = this.state.filterForm.indexOf(elem);
    this.state.filterForm.splice(index, 1)
    this.setState({
      filterForm: this.state.filterForm
    })
  }

  uuidv4 = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
  }

  renderEditor = () => {
    return { __html: '<span>' + this.state.editorHTML + '</span>' };
  }

  render() {
    const { editorState } = this.state;
    return (<div>
      <Modal isOpen={this.props.modal} className="my-modal-dialog" toggle={this.props.toggleEdit} >
        <fieldset className="border p-2 form-group">
          <legend className="w-auto">Cập nhật thông tin tin tức</legend>
          <ModalBody>
            <Row>
              <Col md={7} style={{ marginLeft: '35px' }}>
                <FormGroup>
                  <Row>
                    <Col md={3} >
                      <Label>
                        Title
                      </Label>
                    </Col>
                    <Col md={6}>
                      <Input value={this.state.title} type='text' name='title' onChange={this.handleChange} />
                    </Col>

                  </Row>
                </FormGroup>
                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Nội dung ngắn gọn
              </Col>
                    <Col md={6}>
                      <Input></Input>
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Nội dung đầy đủ
              </Col>
                    <Col md={6}>
                      <Editor
                        editorState={editorState}
                        onEditorStateChange={this.onEditorChange}
                        toolbar={{
                          options: ['inline', 'fontSize', 'fontFamily', 'list',
                            'textAlign', 'colorPicker', 'link', 'image'],
                          link: {
                            defaultTargetOption: '_blank',
                            popupClassName: "mail-editor-link"
                          },
                          image: {
                            urlEnabled: true,
                            uploadEnabled: true,
                            uploadCallback: this.uploadImageCallBack,
                            alignmentEnabled: true,
                            defaultSize: {
                              height: 'auto',
                              width: 'auto',
                            },
                            inputAccept: 'application/pdf,text/plain,application/vnd.openxmlformatsofficedocument.wordprocessingml.document,application/msword,application/vnd.ms-excel'
                            ,
                            colorPicker: {
                              className: undefined,
                              component: undefined,
                              popupClassName: undefined,
                              colors: ['rgb(97,189,109)', 'rgb(26,188,156)', 'rgb(84,172,210)', 'rgb(44,130,201)',
                                'rgb(147,101,184)', 'rgb(71,85,119)', 'rgb(204,204,204)', 'rgb(65,168,95)', 'rgb(0,168,133)',
                                'rgb(61,142,185)', 'rgb(41,105,176)', 'rgb(85,57,130)', 'rgb(40,50,78)', 'rgb(0,0,0)',
                                'rgb(247,218,100)', 'rgb(251,160,38)', 'rgb(235,107,86)', 'rgb(226,80,65)', 'rgb(163,143,132)',
                                'rgb(239,239,239)', 'rgb(255,255,255)', 'rgb(250,197,28)', 'rgb(243,121,52)', 'rgb(209,72,65)',
                                'rgb(184,49,47)', 'rgb(124,112,107)', 'rgb(209,213,216)']
                            }
                          }
                        }}
                        editorClassName="form-control editorClassName"
                      />
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Loại tin tức
              </Col>
                    <Col md={3}>
                      <select name="status"
                        id="inputstatus" className="form-control">

                        <option value={true} >Khuyễn mãi</option>
                        <option value={false}>Thông tin HD Saigon</option>
                      </select>
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Ngày bắt đầu hiển thị
              </Col>
                    <Col md={3} className="text-center">
                      <DatePicker
                        className="form-control"
                        name="startDate"
                        selected={this.state.startDate}
                        onChange={this.handleChangeDate}
                        showYearDropdown
                        dateFormatCalendar="MMMM"
                        scrollableYearDropdown
                        yearDropdownItemNumber={15}
                      />
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Hình ảnh
              </Col>
                    <Col md={6}>
                      <Label htmlFor="exampleFile">Click</Label>
                      <Input type="file" name="file" id="exampleFile"
                        accept="image/*" onChange={this.onchangeImage} />
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Tin nổi bật
                    </Col>
                    <Col md={6}>
                      <Input type="checkbox" style={{ 'marginLeft': '0px' }} />
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Gửi notification
                    </Col>
                    <Col md={6}>
                      <Input type="checkbox" style={{ 'marginLeft': '0px' }} />
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md={3}>
                      Tin tức dành cho
              </Col>
                    <Col md={2}>
                      <FormGroup check>
                        <Label check>
                          <Input type="radio" value='none' onChange={this.onchangeRadio}
                            checked={this.state.display === 'none'} />{' '}
                          Toàn bộ
                </Label>
                      </FormGroup>
                    </Col>
                    <Col md={3}>
                      <FormGroup check>
                        <Label check>
                          <Input type="radio" value='' onChange={this.onchangeRadio}
                            checked={this.state.display === ''} />
                          Chỉ 1 nhóm người
                    </Label>
                      </FormGroup>
                    </Col>
                  </Row>
                </FormGroup>

                <FormGroup style={{ display: this.state.display }}>
                  <Row>
                    <Col>
                      Lọc đanh sách tài khoản nhận tin tức
                    </Col>
                  </Row>

                  <FormGroup>
                    <Row className="text-center">
                      <Col md={1}>
                      </Col>
                      <Col>
                        Điều kiện
                    </Col>
                      <Col>
                        So sánh
                    </Col>
                      <Col>
                        Giá trị
                    </Col>
                    </Row>
                    <FilterAccountForm data={this.state.filterForm} decreaseFilterForm={this.decreaseFilterForm} />
                  </FormGroup>

                  <Row>
                    <Col md={1}>
                      <i className="fa fa-plus" onClick={this.increaseFilterForm} />
                    </Col>
                  </Row>
                </FormGroup>
              </Col>
              <Col md={4}>
                <ReviewFormNews file={this.state.file} renderEditor={this.renderEditor} title={this.state.title} />
              </Col>
            </Row>
          </ModalBody>

          <ModalFooter>

            <Col md={5} className="primary">
            </Col>
            <Col md={7} className="text-right">
              <Button color="primary" onClick={this.props.toggleEdit}>Quay lại</Button>{' '}
              <Button color="secondary" onClick={this.props.toggleEdit}>Lưu</Button>
            </Col>
          </ModalFooter>
        </fieldset>
      </Modal>
    </div >
    );
  }
}

export default ModalEditNotification;
