import React, { Component } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from "reactstrap";
export class ModalDeleteNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: props.modal
    };
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={this.props.toggleDelete}>
          <fieldset className="border p-2 form-group">
            <legend className="w-auto">Xác nhận xóa tin tức</legend>
            <ModalHeader>Tiêu đề</ModalHeader>
            <ModalBody>
              <Row className="border-bottom">
                <Col md={12}>Nội dung</Col>
              </Row>
              <Row>
                <Col md={12} className="text-center">
                  Xác nhận xóa tin tức?
                </Col>
              </Row>
            </ModalBody>

            <ModalFooter>
              <Button color="primary" onClick={this.props.toggleDelete}>
                Hủy bỏ
              </Button>{" "}
              <Button color="secondary" onClick={this.props.toggleDelete}>
                Đồng ý
              </Button>
            </ModalFooter>
          </fieldset>
        </Modal>
      </div>
    );
  }
}

export default ModalDeleteNotification;
