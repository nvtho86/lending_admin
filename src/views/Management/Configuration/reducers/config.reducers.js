// reducers/todosReducers.js
import { GENERAL_CONFIG } from '../constants/ActionTypes';

const initialState = {
    startDate:'2019-06-30',
    endDate: '2019-12-31',
    otp: '2',
    addendumMailReceivingTime:'3',
    addendumMailReceivingList:'3',
    fileMailReceivingTime:'4',
    fileMailReceivingList:'5',
    infoMailReceivingTime:'6',
    infoMailReceivingList:'7',
    setupTimeApp: '7',
    setupTimeWebAdmin: '6'
};

export default function todos(state = initialState, action) {
    switch (action.type) {
        case GENERAL_CONFIG:
          var dataConfig = {
            startDate: action.config.startDate,
            endDate:  action.config.endDate,
            otp: action.config.otp,
            addendumMailReceivingTime: action.config.addendumMailReceivingTime,
            addendumMailReceivingList: action.config.addendumMailReceivingList,
            fileMailReceivingTime: action.config.fileMailReceivingTime,
            fileMailReceivingList: action.config.fileMailReceivingList,
            infoMailReceivingTime: action.config.infoMailReceivingTime,
            infoMailReceivingList: action.config.infoMailReceivingList,
            setupTimeApp: action.config.setupTimeApp,
            setupTimeWebAdmin: action.config.setupTimeWebAdmin
          }
          state.push(dataConfig);
          localStorage.setItem('generalConfig', JSON.stringify(state))
          return [...state];
            // return [{
            //     id: (state.length === 0) ? 0 : state[0].id + 1,
            //     marked: false,
            //     text: action.text
            // }, ...state];

        // case EDIT_TODO:
        //     return state.map((todo) => todo.id === action.id ? { ...todo, text: action.text } : todo);

        // case MARK_TODO:
        //     return state.map((todo) => todo.id === action.id ? { ...todo, marked: !todo.marked } : todo);

        // case MARK_ALL:
        //     const areAllMarked = state.every((todo) => todo.marked);
        //     return state.map((todo) => ({...todo, marked: !areAllMarked}));

        // case CLEAR_MARKED:
        //     return state.filter((todo) => todo.marked === false);

        default:
            return state;
    }
}
