import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, Button, Label, Input, FormGroup } from 'reactstrap';

import * as actions from './actions/config.actions';

import { connect } from 'react-redux'

class Configuration extends Component {


  componentWillMount() {
    /** kiem tra cap nhat */
    // const value = new Date().toISOString();
    // const date = value;
    // const locale = locale;
    if (this.props.generalConfig) {

      this.setState({
        startDate:this.props.generalConfig.startDate,
        endDate: this.props.generalConfig.endDate,
        otp: this.props.generalConfig.otp,
        addendumMailReceivingTime:this.props.generalConfig.addendumMailReceivingTime,
        addendumMailReceivingList:this.props.generalConfig.addendumMailReceivingList,
        fileMailReceivingTime:this.props.generalConfig.fileMailReceivingTime,
        fileMailReceivingList:this.props.generalConfig.fileMailReceivingList,
        infoMailReceivingTime:this.props.generalConfig.infoMailReceivingTime,
        infoMailReceivingList:this.props.generalConfig.infoMailReceivingList,
        setupTimeApp: this.props.generalConfig.setupTimeApp,
        setupTimeWebAdmin: this.props.generalConfig.setupTimeWebAdmin
      });
    }
  }
  /** push data to field */
  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.generalConfig) {
      this.setState({
        startDate:nextProps.generalConfig.startDate,
        endDate: nextProps.generalConfig.endDate,
        otp: nextProps.generalConfig.otp,
        addendumMailReceivingTime:nextProps.generalConfig.addendumMailReceivingTime,
        addendumMailReceivingList:nextProps.generalConfig.addendumMailReceivingList,
        fileMailReceivingTime:nextProps.generalConfig.fileMailReceivingTime,
        fileMailReceivingList:nextProps.generalConfig.fileMailReceivingList,
        infoMailReceivingTime:nextProps.generalConfig.infoMailReceivingTime,
        infoMailReceivingList:nextProps.generalConfig.infoMailReceivingList,
        setupTimeApp: nextProps.generalConfig.setupTimeApp,
        setupTimeWebAdmin: nextProps.generalConfig.setupTimeWebAdmin

      });
    }
  }
  onHandlechange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  }
  onUpdateConfig =() =>{
    this.props.onUpdateConfig(this.state);
  }
  render() {
    // var {generalConfig} = this.props;
    return (
      <div className="animated fadeIn">
        <Row >
          <Col xl={12}>
            <Card>
              <CardHeader>
                <div className="form-group text-center">
                  <h3>Cấu hình chung</h3>
                </div>
              </CardHeader>
              <CardBody>
                <FormGroup>
                  <h5>Thời gian ký Asign</h5>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleEmail" sm={1}>Từ</Label>
                  <Col sm={3}>
                    <Input type="date"
                    value={this.state.startDate}
                    // defaultValue={generalConfig.startDate}
                    name="startDate"
                    onChange={this.onHandlechange}
                    format="dd/MM/yyyy"
                    />
                  </Col>
                  <Label for="examplePassword" sm={1}>Đến</Label>
                  <Col sm={3}>
                    <Input type="date"
                    // defaultValue={generalConfig.endDate}
                    value={this.state.endDate.toString()}
                    name="endDate" onChange={this.onHandlechange} />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Label for="otp" sm={5}><h5>Thời gian hêt hạn OTP</h5></Label>
                  <Col sm={6}>
                    <Input type="text" value={this.state.otp}  onChange={this.onHandlechange} name="otp" id="otp" placeholder="OTP" /> (Phút)
                  </Col>
                </FormGroup>
                <FormGroup>
                  <h5>Cấu hình điều chỉnh phụ lục hợp đồng</h5>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleEmail" sm={2}>Thời gian nhận mail</Label>
                  <Col sm={2}>
                    <Input type="text" value={this.state.addendumMailReceivingTime} onChange={this.onHandlechange} name="addendumMailReceivingTime" id="" placeholder="" />
                  </Col>
                  <Label for="examplePassword" sm={2}>Danh sách nhận mail</Label>
                  <Col sm={6}>
                    <Input type="text" value={this.state.addendumMailReceivingList} onChange={this.onHandlechange} name="addendumMailReceivingList" id="" placeholder="" />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <h5>Cấu hình gởi file hợp đồng</h5>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleEmail" sm={2}>Thời gian nhận mail</Label>
                  <Col sm={2}>
                    <Input type="text" value={this.state.fileMailReceivingTime} onChange={this.onHandlechange} name="fileMailReceivingTime" id="" placeholder="" />
                  </Col>
                  <Label for="examplePassword" sm={2}>Danh sách nhận mail</Label>
                  <Col sm={6}>
                    <Input type="text" value={this.state.fileMailReceivingList} onChange={this.onHandlechange} name="fileMailReceivingList" id="" placeholder="" />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <h5>Cấu hình gởi thông tin đăng ký vay</h5>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleEmail" sm={2}>Thời gian nhận mail</Label>
                  <Col sm={2}>
                    <Input type="text" value={this.state.infoMailReceivingTime} onChange={this.onHandlechange} name="infoMailReceivingTime" id="" placeholder="" />
                  </Col>
                  <Label for="examplePassword" sm={2}>Danh sách nhận mail</Label>
                  <Col sm={6}>
                    <Input type="text" value={this.state.infoMailReceivingList} onChange={this.onHandlechange} name="infoMailReceivingList" id="" placeholder="" />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <h5>Thiết lập thời gian timeout</h5>
                </FormGroup>
                <FormGroup>
                  <FormGroup row>
                  <Label for="exampleEmail" sm={2}>Mobile App</Label>
                  <Col sm={2}>
                    <Input type="text" value={this.state.setupTimeApp} onChange={this.onHandlechange} name="setupTimeApp" id="" placeholder="" />
                  </Col>
                  <Col sm={2}>
                  <Input type="select" name="select" id="exampleSelect">
                      <option value="m">Phút</option>
                      <option value="h">Giờ</option>
                      <option value="d">Ngày</option>
                      <option value="M">Tháng</option>
                  </Input>
                  </Col>
                  <Label for="examplePassword" sm={2}>Esign Portal</Label>
                  <Col sm={2}>
                    <Input type="text" name="" id="" placeholder="" />
                  </Col>
                  <Col sm={2}>
                  <Input type="select" name="select" id="exampleSelect">
                      <option value="m">Phút</option>
                      <option value="h">Giờ</option>
                      <option value="d">Ngày</option>
                      <option value="M">Tháng</option>
                  </Input>
                  </Col>
                  </FormGroup>
                  <FormGroup row>

                  <Label for="exampleEmail" sm={2}>Web Admin</Label>
                  <Col sm={2}>
                    <Input type="text" value={this.state.setupTimeWebAdmin} onChange={this.onHandlechange} name="setupTimeWebAdmin" id="" placeholder="" />
                  </Col>
                  <Col sm={2}>
                  <Input type="select" name="select" id="exampleSelect">
                      <option value="m">Phút</option>
                      <option value="h">Giờ</option>
                      <option value="d">Ngày</option>
                      <option value="M">Tháng</option>
                  </Input>
                  </Col>
                  <Label for="examplePassword" sm={2}>Thời gian đổi mật khẩu</Label>
                  <Col sm={2}>
                    <Input type="text" value={this.state.name} onChange={this.onHandlechange} name="" id="" placeholder="" />
                  </Col>
                  <Col sm={2}>
                  <Input type="select" name="select" id="exampleSelect">
                      <option value="m">Phút</option>
                      <option value="h">Giờ</option>
                      <option value="d">Ngày</option>
                      <option value="M">Tháng</option>
                  </Input>
                  </Col>
                  </FormGroup>
                </FormGroup>
                <FormGroup row>
                    <Col sm="12" md={{ size: 6, offset: 5 }}>
                      <Button type="reset" color="secondary mr-2">Hủy</Button>
                      <Button type="submit" onClick={this.onUpdateConfig} color="primary">Lưu</Button>
                    </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    )
  }
}
const mapStateToProps = (state) =>{
  return {
    generalConfig: state.generalConfig
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateConfig: (config)=>{
      dispatch(actions.addConfig(config))
    }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Configuration);
