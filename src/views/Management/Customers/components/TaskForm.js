import React, { Component } from 'react';
import { Row, Col, Table } from 'reactstrap';
import { connect } from "react-redux";


class TaskForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      name: '',
      status: false

    }
  }
  componentWillMount() {
    if (localStorage && localStorage.getItem('tasks')) {
      var tasks = JSON.parse(localStorage.getItem('tasks'));
      this.setState({
        tasks: tasks
      });
    }

  }
  onGeneratetask = () => {
    var tasks = [
      {
        id: this.generatesID(),
        name: "Van Tho",
        age: '25',
        startDate: '2019-07-02',
        status: true
      },
      {
        id: this.generatesID(),
        name: "Nguyen ",
        age: '25',
        startDate: '2019-07-02',
        status: true
      },
      {
        id: this.generatesID(),
        name: "Nguyen Ku Vang",
        age: '25',
        startDate: '2019-07-02',
        status: false
      }
    ];
    this.setState({ tasks: tasks });
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  generatesID() {
    return this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4();
  }
  onEdit = (id) => {
    alert(id);
    console.log(id)
  }
  onDelete = (id) => {
    alert(id);
    console.log(id)
  }
  onAdd = () => {

  }
  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  }
  onSubmit = (event) => {
    event.preventDefault();
    console.log(this.state.name);
    console.log(this.state.status);
    // this.props.onSubmit(this.state.name, this.state.status==='true'?true:false);
  }
  render() {
    const listItems = this.state.tasks.map((task, index) =>
      <tr key={task.id}>
        <td>{index + 1}</td>
        <td>{task.name}</td>
        <td>{task.age}</td>
        <td>{task.status == true ? "active" : "pending"}</td>
        <td>{task.startDate}</td>
        <td>{task.startDate}</td>
        <td>
          <button type="button" className="btn mr-2 btn-warning" onClick={() => this.onEdit(task.id)} >Sửa</button>
          <button type="button" className="btn btn-danger" onClick={() => this.onDelete(task.id)}>Xóa</button>
        </td>
      </tr>
    );
    return (

      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <button type="button" className="btn  btn-success mb-5 mr-4" onClick={this.onAdd}>Thêm mới</button>
          <button type="button" className="btn  btn-danger mb-5" onClick={this.onGeneratetask}>Generate task</button>

          <Row>
            <Col>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <legend>Thêm thành viên mới</legend>
                </div>
                <div className="row col-md-12 form-group">

                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label className="col-sm-12 control-label">Tên thành viên mới:</label>
                    <div className="col-sm-12">
                      <input type="text"
                        name="name"
                        value={this.state.name}
                        onChange={this.onChange}
                        id="inputname"
                        className="form-control"
                        placeholder="Tên thành viên mới" />
                    </div>
                  </div>
                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <label className="col-sm-12 control-label">Trạng thái:</label>
                    <div className="col-sm-12">
                      <select name="status"
                        value={this.state.status}
                        onChange={this.onChange}
                        id="inputstatus" className="form-control">

                        <option value={true} >Kích hoạt</option>
                        <option value={false}>Ẩn</option>
                      </select>
                    </div>
                  </div>

                </div>

                <div className="form-group">
                  <div className="col-sm-11 col-sm-offset-2 text-right">
                    <button type="submit" className="btn btn-primary mr-2">Submit</button>
                    <button type="submit" className="btn btn-primary">Hủy</button>
                  </div>
                </div>
              </form>

            </Col>
          </Row>
          <Row>
            <Col>
              <Table bordered striped hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Tên</th>
                    <th>Tuổi</th>
                    <th>Trạng thái</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th>Hành động</th>
                  </tr>
                </thead>
                <tbody>
                  {listItems}
                </tbody>
              </Table>
            </Col>
          </Row>
        </div>
      </div>

    )
  }
}
export default TaskForm;
