import * as types from './../constants/ActionTypes';
// var data = JSON.parse(localStorage.getItem('contracts'));
// var initialState = data ? data : [];

const initialState = {
  accounts: [],
  keyword: '',
  modalLog: false,
  modal: false,
  modalLock: false,
  selectAccount: undefined
};


// var findIndex = (accounts, id) => {
//   var result = -1;
//   accounts.forEach((account, index) => {
//     if (account.id === id) {
//       result = index
//     }
//   })
//   return result;
// }
var accounts = (state = initialState, action) => {
  switch (action.type) {

    case types.LIST_ALL_ACCOUNT:
      return {
        ...state,
        accounts: action.accounts
      };
    case types.DETAIL_ACCOUNT:
      return {
        ...state,
        selectAccount: action.account,
        modal: !state.modal
      };
    case types.SEARCH_ACCOUNT:
      return {
        ...state,
        accounts: action.accounts,
      };
    case types.RESET_TATBLE_ACCOUNT:
      return {
        ...state,
        accounts: action.accounts,
      };
    case types.LOG_ACCOUNT:
      return {
        ...state,
        selectAccount: action.account,
        modalLog: !state.modalLog
      };
    case types.CLOSE_MODAL_LOG:
      return {
        ...state,
        modalLog: !state.modalLog
      };
    case types.CLOSE_MODAL_ACCOUNT:
      return {
        ...state,
        modal: !state.modal
      };

    case types.CHECK_ACCOUNT:
      return {
        ...state,
        modalCheck: !state.modalCheck
      };
    case types.ENABLE_ACCOUNT:
      return {
        ...state,
        accounts: action.accounts,
        modalLock: !state.modalLock
      };  
    case types.DISABLE_ACCOUNT:
      return {
        ...state,
        accounts: action.accounts,
        modalLock: !state.modalLock
      };  
    case types.MODAL_LOCK:

      return {
        ...state,
        selectAccount: action.account,
        modalLock: !state.modalLock
      };

    case types.CLOSE_MODAL_LOCK:

      return {
        ...state,
        modalLock: !state.modalLock
      };
    default:

      return state;
  }
}
export default accounts;
