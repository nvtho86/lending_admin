import React, { Component } from 'react';
import {Row,Col,Card,CardHeader,CardBody} from 'reactstrap';
import AccountForm from './components/AccountForm';
import AccountList from './components/AccountList';

class Accounts extends Component {

  render() {
    return (
      <div className="animated fadeIn">
      <Row >
        <Col xl={12}>
          <Card>
            <CardHeader>
              <h3>Quản lý tài khoản</h3>
            </CardHeader>
            <CardBody>
              <AccountForm/>
              <AccountList {...this.props} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
    );
  }
}

export default Accounts;
