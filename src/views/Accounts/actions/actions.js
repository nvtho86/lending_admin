import * as types from './../constants/ActionTypes';
import apiCaller from './../../../_helpers/apiCaller';

import { returnStatement } from '@babel/types';

export const atcListAllResquest = (cursor) => {
  return (dispatch) => {
    return apiCaller('customer','search', 'POST',
      JSON.stringify(
        {
          "payload": {
            "direction": "Desc",
            "keyWord": "",
            "orderBy": "createdAt",
            "pageNum": 1,
            "pageSize": 10,
            "type": 0
          }
        }
      )
    ).then(res=>{
      if(res){
        // if(res.data.code!=='401'){
          dispatch(actFetchAccounts(res.data.payload.list,res.data.payload.number))
        // }else{
        //   localStorage.clear();
        // }
      }
    })
  };
}

export const actFetchAccounts = (accounts,cursor) => {
  return {
    type: types.LIST_ALL_ACCOUNT,
    accounts,
    cursor
  }
};
export const checkContract = (id) => {
  return {
    type: types.CHECK_ACCOUNT,
    id

  }
}
export const atcDetailResquest = (id) => {
  return (dispatch) => {
    return apiCaller('customer','detail', 'POST',
      JSON.stringify({
        payload: {
          id: id
        }
      })
    ).then(res=>{
      if(res){
      dispatch(actDetail(res.data.payload))
      }
    })
  };
}
export const actDetail = (account) => {
  return {
    type: types.DETAIL_ACCOUNT,
    account
  }
}
export const actLogResquest = (id) => {
  return (dispatch) => {
    return apiCaller('customer','detail', 'POST',
      JSON.stringify({
        payload: {
          id: id
        }
      })
    ).then(res=>{
      if(res){
      dispatch(actLog(res.data.payload))
      }
    })
  };
}
export const actLog = (account) => {
  return {
    type: types.LOG_ACCOUNT,
    account
  }
}
export const actSearchResquest = (keyword, type) => {
  return (dispatch) => {
    return apiCaller('customer','search', 'POST',JSON.stringify({
        "payload": {
          "direction": "Desc",
          "keyWord": keyword,
          "orderBy": "createdAt",
          "pageNum": 1,
          "pageSize": 10,
          "type":type
        }
      })
    ).then(res=>{
      if(res){
      dispatch(actSearch(res.data.payload.list, keyword))
      }
    })
  };
}

export const actSearch = (accounts,keyword) => {
  return {
    type: types.SEARCH_ACCOUNT,
    accounts,
    keyword
  }
}
export const atcResetAccountResquest = () => {
  return (dispatch) => {
    return apiCaller('customer','search', 'POST',JSON.stringify({
        "payload": {
          "direction": "Desc",
          "keyWord": "",
          "orderBy": "createdAt",
          "pageNum": 1,
          "pageSize": 10,
          "type":0
        }
      })
    ).then(res=>{
      if(res){
      dispatch(actSearch(res.data.payload.list))
      }
    })
  };
}

export const atcResetSearch = (accounts,keyword) => {
  return {
    type: types.RESET_TATBLE_ACCOUNT,
    accounts,
  
  }
}
export const actLockResquest = (id) => {
  return (dispatch) => {
    return apiCaller('customer','detail', 'POST',
      JSON.stringify({
        payload: {
          id: id
        }
      })
    ).then(res=>{
      if(res){
      dispatch(actLock(res.data.payload))
      }
    })
  };
}
export const actLock = (account) => {
  return {
    type: types.MODAL_LOCK,
    account

  }
}
export const actEnableAccountkResquest = (id) => {
  return (dispatch) => {
    return apiCaller('customer','enable', 'POST',
      JSON.stringify({
        payload: {
          id: id
        }
      })
    ).then(res=>{
      if(res){
        return apiCaller('customer','search', 'POST',JSON.stringify({
          "payload": {
            "direction": "Desc",
            "keyWord": "",
            "orderBy": "createdAt",
            "pageNum": 1,
            "pageSize": 10,
            "type":0
          }
        })
      ).then(res=>{
        if(res){
        dispatch(actEnableAccount(res.data.payload.list))
        }
      })
      }else{
        dispatch(actEnableAccount(res.data.code))
      }
    })
  };
}
export const actEnableAccount = (accounts) => {
  return {
    type: types.ENABLE_ACCOUNT,
    accounts
  }
}
export const actDisableAccountResquest = (id) => {
  return (dispatch) => {
    return apiCaller('customer','disable', 'POST',
      JSON.stringify({
        "payload": {
          "id": id
        }
      })
    ).then(res=>{
      if(res.data.code===200){
        return apiCaller('customer','search', 'POST',JSON.stringify({
          "payload": {
            "direction": "Desc",
            "keyWord": "",
            "orderBy": "createdAt",
            "pageNum": 1,
            "pageSize": 10,
            "type":0
          }
        })
      ).then(res=>{
        dispatch(actDisableAccount(res.data.payload.list))
      })
      }else{
        dispatch(actDisableAccount(res.data.code))
      }
    })
  };
}
export const actDisableAccount = (accounts) => {
  return {
    type: types.DISABLE_ACCOUNT,
    accounts
  }
}
export const actCloseModalLock = () => {
  return {
    type: types.CLOSE_MODAL_LOCK

  }
}
export const closeModal = () => {
  return {
    type: types.CLOSE_MODAL_ACCOUNT,


  }
}
export const closeModalLog = () => {
  return {
    type: types.CLOSE_MODAL_LOG,


  }
}


