import React from 'react';
import { Table, Button, Modal, Col, ModalHeader, Input, Row, ModalBody, ModalFooter, Form, Label, FormGroup, Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import { connect } from 'react-redux';
import * as actions from './../actions/actions';

class ModalLog extends React.Component {


  onCloseModal = () => {
    this.props.onCloseModal(this.state);
  }

  render() {
    const { modalLog, contracts, selectAccount } = this.props;
    const item = contracts.map((contract, index) => {
      return (
        <tr key={index}>
          <td className="text-center">{contract.id}</td>
          <td className="text-center" >{contract.code}</td>
          <td className="text-center" >{contract.name}</td>
          <td className="text-center" >09:13 15/08/2019</td>
        </tr>
      )
    });
    if(!selectAccount) return(<div></div>);
    return (
      <Modal isOpen={modalLog} toggle={this.toggle}
        className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Log thông tin tài khoản</ModalHeader>
        <ModalBody>
          <Col sm={{ size: 'auto', offset: 2 }} >
            <FormGroup>
              <Label>Tài khoản</Label> : xxxxxxxx0 {selectAccount.username}
              </FormGroup>
            <FormGroup>
              <Label>Hợp đồng liên quan</Label> : xxxxxxxx{selectAccount.username}
              </FormGroup>
            <FormGroup>
              <Label>Lần đăng nhập đầu tiên</Label> : 09:13 15/08/2019
              </FormGroup>
          </Col>


          <Row className="text-right">
            <Col md={7}></Col>
            <Col md={2}>
              <FormGroup>
                <Input type="select" name="select" id="exampleSelect">
                  <option>Tất cả</option>
                  <option>Hợp đồng</option>

                </Input>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup check inline>
                <Button type="submit" onClick={this.onSearch} color="success" className="mr-2">Tìm kiếm</Button>
                <Button type="reset" onClick={this.onReset} color="info">Reset</Button>
              </FormGroup>

            </Col>
          </Row>

          <Col md={12}>
            <Table className="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th className="text-center" >STT</th>
                  <th className="text-center" >Hợp đồng</th>
                  <th className="text-center" >Thao tác  </th>
                  <th className="text-center" >Thời gian </th>
                </tr>
              </thead>
              <tbody>
                {item}
              </tbody>
            </Table>
          </Col>
        </ModalBody>
        <ModalFooter>
          {/* <Button color="primary" onClick={this.onCloseModal}>Lưu</Button>{' '} */}
          <Button color="secondary" onClick={this.onCloseModal}>Đóng</Button>
        </ModalFooter>
      </Modal>

    );
  }
}
const mapStateToProps = (state) => {
  return {
    modalLog: state.accounts.modalLog,
    contracts: state.contracts.contracts,
    selectAccount: state.accounts.selectAccount
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onCloseModal: () => {
      dispatch(actions.closeModalLog())
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalLog);

