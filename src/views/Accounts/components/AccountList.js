import React from 'react';
import { Table, Button, Modal, Col, ModalHeader, Row, ModalBody, ModalFooter, Form, Label, FormGroup,Pagination, PaginationItem, PaginationLink  } from 'reactstrap';

import AccountItem from './AccountItem'
import {connect} from 'react-redux';
import * as actions from './../actions/actions';
import ModalLog from './ModalLog';
import ModalDetail from './ModalDetail';
import ModalLock from './ModaLock';
import apiCaller from './../../../_helpers/apiCaller';



class AccountList extends React.Component {

  constructor() {
    super();
    this.state = {
      currentPage: 1,
      newsPerPage: 3
    };
  }

  componentDidMount() {
    apiCaller('customer','search', "POST", JSON.stringify({
      "payload": {
        "direction": "Desc",
        "keyWord": "",
        "orderBy": "createdAt",
        "pageNum": 1,
        "pageSize": 10,
        "type": 0
      }
      
    })).then(res => {
      if (res){
          this.props.actFetchAccounts(res.data.payload.list)
        }else{
          // localStorage.clear();
          // this.props.history.push(`/login`);
        }

    })
  }
  chosePage = (event) => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  select = (event) => {
    this.setState({
      newsPerPage: event.target.value
    })
  }
  onCloseModal = () =>{
    this.props.onCloseModal(this.state);
  }
  
  render() {
    const {modal,accounts,modalLog,modalLock} = this.props;
    const currentPage = this.state.currentPage;
    const newsPerPage = this.state.newsPerPage;
    const indexOfLastNews = currentPage * newsPerPage;
    const indexOfFirstNews = indexOfLastNews - newsPerPage;
    const currentTodos = accounts.slice(indexOfFirstNews, indexOfLastNews);
    const renderTodos = currentTodos.map((account, index) => {

        return <AccountItem stt={index + 1 + (currentPage - 1)*newsPerPage} key={index} account={account} />;
      
    });
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(accounts.length / newsPerPage); i++) {
      pageNumbers.push(i);
    }

    return (
      <div>
        <Table className="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>STT</th>
              <th>Tên đăng nhập</th>
              <th>Mã hợp đồng</th>
              <th>CMND/CCCD</th>
              <th>Số điện thoại</th>
              <th>Trạng thái</th>
              <th>Khóa</th>
              <th>Reset mật khẩu</th>
              <th>Xem log</th>
            </tr>
          </thead>
          <tbody>
            {renderTodos}
          </tbody>
        </Table>
       
        <div className="pagination-custom">
        <Pagination>
            {
              pageNumbers.map(number => {
                if (this.state.currentPage === number) {
                  return (
                    <PaginationItem key={number} active>
                      <PaginationLink tag="button" key={number} id={number} >
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  )
                }
                else {
                  return (
                    <PaginationItem key={number} >
                      <PaginationLink key={number} id={number} onClick={this.chosePage} tag="button">
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  )
                }
              })
            }
          </Pagination>
        </div>

        <ModalDetail modaLDetail={modal} />
        <ModalLog modalLog={modalLog} />
        <ModalLock modalLock={modalLock} />
      </div>
    );
  }
}
 const mapStateToProps = (state) =>{
    return {
      modal: state.accounts.modal,
      accounts: state.accounts.accounts,
      modalLog: state.accounts.modalLog,
      modalLock: state.accounts.modalLock,

    }
 }
 const mapDispatchToProps = (dispatch, props) => {
  return {

    onCloseModal: () => {
      dispatch(actions.closeModal())
    },
    actFetchAccounts:()=>{
      dispatch(actions.atcListAllResquest())
    },
    
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(AccountList);

