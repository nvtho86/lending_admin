import React, { Component } from 'react';
import { CustomInput,Badge, Card, CardBody, CardHeader, Col, Row, Table, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/actions';


class AccountForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: '',
      isContract: false,
      isIdentityCard: false,
      type:0
    }
  }
  toggleChangeCode = () => {
    this.setState(prevState => ({
      isContract: !prevState.isContract,
      isIdentityCard:false,
      type:2
     
    }));
  }

  toggleChangeIdentityCard = () => {
    this.setState(prevState => ({
      isContract: false,
      isIdentityCard: !prevState.isIdentityCard,
      type:1
    }));
  }

  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === 'status') {
      value = target.value === 'true' ? true : false;
    }
    this.setState({
      [name]: value
    });
  }
  
  onSearch = (e) => {
    e.preventDefault();
    let { keyword, type } = this.state;
    this.props.onSearch(keyword,type)
  }
  onReset = () => {
    this.setState({
      keyword: '',
    });
    this.props.onReset(this.state);
  }
  render() {
    return (
      <Form>
        {/* <legend className="border-bottom"><Label>Tìm kiếm</Label></legend> */}
        <FormGroup>
          <Col sm={{ size: 12, offset: 3 }}>
            <FormGroup check inline>
              <Label check>Tìm theo</Label>
            </FormGroup>
            <FormGroup check inline>
              <CustomInput type="radio"
                checked={this.state.isContract}
                value='2'
                id="CustomRadio"
                name="code" 
                onChange={this.toggleChangeCode} label="Số hợp đồng" /> 
            </FormGroup>
            <FormGroup check inline>
             <CustomInput  type="radio"
                name="code"
                value="1"
                id="CustomRadio2"
                checked={this.state.isIdentityCard}
                onChange={this.toggleChangeIdentityCard} label="CMND" /> 
            </FormGroup>
          
          </Col>
        </FormGroup>
        <FormGroup >
        <Row form>
            <Col md={1} sm={{ size: 12, offset: 2 }}>
              <FormGroup check inline>
                <Label check>Từ khóa </Label>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Input  type="text"
                name="keyword"
                onChange={this.onChange}
                value={this.state.keyword}
                 />
              </FormGroup>
            </Col>
            <Col md={3} className="text-right">
              <FormGroup check inline>
                <Button type="submit" onClick={this.onSearch} color="success" className="mr-2">Tìm kiếm</Button>
                <Button type="reset" onClick={this.onReset}  color="info">Reset</Button>
              </FormGroup>
            </Col>
          </Row>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
  return {

  }
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSearch:(keyword,type)=>{
      dispatch(actions.actSearchResquest(keyword,type))
    },
    onReset: () => {
      dispatch(actions.atcResetAccountResquest())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountForm);
