import React from 'react';
import { Table, Button, Modal, Col, ModalHeader, Input, Row, ModalBody, ModalFooter, Form, Label, FormGroup, Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import { connect } from 'react-redux';
import * as actions from './../actions/actions';

class ModalLock extends React.Component {


  onCloseModal = () => {
    this.props.onCloseModal(this.state);
  }
  onEnableAccount = () =>{
    this.props.onEnableAccount(this.props.selectAccount.uuid)
  }
  onDisableAccount = () =>{
    this.props.onDisableAccount(this.props.selectAccount.uuid)
  }

  render() {
    const { modalLock, selectAccount } = this.props;
    if(!selectAccount) return(<div></div>);
    return (
      <Modal isOpen={modalLock} toggle={this.toggle}
        className={'modal' + this.props.className}>
        <ModalHeader toggle={this.toggle}>{selectAccount.status!==1?'Xác nhận mở khóa tài khoản khách hàng':'Xác nhận khóa tài khoản khách hàng'}</ModalHeader>
        <ModalBody>
          <Col sm={{ size: 'auto', offset: 3 }}  >
            <FormGroup>
              <Label>Tài khoản</Label> : {selectAccount.username}
              </FormGroup>
          </Col>
          <Row className="text-left">
          <Col md={12}  className="border-top">
            <p color="red">
            {selectAccount.status!==1?'Tài khoản khách hàng hiện đang bị "KHÓA" và không thể sử dụng hệ thống. Việc mở khóa sẽ cấp quyền cho khách hàng sử dụng lại hệ thống với quyền hạng đã được cấp phép trước đó. Vui lòng xác nhận "Mở Khóa" tài khoản '+selectAccount.username+' này':'Khách hàng không thể tiếp tục sử dụng hệ thống sau khi "KHÓA" tài khoản.Vui lòng xác nhận KHÓA tài khoản khách hàng này?'}
              
            </p>
          </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={selectAccount.status!==1?this.onEnableAccount:this.onDisableAccount}>Đồng ý</Button>{' '}
          <Button color="secondary" onClick={this.onCloseModal}>Hủy</Button>
        </ModalFooter>
      </Modal>

    );
  }
}
const mapStateToProps = (state) => {
  return {
    modalLock: state.accounts.modalLock,
    selectAccount: state.accounts.selectAccount
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onCloseModal: () => {
      dispatch(actions.actCloseModalLock())
    },
    onEnableAccount: (id) => {
      dispatch(actions.actEnableAccountkResquest(id))
    },
    onDisableAccount: (id) => {
      dispatch(actions.actDisableAccountResquest(id))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalLock);

