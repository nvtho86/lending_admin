import React from 'react';
import { Badge, Input, Button} from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/actions';

class AccountItem extends React.Component {


  constructor(props) {
    super(props);
    this.state={
        status:0
    }
  }


  onDetail = () => {
    this.props.onDetail(this.props.account.uuid)

  }
  onLog = () => {
    this.props.onLog(this.props.account.uuid)

  }
  onChange =()=>{
    this.setState({
      status:this.props.account.status
    })
  }
  onLock = () =>{
    // if(confirm("Bạn chắc chắn muốn khóa tài khoản này?")){ //eslint-disable-line
    //   console.log('aaaaaaaaaaaâ')
    // }
    this.props.onLock(this.props.account.uuid)
  }
  iconLock = (status) =>{
    let icon = 'fa fa-unlock';
    if(status!==1){
      icon = 'fa fa-lock';
    }
    return icon
  }
  render() {
    var {account,index,stt} = this.props;
    return (
      <tr key={index}>
        <td className="text-center">{stt}</td>
        <td>{account.username}</td>
        <td>
        <Link to="#"  onClick={this.onDetail}>XXXXXX{stt}</Link>
        </td>
        <td>{account.identityCardNumber}</td>
        <td>{account.phoneNumber}</td>
        <td>Hoạt động bình thường</td>
        <td className="text-center"><span  onChange={this.onChange} onClick={this.onLock} ><i className={this.iconLock(account.status)}> </i></span></td>
        <td className="text-center"><Link to="#">Reset</Link> </td>
        <td>
          <button className="btn btn-sm btn-warning" onClick={this.onLog} >Xem log</button >
        </td>
      </tr>

    );
  }
}
const mapStateToProps = (state) => {
  return {

  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onLog: (id) => {
      dispatch(actions.actLogResquest(id));
    },
    onDetail: (id) => {
      dispatch(actions.atcDetailResquest(id));
    },
    onLock: (id) => {
      dispatch(actions.actLockResquest(id));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AccountItem);
