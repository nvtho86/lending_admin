import React, { Component } from 'react';
import CheckProfilesItem from './CheckProfilesItem';
import { Table, Pagination, PaginationItem, PaginationLink, Button, Col, Row, Input, Form, FormGroup } from 'reactstrap';

class CheckProfilesList extends Component {

  constructor() {
    super();
    this.state = {
      currentPage: 1,
      newsPerPage: 3
    };
  }

  chosePage = (event) => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  select = (event) => {
    this.setState({
      newsPerPage: event.target.value
    })
  }
  render() {
    const currentPage = this.state.currentPage;
    const newsPerPage = this.state.newsPerPage;
    const indexOfLastNews = currentPage * newsPerPage;
    const indexOfFirstNews = indexOfLastNews - newsPerPage;
    const currentTodos = newsList.slice(indexOfFirstNews, indexOfLastNews);
    const renderTodos = currentTodos.map((todo, index) => {
      return <CheckProfilesItem stt={index + 1 + (currentPage - 1) * newsPerPage} key={index} data={todo} />;
    });
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(newsList.length / newsPerPage); i++) {
      pageNumbers.push(i);
    }

    return (
      <Row>
        <Col md={12}>
          <Table className="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th className="text-center" >STT</th>
                <th className="text-center" >Thông tin</th>
                <th className="text-center" >Thông tin hiện tại</th>
                {/* <th className="text-center">Hành động</th> */}
                <th className="text-center" >Thông tin điều chỉnh</th>
              </tr>
            </thead>
            <tbody>
              {renderTodos}
            </tbody>
          </Table>
        </Col>
        {/* <div className="news-per-page">
          <select defaultValue="0" onChange={this.select} >
            <option value="0" disabled>Get by</option>
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="7">7</option>
          </select>
        </div> */}

        <Col md={12} className="text-right">
          <FormGroup check inline>
            <Button type="reset" onClick={this.onReset} color="btn btn-secondary">Hủy</Button>
            <Button type="submit" onClick={this.onSearch} color="primary" className="btn ml-2">Lưu</Button>
          </FormGroup>
        </Col>

      </Row>

    );
  }
}


export default CheckProfilesList;
var newsList = [
  {
    "id": "abc01",
    "lastname": "Văn Thơ",
    "firstname": "Nguyễn",
    "content": "ct1"
  },
  {
    "id": "abc01",
    "lastname": " Vinh Hao",
    "firstname": "Nguyễn",
    "content": "ct2"
  },
  {
    "id": "abc01",
    "lastname": "Văn Thai",
    "firstname": "Nguyễn",
    "content": "ct2"
  },
  {
    "id": "abc01",
    "lastname": "Van Van",
    "firstname": "Nguyễn",
    "content": "ct2"
  },
]




