import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table, Input } from 'reactstrap';
import { Link } from 'react-router-dom';


class CheckProfilesItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hiddeInput: false
    }
  }
  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;

    this.setState({
      [name]: value
    });
  }
  onShowInput = () => {
    this.setState({ hiddeInput: !this.state.hiddeInput });

  }


  render() {
    return (
      <tr>
        <td className="text-center">{this.props.stt}</td>
        <td>{this.props.data.lastname}</td>
        <td>
          <div>
            <div className="col-8 text-left check d-inline-block">{this.props.data.firstname}</div>
            <div className="col-4 text-right d-inline-block">
              <button type="button" className="btn btn-sm btn-edit ml-lg-5" onClick={this.onShowInput} ><i className="fa fa-edit"></i></button>
            </div>
          </div>
        </td>
        <td>
          <Input type={this.state.hiddeInput ? "" : "hidden"} color="secondary" onChange={this.onChange} />
        </td>
      </tr>
    );
  }
}
export default CheckProfilesItem;
