import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table, Form, Label, Button, FormGroup, Input } from 'reactstrap';
import { Link } from 'react-router-dom';


class ContractForm extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      id: '',
      name: '',
      address: '',
      status: false,
    }
  }
  componentWillMount() {
    /** kiem tra cap nhat */
    if (this.props.contractEditing) {
      this.setState({
        id: this.props.contractEditing.id,
        name: this.props.contractEditing.name,
        address: this.props.contractEditing.address,
        status: this.props.contractEditing.status,
      });
    }
  }
  /** push data to field */
  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.contractEditing) {
      this.setState({
        id: nextProps.contractEditing.id,
        name: nextProps.contractEditing.name,
        address: nextProps.contractEditing.address,
        status: nextProps.contractEditing.status,
      });
    }
  }
  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === 'status') {
      value = target.value === 'true' ? true : false;
    }
    this.setState({
      [name]: value
    });
  }
  onSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state)
    this.setState({
      id: '',
      name: '',
      address: '',
      status: false
    });
  }
  render() {
    var { id } = this.state;
    return (
      <div className="form-group">
        <Form>
          {/* <legend className="border-bottom"><Label>Tìm kiếm</Label></legend> */}
          <Row form>
            <Col md={1} sm={{ size: 12, offset: 2 }}>
              <FormGroup check inline>
                <Label check>Từ khóa </Label>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Input type="text"
                  name="keyword"
                  onChange={this.onChange}
                  value={this.state.keyword}
                />
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup check inline>
                <Button type="submit" onClick={this.onSearch} color="success" className="btn mr-2">Tìm kiếm</Button>
                <Button type="reset" onClick={this.onReset} color="info">Reset</Button>
              </FormGroup>

            </Col>
          </Row>
          
        </Form>
      </div>
    );
  }
}


export default ContractForm;
