import React, { Component } from 'react';
// import { Row, Col, Card, CardBody, CardHeader,Button, Col, Row, Input, Form, FormGroup  } from 'reactstrap';
import {Card, CardBody, CardHeader, Table, Button, Modal, Col, ModalHeader, Row, ModalBody, ModalFooter, Form, Label, FormGroup,Pagination, PaginationItem, PaginationLink  } from 'reactstrap';

import CheckProfilesForm from './components/CheckProfilesForm';
import CheckProfilesList from './components/CheckProfilesList';



class ApprovalSchedule extends Component {


  render() {

    return (
      <div className="animated fadeIn">
        <Row >
          <Col xl={12}>
            <Card>
              <CardHeader>
                <h3>Tìm kiếm</h3>
              </CardHeader>
              <CardBody>
              <CheckProfilesForm/>
              {/* <CheckProfilesList/> */}
              </CardBody>
            </Card>
          </Col>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <h3>Danh sách kiểm tra hồ sơ</h3>
                {/* <legend className="border-bottom"><Label>Thông tin hợp đồng</Label></legend> */}
                <Row form className="mt-2">
            <Col md={3} sm={{ offset: 2 }}>
              <FormGroup check >
                <Label check> Số hợp đồng </Label> : <b>0000000001</b>
              </FormGroup>
            </Col>
            <Col md={5}>
              <FormGroup check >
                <Label check> Trạng thái </Label> : <b>Document verification</b>
              </FormGroup>
              <FormGroup check >
                <Label check> Ngày bắt đầu </Label> : <b>16/10/2019</b>
              </FormGroup>
            </Col>
          </Row>
              </CardHeader>
              <CardBody>
              <CheckProfilesList/>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ApprovalSchedule;
// export default connect(mapStateToProps, null)(ContractLists);

