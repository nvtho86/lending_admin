import * as types from './../constants/ActionTypes';

export const listAll = () => {
  return {
    type: types.LIST_ALL_CONTRACT
  }
}
export const addContract = () => {
  return {
    type: types.ADD_CONTRACT,
    contracts:{
      id:3,
      name: 1,
      address: "HCM",
      status: 1
    }
  }
}
export const updateContract = (id) => {
  return {
    type: types.UPDATE_CONTRACT,
    id
  }
}
export const deleteContract = (id) => {
  return {
    type: types.DELETE_CONTRACT,
    id
  }
}


