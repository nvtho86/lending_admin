import * as types from './../constants/ActionTypes';

// export const listAll = () => {
//   return {
//     type: types.LIST_ALL_CONTRACT
//   }
// }
// export const addContract = () => {
//   return {
//     type: types.ADD_CONTRACT,
//     contracts:{
//       id:3,
//       name: 1,
//       address: "HCM",
//       status: 1
//     }
//   }
// }
// export const updateContract = (id) => {
//   return {
//     type: types.UPDATE_CONTRACT,
//     id
//   }
// }
// export const deleteContract = (id) => {
//   return {
//     type: types.DELETE_CONTRACT,
//     id
//   }
// }
// export const searchContract = (keyword) => {
//   return {
//     type: types.SEARCH_CONTRACT,
//     keyword
//   }
// }
// export const resetTable = () => {
//   return {
//     type: types.RESET_TATBLE_CONTRACT,

//   }
// }
export const checkContract = (id) => {
  return {
    type: types.CHECK_CONTRACT_APPROVAL_SCHEDULES,
    id

  }
}
export const detail = (id) => {
  return {
    type: types.DETAIL_APPROVAL_SCHEDULES,
    id

  }
}
export const closeModal = () => {
  return {
    type: types.CLOSE_MODAL_APPROVAL_SCHEDULES,


  }
}
export const closeModalCheck = () => {
  return {
    type: types.CLOSE_MODAL_CHECK_APPROVAL_SCHEDULES,


  }
}

export const backToList = () => {
  return {
    type: types.BACK_TO_LIST_APPROVAL_SCHEDULES,

  }
}


