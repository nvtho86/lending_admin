import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  Card,
  CardBody,
  CardHeader,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  ModalHeader
} from 'reactstrap';
import './style/style.css';
import ApprovalSchedulesForm from './components/ApprovalSchedulesForm';
import ApprovalSchedulesList from './components/ApprovalSchedulesList';
import ApprovalSchedulesConfirm from "./components/ApprovalSchedulesConfirm";


class ApprovalSchedule extends Component {

  render() {
    console.log(this.props.showDetail);

    return (
      <div className="animated fadeIn">
        <Row >
          {!this.props.showDetail &&
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <h3>Danh sách hợp đồng điều chỉnh</h3>
                </CardHeader>
                <CardBody>
                  <ApprovalSchedulesForm/>
                  <ApprovalSchedulesList/>
                </CardBody>
              </Card>
            </Col>
          }
          {this.props.showDetail &&
          <Col xl={12}>
            <Card>
              <CardHeader>
                <h3>Xác nhận thông tin hồ sơ</h3>
              </CardHeader>
              <CardBody>
                <ApprovalSchedulesConfirm />
              </CardBody>
            </Card>
          </Col>
          }
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    showDetail: state.approvalSchedules.showDetail
  }
}

// export default ApprovalSchedule;
export default connect(mapStateToProps, null)(ApprovalSchedule);

var newsList = [
  {
    "id": "abc01",
    "code":"000001",
    "approval":true,
    "title": "The Highs and Lows of Life as a Black Editor in Chief",
    "content": "ct1"
  },
  {
    "id": "abc02",
    "code":"000001",
    "approval":true,
    "title": "The Real Reason Apple Wants You to Use Its Sign-in Service",
    "content": "ct2"
  },
  {
    "id": "abc03",
    "code":"000001",
    "approval":true,
    "title": "Men Need To Think More About Fertility",
    "content": "ct3"
  },
  {
    "id": "abc04",
    "code":"000001",
    "approval":true,
    "title": "Reactive Streams and Kotlin Flows",
    "content": "ct4"
  },
  {
    "id": "abc05",
    "code":"000001",
    "approval":true,
    "title": "The Incredible Creative Power of the Index Card",
    "content": "ct5"
  },
  {
    "id": "abc06",
    "code":"000001",
    "approval":true,
    "title": "The Man Who Helped the Beatles Admit It’s Getting Better",
    "content": "ct6"
  },
  {
    "id": "abc07",
    "code":"000001",
    "approval":true,
    "title": "Facebook Can Resolve Its Issues — How Will We Resolve Ours?",
    "content": "ct7"
  },
  {
    "id": "abc08",
    "code":"000001",
    "approval":true,
    "title": "The Personal Newsletter Fad Needs to End",
    "content": "ct8"
  },
  {
    "id": "abc09",
    "code":"000001",
    "approval":true,
    "title": "How Do You Know You Have a Good Idea?",
    "content": "ct9"
  },
  {
    "id": "abc10",
    "code":"000001",
    "approval":true,
    "title": "Ronaldo & Messi",
    "content": "ct10"
  }
]

// // class TableItem extends React.Component {
// //   render() {

// //     return (
// //       <tr>
// //           <td>{this.props.stt}</td>
// //           <td>{this.props.data.code}</td>
// //           <td>{this.props.data.title}</td>
// //           <td>{this.props.data.content}</td>
// //           <td>{this.props.stt}</td>
// //           <td>{this.props.stt}</td>
// //           <td>{this.props.stt}</td>
// //           <td>
// //           <button type="button" className="btn mr-2 btn-warning" onClick={this.onDetail} >Kiểm tra</button>
// //             <span>{this.props.data.approval}</span>
// //           </td>
// //         </tr>
// //     )
// //   }
// // }

// class TableList extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       currentPage: 1,
//       newsPerPage: 3
//     };
//   }

//   chosePage = (event) => {
//     this.setState({
//       currentPage: Number(event.target.id)
//     });
//   }

//   select = (event) => {
//     this.setState({
//       newsPerPage: event.target.value
//     })
//   }

//   render() {
//     const currentPage = this.state.currentPage;
//     const newsPerPage = this.state.newsPerPage;
//     const indexOfLastNews = currentPage * newsPerPage;
//     const indexOfFirstNews = indexOfLastNews - newsPerPage;
//     const currentTodos = newsList.slice(indexOfFirstNews, indexOfLastNews);
//     const renderTodos = currentTodos.map((todo, index) => {
//       return <TableItem stt={index + 1 + (currentPage - 1)*newsPerPage} key={index} data={todo} />;
//     });
//     const pageNumbers = [];
//     for (let i = 1; i <= Math.ceil(newsList.length / newsPerPage); i++) {
//       pageNumbers.push(i);
//     }

//     return (
//       <div>
//         <Table>
//           <thead>
//             <tr>
//               <th>STT</th>
//               <th>Số hợp đồng</th>
//               <th>Tên khách hàng</th>
//               <th>Khoản vay</th>
//               <th>Loại vay</th>
//               <th>Trạng thái</th>
//               <th>Ngày điều chỉnh</th>
//               <th>Duyệt điều chỉnh</th>
//             </tr>
//           </thead>
//           <tbody>
//             {renderTodos}
//           </tbody>
//         </Table>
//         <div className="news-per-page">
//           <select defaultValue="0" onChange={this.select} >
//             <option value="0" disabled>Get by</option>
//             <option value="3">3</option>
//             <option value="5">5</option>
//             <option value="7">7</option>
//           </select>
//         </div>
//         <div className="pagination-custom">
//         <Pagination>
//             {
//               pageNumbers.map(number => {
//                 if (this.state.currentPage === number) {
//                   return (
//                     <PaginationItem active>
//                       <PaginationLink tag="button" key={number} id={number} >
//                         {number}
//                       </PaginationLink>
//                     </PaginationItem>
//                   )
//                 }
//                 else {
//                   return (
//                     <PaginationItem >
//                       <PaginationLink key={number} id={number} onClick={this.chosePage} tag="button">
//                         {number}
//                       </PaginationLink>
//                     </PaginationItem>
//                   )
//                 }
//               })
//             }
//           </Pagination>
//         </div>
//       </div>
//     );
//   }

// }




