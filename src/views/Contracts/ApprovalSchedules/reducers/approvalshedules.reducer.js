import * as types from './../constants/ActionTypes';
// var data = JSON.parse(localStorage.getItem('contracts'));
// var initialState = data ? data : [];
var data = [
  {
    "id": "abc01",
    "code":"000001",
    "approval":true,
    "title": "The Highs and Lows of Life as a Black Editor in Chief",
    "content": "ct1"
  },
  {
    "id": "abc02",
    "code":"000001",
    "approval":true,
    "title": "The Real Reason Apple Wants You to Use Its Sign-in Service",
    "content": "ct2"
  },
  {
    "id": "abc03",
    "code":"000001",
    "approval":true,
    "title": "Men Need To Think More About Fertility",
    "content": "ct3"
  },
  {
    "id": "abc04",
    "code":"000001",
    "approval":true,
    "title": "Reactive Streams and Kotlin Flows",
    "content": "ct4"
  },
  {
    "id": "abc05",
    "code":"000001",
    "approval":true,
    "title": "The Incredible Creative Power of the Index Card",
    "content": "ct5"
  },
  {
    "id": "abc06",
    "code":"000001",
    "approval":true,
    "title": "The Man Who Helped the Beatles Admit It’s Getting Better",
    "content": "ct6"
  },
  {
    "id": "abc07",
    "code":"000001",
    "approval":true,
    "title": "Facebook Can Resolve Its Issues — How Will We Resolve Ours?",
    "content": "ct7"
  },
  {
    "id": "abc08",
    "code":"000001",
    "approval":true,
    "title": "The Personal Newsletter Fad Needs to End",
    "content": "ct8"
  },
  {
    "id": "abc09",
    "code":"000001",
    "approval":true,
    "title": "How Do You Know You Have a Good Idea?",
    "content": "ct9"
  },
  {
    "id": "abc10",
    "code":"000001",
    "approval":true,
    "title": "Ronaldo & Messi",
    "content": "ct10"
  }
]
const initialState = {
  approvalSchedules: data ? data : [],
  keyword: '',
  // isCode: false,
  // isPhone: false,
  // isIdentityCard: false,
  // isAll: true,
  // optionSearch: 1,
  modalCheck:false,
  modal: false,
  showDetail: false
};


var findIndex = (approvalSchedules, id) => {
  var result = -1;
  approvalSchedules.forEach((approvalSchedule, index) => {
    if (approvalSchedules.id === id) {
      result = index
    }
  })
  return result;
}
var approvalSchedules = (state = initialState, action) => {
  switch (action.type) {

    case types.DETAIL_APPROVAL_SCHEDULES:
      var id = action.id;
      var index = findIndex(state.approvalSchedules, id);
      state.approvalSchedules[index] = {
        ...state.approvalSchedules[index],
      }
      return {
        ...state,
        modal: !state.modal,
      };

    case types.CLOSE_MODAL_APPROVAL_SCHEDULES:
        return {
          ...state,
          modal: !state.modal
        };

    case types.CHECK_CONTRACT_APPROVAL_SCHEDULES:
        return {
          ...state,
          // modalCheck: !state.modalCheck,
          showDetail: !state.showDetail
        };
    case types.CLOSE_MODAL_CHECK_APPROVAL_SCHEDULES:
      return {
        ...state,
        modalCheck: !state.modalCheck
      };

    case types.BACK_TO_LIST_APPROVAL_SCHEDULES:
      return {
        ...state,
        showDetail: !state.showDetail
      }
    default:
      return state;
  }
}
export default approvalSchedules;
