import React from 'react';
import { Table, Button, Modal, Col, ModalHeader, Row, ModalBody, ModalFooter, Form, Label, FormGroup,Pagination, PaginationItem, PaginationLink  } from 'reactstrap';

import ApprovalSchedulesItem from './ApprovalSchedulesItem'
import {connect} from 'react-redux';
import * as actions from './../actions/actions';
import ModalCheck from './ModalCheck';
import ModalDetail from './ModalDetail';



class ApprovalSchedulesList extends React.Component {

  constructor() {
    super();
    this.state = {
      currentPage: 1,
      newsPerPage: 3
    };
  }

  chosePage = (event) => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  select = (event) => {
    this.setState({
      newsPerPage: event.target.value
    })
  }
  onCloseModal = () =>{
    this.props.onCloseModal(this.state);
  }


  render() {
    const {modal, approvalSchedules,modalCheck} = this.props;
    const currentPage = this.state.currentPage;
    const newsPerPage = this.state.newsPerPage;
    const indexOfLastNews = currentPage * newsPerPage;
    const indexOfFirstNews = indexOfLastNews - newsPerPage;
    const currentTodos = approvalSchedules.slice(indexOfFirstNews, indexOfLastNews);
    const renderTodos = currentTodos.map((todo, index) => {
      return <ApprovalSchedulesItem stt={index + 1 + (currentPage - 1)*newsPerPage} key={index} data={todo} />;
    });
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(approvalSchedules.length / newsPerPage); i++) {
      pageNumbers.push(i);
    }

    return (
      <div>
        <Table className="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>STT</th>
              <th>Số hợp đồng</th>
              <th>Tên khách hàng</th>
              <th>Khoản vay</th>
              <th>Loại vay</th>
              <th>Trạng thái</th>
              <th>Ngày điều chỉnh</th>
              <th>Duyệt điều chỉnh</th>
            </tr>
          </thead>
          <tbody>
            {renderTodos}
          </tbody>
        </Table>
        <div className="news-per-page">
          <select defaultValue="0" onChange={this.select} >
            <option value="0" disabled>Get by</option>
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="7">7</option>
          </select>
        </div>
        <div className="pagination-custom">
        <Pagination>
            {
              pageNumbers.map(number => {
                if (this.state.currentPage === number) {
                  return (
                    <PaginationItem active>
                      <PaginationLink tag="button" key={number} id={number} >
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  )
                }
                else {
                  return (
                    <PaginationItem >
                      <PaginationLink key={number} id={number} onClick={this.chosePage} tag="button">
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  )
                }
              })
            }
          </Pagination>
        </div>

        <ModalDetail modaLDetail={modal} />
        <ModalCheck modalCheck={modalCheck} />
      </div>
    );
  }
}
 const mapStateToProps = (state) =>{
   console.log(state.approvalSchedules.modalCheck)
    return {
      modal: state.approvalSchedules.modal,
      approvalSchedules: state.approvalSchedules.approvalSchedules,
      modalCheck: state.approvalSchedules.modalCheck,

    }
 }
 const mapDispatchToProps = (dispatch, props) => {
  return {

    onCloseModal: () => {
      dispatch(actions.closeModal())
    }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ApprovalSchedulesList);

