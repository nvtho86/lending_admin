import React from 'react';
import { Table, Button, Modal, Col, ModalHeader, Input, Row, ModalBody, ModalFooter, Form, Label, FormGroup, Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import ApprovalSchedulesItem from './ApprovalSchedulesItem'
import { connect } from 'react-redux';
import * as actions from './../actions/actions';

class ModalCheck extends React.Component {


  onCloseModal = () => {
    this.props.onCloseModal(this.state);
  }

  render() {
    const { modalCheck, contracts } = this.props;
    const item = contracts.map((contract, index) => {
      return (
        <tr>
          <td className="text-center" >{contract.id}</td>
          <td className="text-center" >{contract.code}</td>

          <td>
            <div className="row">
              <div className="col-8 text-left">{contract.name}</div>
              <div className="col-4 text-right">
                <button type="button" className="btn btn-sm btn-warning" onClick={this.onShowInput} >Sửa</button>
              </div>
            </div>
          </td>
          <td className="text-center" ><Input /></td>
        </tr>
      )
    });
    return (
      <Modal isOpen={modalCheck} toggle={this.toggle}
        className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Xác nhận thông tin hồ sơ</ModalHeader>
        <ModalBody>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label>Số hợp đồng</Label>: <span className="font-weight-bold">029348204234</span>
              </FormGroup>
              <FormGroup>
                <Label>Trạng thái hợp đồng</Label>: <span className="font-weight-bold">Đã Duyệt</span>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label>Thời gian yêu cầu điều chỉnh</Label>: <span className="font-weight-bold">09:45 20/10/2019</span>
              </FormGroup>
              <FormGroup>
                <Label>Người yêu cầu điều chỉnh</Label>: <span className="font-weight-bold">ntho00001</span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Table className="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th className="text-center" >#</th>
                    <th className="text-center" >Thông tin</th>
                    <th className="text-center" >Thông tin hiện tại</th>
                    <th className="text-center" >Thông tin điều chỉnh</th>
                  </tr>
                </thead>
                <tbody>
                  {item}
                </tbody>
              </Table>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.onCloseModal}>Lưu</Button>{' '}
          <Button color="secondary" onClick={this.onCloseModal}>Đóng</Button>
        </ModalFooter>
      </Modal>

    );
  }
}
const mapStateToProps = (state) => {
  return {
    modalCheck: state.approvalSchedules.modalCheck,
    contracts: state.contracts.contracts
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onCloseModal: () => {
      dispatch(actions.closeModalCheck())
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalCheck);
