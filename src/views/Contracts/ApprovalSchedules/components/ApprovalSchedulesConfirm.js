import React from 'react';
import {
  Table,
  Button,
  Modal,
  Col,
  ModalHeader,
  Row,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  Pagination,
  PaginationItem,
  PaginationLink,
  Input
} from 'reactstrap';

import { connect } from 'react-redux';
import * as actions from './../actions/actions';

class ApprovalSchedulesConfirm extends React.Component {


  onBackToList = () => {
    this.props.onBackToList();
  }

  render() {
    const { contracts } = this.props;
    const item = contracts.map((contract, index) => {
      return (
        <tr>
          <td className="text-center" >{contract.id}</td>
          <td className="text-center" >{contract.code}</td>

          <td>
            <div className="row">
              <div className="col-8 text-left">{contract.name}</div>
              <div className="col-4 text-right">
                <button type="button" className="btn btn-warning" onClick={this.onShowInput} >Sửa</button>
              </div>
            </div>
          </td>
          <td className="text-center" ><Input /></td>
        </tr>
      )
    });
    return (
      <div>
        <Col md={12}>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label>Số hợp đồng</Label>: <span className="font-weight-bold">029348204234</span>
              </FormGroup>
              <FormGroup>
                <Label>Trạng thái hợp đồng</Label>: <span className="font-weight-bold">Đã Duyệt</span>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label>Thời gian yêu cầu điều chỉnh</Label>: <span className="font-weight-bold">09:45 20/10/2019</span>
              </FormGroup>
              <FormGroup>
                <Label>Người yêu cầu điều chỉnh</Label>: <span className="font-weight-bold">ntho00001</span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Table striped bordered hover variant="dark">
                <thead>
                <tr>
                  <th className="text-center" >#</th>
                  <th className="text-center" >Thông tin</th>
                  <th className="text-center" >Thông tin hiện tại</th>
                  <th className="text-center" >Thông tin điều chỉnh</th>
                </tr>
                </thead>
                <tbody>
                {item}
                </tbody>
              </Table>
            </Col>
          </Row>
        </Col>
        <div className="text-right">
          <Button color="primary" className="mr-2" onClick={this.onBackToList}>Lưu</Button>
          <Button outline color="info" onClick={this.onBackToList}>Back To List</Button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    contracts: state.contracts.contracts
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onBackToList: () => {
      dispatch(actions.backToList())
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ApprovalSchedulesConfirm);
