import React from 'react';
import { Badge } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/actions';

class ApprovalSchedulesItem extends React.Component {


  onDetail = () => {

    this.props.onDetail(this.props.data.id)
  }
  onCheck = () => {
    this.props.onCheck(this.props.data.id)

  }
  render() {
    // var { } = this.props;
    return (
      <tr>
        <td className="text-center">{this.props.stt}</td>
        <td>
          <a className="outline" onClick={this.onDetail} >{this.props.data.code}</a>
        </td>
        <td>{this.props.data.title}</td>
        <td>{this.props.data.content}</td>
        <td>{this.props.stt}</td>
        <td>{this.props.stt}</td>
        <td>{this.props.stt}</td>
        <td>
          <button type="button" className="btn btn-sm mr-2 btn-warning" onClick={this.onCheck} >Kiểm tra</button>
          <span>{this.props.data.approval}</span>
        </td>
      </tr>

    );
  }
}
const mapStateToProps = (state) => {
  return {

  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onCheck: () => {
      dispatch(actions.checkContract());
    },
    onDetail: () => {
      dispatch(actions.detail());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ApprovalSchedulesItem);
