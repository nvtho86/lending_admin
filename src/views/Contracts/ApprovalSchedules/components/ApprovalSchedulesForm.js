import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/actions';


class ApprovalSchedulesForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
     keyword:'',
      status: false,
    }
  }

  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === 'status') {
      value = target.value === 'true' ? true : false;
    }
    this.setState({
      [name]: value
    });
  }
  onSearch =(e) =>{
    e.preventDefault();
  }
  render() {
    var { id } = this.state;
    return (

      <Form>
          {/* <legend className="border-bottom"><Label>Tìm kiếm</Label></legend> */}
        <FormGroup >
          <Row form>
            <Col md={1} sm={{ size: 12, offset: 1 }}>
              <FormGroup check inline>
                <Label check>Từ khóa </Label>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Input type="text"
                name="keyword"
                onChange={this.onChange}
                value={this.state.keyword}
                 />
              </FormGroup>
            </Col>
            <Col md={1} sm={{ size: 12, offset: 1 }}>
              <FormGroup check inline>
                <Label check>Trạng thái </Label>
              </FormGroup>
            </Col>
            <Col md={2}>
              <FormGroup>
              <Input type="select" name="select" id="exampleSelect">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Input>
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup check inline>
                <Button type="submit" onClick={this.onSearch} color="success" className="mr-2">Tìm kiếm</Button>
                <Button type="reset" onClick={this.onReset}  color="info">Reset</Button>
              </FormGroup>

            </Col>
          </Row>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = (state) => {
   return {

   }
};
const mapDispatchToProps = (dispatch,props) => {
   return {
      // onAddUser:(user)=>{
      //   dispatch(actions.AddUser(user))
      // }
   }
}

export default connect(mapStateToProps,mapDispatchToProps)(ApprovalSchedulesForm);
