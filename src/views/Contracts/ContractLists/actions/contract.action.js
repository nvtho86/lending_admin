import * as types from './../constants/ActionTypes';
import apiCaller from './../../../../_helpers/apiCaller';

export const atcContractListResquest = () => {
  return (dispatch) => {
    return apiCaller('contract','list', 'POST',
      JSON.stringify({
        "payload": {}
      })
    ).then(res=>{
      if(res){
        dispatch(actListAll(res.data.payload))
      }
    })
  };
}

export const actListAll = (contracts) => {
  return {
    type: types.LIST_ALL_CONTRACT,
    contracts
  }
}
export const addContract = () => {
  return {
    type: types.ADD_CONTRACT,
    contracts:{
      id:3,
      name: 1,
      address: "HCM",
      status: 1
    }
  }
}
export const updateContract = (id) => {
  return {
    type: types.UPDATE_CONTRACT,
    id
  }
}
export const deleteContract = (id) => {
  return {
    type: types.DELETE_CONTRACT,
    id
  }
}
export const searchContract = (keyword) => {
  return {
    type: types.SEARCH_CONTRACT,
    keyword
  }
}
export const resetTable = () => {
  return {
    type: types.RESET_TATBLE_CONTRACT,

  }
}
export const atcDetailContractResquest = (id) => {
  return (dispatch) => {
    return apiCaller('contract','detail', 'POST',
      JSON.stringify({
        "payload": {
          "id":id
        }
      })
    ).then(res=>{
      if(res){
        dispatch(actDetailContract(res.data.payload))
      }
    })
  };
}
export const actDetailContract = (detail) => {
  return {
    type: types.DETAIL_CONTRACT,
    detail

  }
}
export const closeModal = () => {
  return {
    type: types.CLOSE_MODAL_CONTRACT,


  }
}


