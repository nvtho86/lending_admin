import * as types from './../constants/ActionTypes';
// var data = JSON.parse(localStorage.getItem('contracts'));
// var initialState = data ? data : [];
var data = [
  {
    id: 1,
    code: '111111111',
    name: 'TVO',
    loanAmount: '500000000', // khoản vay
    typeOfLoan: 'Vay mua xe', // loại vay
    identityCard: '555555555', // CMND
    address: 'HCM',
    phone: '0938582610',
    status: false,
  },
  {
    id: 2,
    code: '222222222',
    name: 'Thai Vinh',
    loanAmount: '100000000', // khoản vay
    typeOfLoan: 'Vay mua die thoai', // loại vay
    identityCard: '311111111', // CMND
    address: 'HCM',
    phone: '0938582610',
    status: false,
  },
  {
    id: 3,
    code: '333333333',
    name: 'Quang Vinh',
    loanAmount: '1000000', // khoản vay
    typeOfLoan: 'Vay tien mat', // loại vay
    identityCard: '222222222', // CMND
    address: 'HCM',
    phone: '0938582610',
    status: false,
  }
];
const initialState = {
  contracts: [],
  keyword: '',
  isCode: false,
  isPhone: false,
  isIdentityCard: false,
  isAll: true,
  optionSearch: 1,
  modal: false,
  detail:[],
};

const s4 = () => {
  return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}
const generatesID = () => {
  return s4() + s4() + '-' + s4() + s4() + '-' + s4() + s4() + '-' + s4() + s4();
}
var findIndex = (contracts, id) => {
  var result = -1;
  contracts.forEach((contract, index) => {
    if (contract.id === id) {
      result = index
    }
  })
  return result;
}

var contracts = (state = initialState, action) => {
  switch (action.type) {
    case types.LIST_ALL_CONTRACT:
    return {
      ...state,
      contracts: action.contracts
    };
    case types.ADD_CONTRACT:
      return [...state, {
        id: 3,
        name: 1,
        address: "HCM",
        status: 1
      }];
    // localStorage.setItem('contracts', JSON.stringify(state))
    case types.UPDATE_CONTRACT:
      return state
    case types.DELETE_CONTRACT:
      return state
    case types.SEARCH_CONTRACT:
      const { keyword, isCode, isPhone, isIdentityCard, isAll } = action.keyword;
      // const contractLists = state.contracts.filter((contract) => contract.name.indexOf(keyword.toString())!==-1);
      return {
        ...state,
        keyword: keyword,
        isCode: isCode,
        isPhone: isPhone,
        isIdentityCard: isIdentityCard,
        isAll: isAll
      };
    case types.RESET_TATBLE_CONTRACT:

      return { ...state, keyword: '' };
    case types.DETAIL_CONTRACT:
      return {...state, detail:action.detail, modal: true };

    case types.CLOSE_MODAL_CONTRACT:
        return {...state, modal: false };
    default:
      return state;
  }
}
export default contracts;
