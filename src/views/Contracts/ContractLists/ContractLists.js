import React, { Component } from 'react';
import { Row, Col, Card, CardBody, CardHeader } from 'reactstrap';
import * as actions from './actions/contract.action';
import { connect } from "react-redux";
import ContractList from './components/ContractList';
import ContractForm from './components/ContractForm';
import contracts from './reducers/contracts.reducer';
import apiCaller from './../../../_helpers/apiCaller'


// import TrackList from './components/TrackList';
// import {configureStore} from './store';
// import * as actions from './actions';

class ContractLists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // contracts: [],
      contractEditing: null

    }
  }
  
  

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  generatesID() {
    return this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4() + '-' + this.s4() + this.s4();
  }



  /** add new */
  onSubmit = (data) => {

    var { contracts } = this.state;
    if (data.id === '') {
      data.id = this.generatesID();
      contracts.push(data);
    } else {
      var index = this.findIndex(data.id)
      contracts[index] = data;
      /** sau khi luu thi xoa trong form  */
      this.setState({
        contractEditing: null
      });
    }
    this.setState({
      contracts: contracts
    });
    localStorage.setItem('contracts', JSON.stringify(contracts));

  }

  /** edit */
  onEdit = (id) => {
    var { contracts } = this.state;
    var index = this.findIndex(id);
    var contractEditing = contracts[index];
    this.setState({
      contractEditing: contractEditing
    });
  }

  /** delete */
  onDelete = (id) => {
    var { contracts } = this.state;
    var index = this.findIndex(id);
    if (index !== -1) {
      contracts.splice(index, 1)
      this.setState({ contracts: contracts });
      localStorage.setItem('contracts', JSON.stringify(contracts));
    }
  }
  /** update status */
  onUpdateStatus = (id) => {
    var { contracts } = this.state;
    var index = this.findIndex(id);
    if (index !== -1) {
      contracts[index].status = !contracts[index].status;
      this.setState({ contracts: contracts });
      localStorage.setItem('contracts', JSON.stringify(contracts));
    }
  }
  findIndex = (id) => {
    var { contracts } = this.state;
    var result = -1;
    contracts.forEach((contract, index) => {
      if (contract.id === id) {
        result = index
      }
    })
    return result;
  }

  render() {
    var { contractEditing } = this.state;
    return (
      <div className="animated fadeIn">
        <Row >
          <Col xl={12}>
            <Card>
              <CardHeader>
                <h3>Hợp đồng</h3>
              </CardHeader>
              <CardBody>
                <ContractForm onSubmit={this.onSubmit} contractEditing={contractEditing} />
                <ContractList/>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ContractLists;

