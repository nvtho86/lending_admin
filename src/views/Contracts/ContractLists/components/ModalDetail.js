import React, { Component } from 'react';
import { Table, Button, Modal, Col, ModalHeader, Row, ModalBody, ModalFooter, Form, Label, FormGroup } from 'reactstrap';

import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from './../actions/contract.action';


class ModalDetail extends Component {

  onCloseModal = () => {
    this.props.onCloseModal(this.props.modal);
  }
  render() {
    const {contractDetail,modal} = this.props;
    
    return (
      <Modal isOpen={modal} toggle={this.toggleLarge}
          className={'modal-lg ' + this.props.className}>
          <ModalHeader toggle={this.toggleLarge}>Chi tiết hợp đồng</ModalHeader>
          <ModalBody>
            <Col md={12}>
              <FormGroup>
                <legend className="border-bottom">Thông tin hợp đồng</legend>
              </FormGroup>
              <Row>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <Row><Label>Số hợp đồng</Label></Row>
                      <Row><Label>Ngày hợp đồng</Label></Row>
                      <Row><Label>Ngày thanh toán đầu tiên</Label></Row>
                      <Row><Label>Ngày thanh toán cuối cùng</Label></Row>
                    </Col>
                    <Col md={6}>
                      <Row><Label><b>{contractDetail.contractNumber}</b></Label></Row>
                      <Row><Label><b>{contractDetail.contractNumber}</b></Label></Row>
                      <Row><Label><b>{contractDetail.contractNumber}</b></Label></Row>
                      <Row><Label><b>{contractDetail.contractNumber}</b></Label></Row>
                      
                    </Col>
                  </Row>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <Row><Label>Trạng thái cửa hàng</Label></Row>
                      <Row><Label>Tên cửa hàng</Label></Row>
                      <Row><Label>Địa chỉ</Label></Row>
                    </Col>
                    <Col md={6}>
                      <Row><Label><b>{contractDetail.status} </b></Label></Row>
                      <Row><Label>10/06/2010</Label></Row>
                      <Row><Label>HCM</Label></Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <FormGroup>
                <legend className="border-bottom">Thông tin khách hàng</legend>
              </FormGroup>
              <Row>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <Row><Label>Tên khách hàng</Label></Row>
                      <Row><Label>Ngày sinh</Label></Row>
                      <Row><Label>Số điện thoại</Label></Row>
                    </Col>
                    <Col md={6}>
                    <Row><Label><b>{contractDetail.lastName} {contractDetail.midName} {contractDetail.firstName} </b></Label></Row>
                    <Row><Label><b>{contractDetail.birthday} </b></Label></Row>
                    <Row><Label><b>{contractDetail.phoneNumber} </b></Label></Row>
                    </Col>
                  </Row>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <Row><Label>Số CMND</Label></Row>
                      <Row><Label>Ngày cấp</Label></Row>
                    </Col>
                    <Col md={6}>
                      <Row><Label>333333333333</Label></Row>
                      <Row><Label>10/06/2010</Label></Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <FormGroup>
                <legend className="border-bottom">Thông tin khoản vay</legend>
              </FormGroup>
              <Row>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <Row><Label>Sản phẩm vay</Label> </Row>
                      <Row><Label>Tổng giá trị</Label></Row>
                      <Row><Label>Số tiền trả trước</Label></Row>
                      <Row><Label>Số tiền vay</Label></Row>
                      <Row><Label>Loại bảo hiểm</Label></Row>
                    </Col>
                    <Col md={6}>
                      <Row><Label><b>{contractDetail.productName}</b></Label></Row>
                      <Row><Label><b>{contractDetail.productPrice}</b></Label></Row>
                      <Row><Label>20.000.000 vnđ</Label></Row>
                      <Row><Label>20.000.000 vnđ</Label></Row>
                      <Row><Label>OBC</Label></Row>
                    </Col>
                  </Row>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <Row><Label>Khoản thanh toán hàng tháng</Label></Row>
                      <Row><Label>Thời hạng vay</Label></Row>
                      <Row><Label>Số khung</Label></Row>
                      <Row><Label>Số máy</Label></Row>
                      <Row><Label>Số seri/IMEI</Label></Row>
                    </Col>
                    <Col md={6}>
                      <Row> <Label>1.800.000 vnđ</Label></Row>
                      <Row><Label>12 Tháng</Label></Row>
                      <Row><Label>0000001</Label></Row>
                      <Row><Label>0000001</Label></Row>
                      <Row><Label>0000001</Label></Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.onCloseModal}>Quay lại</Button>{' '}
            <Button color="secondary" onClick={this.onCloseModal}>Đóng</Button>
          </ModalFooter>
        </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    modal: state.contracts.modal,
    contractDetail: state.contracts.detail
  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onCloseModal: () => {
      dispatch(actions.closeModal())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalDetail);
