import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from './../actions/contract.action';


class ContractItem extends Component {

  onUpdateStatus = () =>{
    this.props.onUpdateStatus(this.props.contract.id)
  }
  onEdit = (id) =>{
    this.props.onEdit(id)
  }
  onDelete = (id) =>{
    this.props.onDelete(id)
  }
  onDetail = () =>{
    this.props.onDetail(this.props.contract.contractCode)
  }

  render() {

    var { contract, index } = this.props;
    return (
      <tr key={index}>
        <td className="text-center">{index+1}</td>
        <td >{contract.contractCode}</td>
        <td >{contract.fullName}</td>
        <td >{contract.identifyId}</td>
        <td >{contract.phoneNumber}</td>
        <td >{contract.loanAmount}</td>
        <td >{contract.loanType}</td>
        <td className="text-center"> {contract.status}</td>
        <td className="text-center">
          <button type="button" className="btn btn-sm mr-2 btn-warning" onClick={this.onDetail} >Chi tiết</button>
        </td>
      </tr>
    );
  }
}

const mapStateToProps = (state) =>{
  return {}
}
const mapDispatchToProps = (dispatch, props) => {
return {
  onDetail: (id)=>{
    dispatch(actions.atcDetailContractResquest(id))
  }
}
}
export default connect(mapStateToProps,mapDispatchToProps)(ContractItem);
// export default ContractItem;
