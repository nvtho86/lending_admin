import React, { Component } from 'react';
import ContractItem from './ContractItem';
import ModalDetail from './ModalDetail';
import { Table, Button, Modal, Col, ModalHeader, Row, ModalBody, ModalFooter, Form, Label, FormGroup } from 'reactstrap';
import { connect } from 'react-redux';
import removeAccents from 'remove-accents';
import * as actions from './../actions/contract.action';
import apiCaller from './../../../../_helpers/apiCaller';

class ContractList extends Component {

  

  componentWillMount() {
    apiCaller('contract','list', "POST", JSON.stringify({
      "payload": {}
    })).then(res => {
      if(res){
        this.props.contractList(res.data.payload)
      }
      
    })
  }
  render() {
    var { contracts, keyword, isCode, isPhone, isIdentityCard, modal } = this.props;
    if (keyword) {
      contracts = contracts.filter((contract) => {
        if (isCode) {
          return removeAccents(contract.code).toLowerCase().includes(removeAccents(keyword.toString().toLowerCase()));
        } else if (isPhone) {
          return removeAccents(contract.phone).toLowerCase().includes(removeAccents(keyword.toString().toLowerCase()));
        } else if (isIdentityCard) {
          return removeAccents(contract.identityCard).toLowerCase().includes(removeAccents(keyword.toString().toLowerCase()));
        } else {
          return removeAccents(contract.name, contract.code, contract.phone, contract.identityCard).toString().toLowerCase().includes(removeAccents(keyword.toString().toLowerCase()));
        }
      });
    }
    var elmContracts = contracts.map((contract, index) => {
      return <ContractItem
        key={index}
        index={index}
        contract={contract}
        // onUpdateStatus={this.props.onUpdateStatus}
        // onEdit={this.props.onEdit}
        // onDelete={this.props.onDelete}
      />
    })
    return (
      <div>
        <Table className="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th className="text-center" >STT</th>
              <th className="text-center" >Số hợp đồng</th>
              <th className="text-center" >Tên hợp đồng</th>
              <th className="text-center" >CMND</th>
              <th className="text-center" >Số điện thoại</th>
              <th className="text-center" >Khoản vay</th>
              <th className="text-center" >Loại vay</th>
              <th className="text-center" >Trạng thái</th>
              <th className="text-center">Hành động</th>
            </tr>
          </thead>
          <tbody>
            {elmContracts}
          </tbody>
        </Table>
        <ModalDetail/>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    keyword: state.contracts.keyword,
    contracts: state.contracts.contracts,
    isCode: state.contracts.isCode,
    isPhone: state.contracts.isPhone,
    isIdentityCard: state.contracts.isIdentityCard,
    isAll: state.contracts.isAll,
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    contractList:() => {
      dispatch(actions.atcContractListResquest());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContractList);

