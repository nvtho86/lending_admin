import React, { Component } from 'react';
import {CustomInput, Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from './../actions/contract.action';


class ContractForm extends Component {

  constructor(props) {
    super(props);
    this.state={
      keyword:'',
      isCode:false,
      isPhone:false,
      isIdentityCard:false,
      isAll:true
    }
  }
  toggleChangeCode = () => {
    this.setState(prevState => ({
      isCode: !prevState.isCode,
      isIdentityCard:false,
      isPhone:false,
      isAll:false,
    }));
  }

  toggleChangeIdentityCard = () => {
    this.setState(prevState => ({
      isCode: false,
      isIdentityCard: !prevState.isIdentityCard,
      isPhone:false,
      isAll:false,
    }));
  }

  toggleChangePhone = () => {
    this.setState(prevState => ({
      isCode: false,
      isIdentityCard: false,
      isPhone: !prevState.isPhone,
      isAll:false,
    }));
  }
  toggleChangeAll = () => {
    this.setState(prevState => ({
      isAll: !prevState.isAll,
      isCode: false,
      isIdentityCard: false,
      isPhone: false,
    }));
  }



  componentWillMount() {
    /** kiem tra cap nhat */
    if (this.props.contractEditing) {
      this.setState({
        id: this.props.contractEditing.id,
        code: this.props.contractEditing.code,
        name: this.props.contractEditing.name,
        loanAmount: this.props.contractEditing.loanAmount,
        typeOfLoan: this.props.contractEditing.typeOfLoan,
        identityCard: this.props.contractEditing.identityCard,
        phone: this.props.contractEditing.phone,
        address: this.props.contractEditing.address,
        status: this.props.contractEditing.status,
      });
    }
  }
  /** push data to field */
  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.contractEditing) {
      this.setState({
        id: nextProps.contractEditing.id,
        code: nextProps.contractEditing.code,
        name: nextProps.contractEditing.name,
        loanAmount: nextProps.contractEditing.loanAmount,
        typeOfLoan: nextProps.contractEditing.typeOfLoan,
        identityCard: nextProps.contractEditing.identityCard,
        address: nextProps.contractEditing.address,
        phone: nextProps.contractEditing.phone,
        status: nextProps.contractEditing.status,
      });
    }
  }
  onChange = (event) => {

    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === 'code') {
      // value = target.value === 'true' ? true : false;
    }
    this.setState({
      [name]: value
    });

  }
  onReset = (event) => {
    event.preventDefault();
    this.props.onReset(this.state)
    this.setState({
      keyword:'',
      isCode: false,
      isIdentityCard: false,
      isPhone:false,
      isAll:true,
    })
  }
  onSearch =(e) =>{
    e.preventDefault();
    this.props.onSearch(this.state)
  }
  render() {
    var { keyword,optionSearch } = this.props;
    return (
      <Form>
          {/* <legend className="border-bottom"><Label>Tìm kiếm</Label></legend> */}
        <FormGroup>
          <Col sm={{ size: 12, offset: 3 }}>
            <FormGroup check inline>
              <Label check>Tìm theo</Label>
            </FormGroup>
            <FormGroup check inline>
              <CustomInput type="radio"
              checked={this.state.isCode}
              value='code'
              id="isCode"
              name="code" onChange={this.toggleChangeCode}  label="Số hợp đồng" />
            </FormGroup>
            <FormGroup check inline>
              <CustomInput type="radio"
               name="code"
               value="IdentityCard"
               id="isIdentityCard"
               checked={this.state.isIdentityCard}
              onChange={this.toggleChangeIdentityCard} label="CMND"  /> 
            </FormGroup>
            <FormGroup check inline>
              <CustomInput type="radio"
              name="code"
              id="isPhone"
              checked={this.state.isPhone}
              onChange={this.toggleChangePhone} label="Số điện thoại" />
            </FormGroup>
            <FormGroup check inline>
              <CustomInput type="radio"
                name="code"
                id="isAll"
                value="phone"
                checked={this.state.isAll}
                onChange={this.toggleChangeAll} label="Tất cả"  /> 
            </FormGroup>
          </Col>
        </FormGroup>
        <FormGroup >

          <Row form>
            <Col md={1} sm={{ size: 12, offset: 2 }}>
              <FormGroup check inline>
                <Label check>Từ khóa </Label>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Input type="text"
                name="keyword"
                onChange={this.onChange}
                value={this.state.keyword}
                 />
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup check inline>
                <Button type="submit" onClick={this.onSearch} color="success" className="mr-2">Tìm kiếm</Button>
                <Button type="reset" onClick={this.onReset}  color="info">Reset</Button>
              </FormGroup>

            </Col>
          </Row>
        </FormGroup>
      </Form>

      // <form onSubmit={this.onSubmit}>
      //         <div className="form-group">
      //           <legend>{id!==''?'Cập nhật':'Thêm hợp đồng mới'}</legend>
      //         </div>
      //         <div className="row col-md-12 form-group">
      //           <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      //             <label className="col-sm-12 control-label">Tên hợp đồng:</label>
      //             <div className="col-sm-12">
      //               <input type="text"
      //                 name="name"
      //                 value={this.state.name}
      //                 onChange={this.onChange}
      //                 id="inputname"
      //                 className="form-control"
      //                 placeholder="Tên hợp đồng" />
      //             </div>
      //           </div>
      //           <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      //             <label className="col-sm-12 control-label">Địa chỉ:</label>
      //             <div className="col-sm-12">
      //               <input type="text"
      //                 name="address"
      //                 value={this.state.address}
      //                 onChange={this.onChange}
      //                 id="inputname"
      //                 className="form-control"
      //                 placeholder="Địa chỉ" />
      //             </div>
      //           </div>
      //           <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      //             <label className="col-sm-12 control-label">Trạng thái:</label>
      //             <div className="col-sm-12">
      //               <select name="status"
      //                 value={this.state.status}
      //                 onChange={this.onChange}
      //                 id="inputstatus" className="form-control">

      //                 <option value={true} >Kích hoạt</option>
      //                 <option value={false}>Ẩn</option>
      //               </select>
      //             </div>
      //           </div>
      //         </div>

      //         <div className="form-group">
      //           <div className="col-sm-12 col-sm-offset-2 text-right">
      //             <button type="submit" className="btn btn-primary mr-2">{id!==''?'Cập nhật':'Thêm mới'}</button>
      //             <button type="submit" className="btn btn-secondary mr-2">Hủy</button>
      //             {/* <button type="button" className="btn  btn-danger" onClick={this.onGeneratecontract}>Generate data</button> */}
      //           </div>
      //         </div>
      //       </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    keyword:state.contracts.keyword,
    // code:state.contracts.optionSearch.code,
    // phone:state.contracts.optionSearch.phone,
    // identityCard:state.contracts.optionSearch.identityCard,
    // all:state.contracts.optionSearch.all,

  };
}
const mapDispatchToProps = (dispatch, props) => {
  return {

    onSearch: (keyword)=>{
      dispatch(actions.searchContract(keyword))
    },
    onReset: ()=>{
      dispatch(actions.resetTable())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContractForm);
