import * as types from './../constants/ActionTypes';
// import apiCaller from './../utils/apiCaller';
import apiCaller from './../../../_helpers/apiCaller'

export const getListNews = (param) => {
    return (dispatch) => {
        return apiCaller('news', 'list', 'POST',
            JSON.stringify(
                {
                    payload: param
                }
            )
        ).then(response => {
            if (response && response.data)
                dispatch(fetchListNews({list: response.data.payload.list, total: response.data.payload.total}))
        })
    };
}

export const onUpdateNews = (param) => {
    return apiCaller('news', 'update', 'POST',
        JSON.stringify(
            {
                payload: param
            }
        )
    ).then(response => {
        if (response && response.data) 
            return response.data.code == 200
    })
}

export const onCreateNews = (param) => {
    return apiCaller('news', 'post', 'POST',
        JSON.stringify(
            {
                payload: param
            }
        )
    ).then(response => {
        console.log(response)
        if (response && response.data) 
            return response.data.code == 200
    })
}

export const onUploadImage = (data) => {
    return apiCaller('news', 'upload_file', 'POST',
        JSON.stringify(
            {
                payload: {
                    files: [
                        {
                            data: data
                        }
                    ]
                }
            }
        )
    ).then(response => {
        if (response && response.data)
        {
            const { data } = response
            if (data.code == 200)
                return data.payload.files[0].data
        }
        return []
    })
}

export const getDetailNews = (id) => {
    return apiCaller('news', 'detail', 'POST',
        JSON.stringify(
            {
                payload: {
                    id: id
                }
            }
        )
    ).then(response => {
        if (response && response.data)
            return response.data.payload
        return {}
    })
}

export const deleteNews = (id) => {
    return apiCaller('news', 'delete', 'POST',
        JSON.stringify(
            {
                payload: {
                    id: id
                }
            }
        )
    ).then(response => {
        if (response && response.data)
            return response.data.code == 200
    })
}

export const fetchListNews = (data) => {
    return {
      type: types.LIST_ALL_NEWS,
      data
    }
}

export const changePagination = (pageNum) => {
    return {
      type: types.CHANGE_PAGINATION,
      pageNum
    }
}