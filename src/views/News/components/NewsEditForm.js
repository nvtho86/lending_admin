import React, { Component } from 'react'
import {
	Row, Col, Button, Card, CardHeader, CardBody, CardFooter,
	Input, FormGroup, Label, CardImg, CustomInput
} from 'reactstrap'
import "react-datepicker/dist/react-datepicker.css"
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { EditorState, convertToRaw, ContentState, convertFromHTML } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import { connect } from 'react-redux'
import DatePicker from 'react-datepicker'
import * as actions from '../actions/news.action'
import moment from "moment"

class NewsEditForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			dataEdit: {
				access: 1,
                content: "",
                contentBrief: "",
                createdAt: "2019-08-22T07:16:43.053+0000",
                createdBy: "45510fe8-db31-4956-8397-acd7b6085af9",
                endDate: "2019-09-12T06:23:38.000+0000",
                id: "",
                imagePath: "",
                isFeatured: 1,
                modifiedAt: "2019-09-05T08:09:39.518+0000",
                modifiedBy: "e2126ff6-4ac5-4a34-b743-9fa3769eb775",
                startDate: "2019-09-06T06:23:38.000+0000",
                status: 1,
                statusNotification: 0,
                title: "",
                type: 1,
                filterCustomers: []
            },
            file: '',
			offerFor: '0',
			
            editorState: EditorState.createEmpty(),
            
            timeNow: moment().format('HH:mm'),
            tempNamePicker: '',
            timeModal: ''
		}
	}

    async componentDidMount () {
        const listPath = this.props.location.pathname.split('/')
        const id = listPath[listPath.length - 1]
        await actions.getDetailNews(id).then(data => {
            this.setState({
                dataEdit: data,
                offerFor: data.filterCustomers && data.filterCustomers.length > 0 ? '1' : '0',
                editorState: data.content ?
						EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML('<p>' + data.content + '</p>'))) :
						EditorState.createEmpty()
            })
        })
    }

	// Add to list of filterList
	addFilterCustomer = () => {
        let { filterCustomers } = this.state.dataEdit
        const { customerFilterCategory } = this.props.defaultData
        if (filterCustomers.length == customerFilterCategory.length)
            return
        let listFilter = filterCustomers.map(item => {
            return item.key
        })
        let list =  customerFilterCategory.filter(item => {
            return !listFilter.includes(item.key)
        })
        if (list.length > 0) {
            filterCustomers.push({
                key: list[0].key,
                compare: '=',
                value: '',
                id: 0
            })
            this.setState(prevState => ({
                dataEdit: {
                    ...prevState.dataEdit,
                    filterCustomers: filterCustomers
                }
            }))
        }
	}

	// Detete one of list of filterList
	deleteFilterCustomer = index => () => {
        var dataTemp = this.state.dataEdit.filterCustomers
        dataTemp.splice(index, 1)
		
		this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                filterCustomers: dataTemp
            }
		}))
	}

	onEditorChange = (editorState) => {
		const content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
		this.setState(prevState => ({
			editorState,
			dataEdit: {
				...prevState.dataEdit,
				content: content
			}
		}))
    }

	renderEditor = () => {
		return { __html: '<span>' + this.state.dataEdit.content + '</span>' };
	}
    
    handleChange = e => {
        const { value, name } = e.target
        this.setState({
            [name]: value
        })
    }

    handleChangeDataEdit = e => {
        const { value, name, checked, type } = e.target
        this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                [name]: type != 'checkbox' ? value : (checked ? 1 : 0)
            }
        }))
    }
    
    handleChangeStartDate = date => {
        this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                startDate: date
            }
        }))
    }

    handleChangeEndDate = date => {
        this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                endDate: date
            }
        }))
    }

	onChangeImage = (event) => {
		if (event && event.target.files[0]) {
			var reader = new FileReader();
			var file = event.target.files[0];
			
			reader.onload = upload => {
				this.setState({
                    file: upload.target.result
				}, () => console.log(this.state.file))
			};
			reader.readAsDataURL(file);
		}
	}

    onUpdateNews = async () => {
        const { dataEdit, file } = this.state
        let imagePath = ''
        if (file) {
            await actions.onUploadImage(file).then(async res => {
                if (res) {
                    imagePath = res
                }
            })
        }
        const filters = dataEdit.filterCustomers
        const param = {
            filters: filters,
            news: {
                ...dataEdit,
                imagePath: imagePath
            }
        }

        delete param.news.filterCustomers

        await actions.onUpdateNews(param).then(res => {
            if (res) {
                this.props.history.push("/news")
            } 
        })
    }

    handleChangeFilterDataEdit = key => e => {
        const { value, name } = e.target
        let { filterCustomers } = this.state.dataEdit
        filterCustomers.forEach(item => {
            if (key == item.key) {
                item[name] = value
            }
        })
        this.setState(prevState => ({
            dataEdit: {
                ...prevState.dataEdit,
                filterCustomers: filterCustomers
            }
        }))
    }

    isDisableFilter = key => {
        const  { filterCustomers } = this.state.dataEdit
        const isExist = function(filter) {
            return filter.key = key
        }
        return false
    }

	render() {
        const { editorState, dataEdit, offerFor } = this.state
        const { customerFilterCategory, compareType } = this.props.defaultData

        const startDate = dataEdit.startDate ? new Date(dataEdit.startDate) : new Date()
        const endDate = dataEdit.endDate ? new Date(dataEdit.endDate) : new Date()

		return (
            <Card className="animated fadeIn">
                <CardHeader><h4>Chỉnh sửa tin tức</h4></CardHeader>
                <CardBody>
                    <Row>
                        <Col md={8}>
                            <FormGroup>
                                <Row>
                                    <Label md={3}>Title</Label>
                                    <Col md={9}>
                                        <Input defaultValue={dataEdit.title} type='text' name='title'
                                            onChange={this.handleChangeDataEdit} />
                                    </Col>
                                </Row>
                            </FormGroup>

                            <FormGroup>
                                <Row>
                                    <Label md={3}>Nội dung ngắn gọn</Label>
                                    <Col md={9}>
                                        <Input type='text' name='contentBrief' value={dataEdit.contentBrief}
                                            onChange={this.handleChangeDataEdit}  />
                                    </Col>
                                </Row>
                            </FormGroup>

                            <FormGroup>
                                <Row>
                                    <Label md={3}>Nội dung đầy đủ</Label>
                                    <Col md={9}>
                                        <Editor
                                            editorState={editorState}
                                            onEditorStateChange={this.onEditorChange}
                                            toolbar={{
                                                options: ['inline', 'fontSize', 'fontFamily', 'list',
                                                'textAlign', 'colorPicker', 'link', 'image'],
                                                link: {
                                                defaultTargetOption: '_blank',
                                                popupClassName: "mail-editor-link"
                                            },
                                            image: {
                                                urlEnabled: true,
                                                uploadEnabled: true,
                                                uploadCallback: this.uploadImageCallBack,
                                                alignmentEnabled: true,
                                                defaultSize: {
                                                height: 'auto',
                                                width: 'auto',
                                                },
                                                inputAccept: 'application/pdf,text/plain,application/vnd.openxmlformatsofficedocument.wordprocessingml.document,application/msword,application/vnd.ms-excel'
                                                ,
                                                colorPicker: {
                                                className: undefined,
                                                component: undefined,
                                                popupClassName: undefined,
                                                colors: ['rgb(97,189,109)', 'rgb(26,188,156)', 'rgb(84,172,210)', 'rgb(44,130,201)',
                                                'rgb(147,101,184)', 'rgb(71,85,119)', 'rgb(204,204,204)', 'rgb(65,168,95)', 'rgb(0,168,133)',
                                                'rgb(61,142,185)', 'rgb(41,105,176)', 'rgb(85,57,130)', 'rgb(40,50,78)', 'rgb(0,0,0)',
                                                'rgb(247,218,100)', 'rgb(251,160,38)', 'rgb(235,107,86)', 'rgb(226,80,65)', 'rgb(163,143,132)',
                                                'rgb(239,239,239)', 'rgb(255,255,255)', 'rgb(250,197,28)', 'rgb(243,121,52)', 'rgb(209,72,65)',
                                                'rgb(184,49,47)', 'rgb(124,112,107)', 'rgb(209,213,216)']
                                                }
                                                }
                                            }}
                                            editorClassName="form-control editorClassName"
                                        />
                                    </Col>
                                </Row>
                            </FormGroup>

                            <FormGroup>
                                <Row>
                                    <Label md={3}>Loại tin tức</Label>
                                    <Col md={3}>
                                        <Input name="type" onChange={this.handleChangeDataEdit} value={dataEdit.type || 0} type="select">
                                            <option value="0">Khuyến mãi</option>
                                            <option value="1">T.tin HD Saigon</option>
                                            <option value="2">Thông tin chung</option>  
                                        </Input>
                                    </Col>
                                    <Label md={3}>Hình ảnh</Label>
                                    <Col md={3}>
                                        <label className="file-button" htmlFor="file">
                                            Chọn hình
                                            <Input hidden name="file" id="file" onChange={this.onChangeImage} type="file" accept="image/*" />
                                        </label>
                                    </Col>
                                </Row>
                            </FormGroup>

                            <FormGroup>
                                <Row>
                                    <Label md={3}>Ngày bắt đầu hiển thị</Label>
                                    <Col md={3} style={{ 'marginLeft': '0px' }} >
                                        <DatePicker
                                            className="form-control"
                                            name="startDate"
                                            selected={startDate}
                                            onChange={this.handleChangeStartDate}
                                            showYearDropdown
                                            dateFormatCalendar="MMMM"
                                            scrollableYearDropdown
                                            yearDropdownItemNumber={15}
                                            dateFormat="dd/MM/yyyy"
                                        />
                                    </Col>
                                    <Label md={3}>Ngày kết thúc hiển thị</Label>
                                    <Col md={3}>
                                        <DatePicker
                                            className="form-control"
                                            name="endDate"
                                            selected={endDate}
                                            onChange={this.handleChangeEndDate}
                                            showYearDropdown
                                            dateFormatCalendar="MMMM"
                                            scrollableYearDropdown
                                            yearDropdownItemNumber={15}
                                            dateFormat="dd/MM/yyyy"
                                        />
                                    </Col>
                                </Row>
                            </FormGroup>

                            <FormGroup row>
                                <Label md={3}>Tùy chọn</Label>
                                <Col md={3} className="d-flex align-items-center">
                                    <CustomInput type="checkbox" id="isFeatured" checked={dataEdit.isFeatured == 1 ? true : false} value={dataEdit.isFeatured == 1 ? true : false} name="isFeatured" onChange={this.handleChangeDataEdit} label="Tin nổi bật" />
                                </Col>
                                
                                <Col md={3} className="d-flex align-items-center">
                                    <CustomInput type="checkbox" id="statusNotification" checked={dataEdit.statusNotification == 1 ? true : false} value={dataEdit.statusNotification == 1 ? true : false} name="statusNotification" onChange={this.handleChangeDataEdit} label="Gửi thông báo" />
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label md={3}>Tin tức dành cho</Label>
                                <Col md={3} className="d-flex align-items-center">
                                    <CustomInput type="radio" value="0" id="0" name="offerFor" checked={offerFor == '0'} onChange={this.handleChange} label="Toàn bộ" />
                                </Col>
                                <Col md={3} className="d-flex align-items-center">
                                    <CustomInput type="radio" value="1" id="1" name="offerFor" checked={offerFor == '1'} onChange={this.handleChange} label="Chỉ 1 nhóm người" />
                                </Col>
                            </FormGroup>
                            {   
                                offerFor == '1' &&
                                <FormGroup className="animated fadeIn">
                                    <Row className="mb-2">
                                        <Label className="mb-2" md={12}>Lọc danh sách tài khoản nhận tin tức</Label>
                                        <Label md={{size: 3, offset: 3}}>
                                            Điều kiện
                                        </Label>
                                        <Label md={3}>
                                            So sánh
                                        </Label>
                                        <Label md={3}>
                                            Giá trị
                                        </Label>
                                    </Row>
                                    {
                                        dataEdit.filterCustomers.map((itemFilter, index) => (
                                        <FormGroup row key={index} >
                                            <Col className="d-flex align-items-center justify-content-end" md={3}>
                                                <Button className="btn-filter" onClick={this.deleteFilterCustomer(index)} ><i className="fa fa-minus"/></Button>
                                            </Col>
                                            <Col md={3}>
                                                <Input type="select" name="key" value={itemFilter.key} onChange={this.handleChangeFilterDataEdit(itemFilter.key)}>
                                                    {
                                                        customerFilterCategory.map((fillterName, index) => {
                                                            return (
                                                                <option key={index} value={fillterName.key}>{ fillterName.label }</option>
                                                            )
                                                        })
                                                    }
                                                </Input>
                                            </Col>
                                            <Col md={3}>
                                                <Input type="select" name="compare" value={itemFilter.compare} onChange={this.handleChangeFilterDataEdit(itemFilter.key)}>
                                                    {
                                                        compareType.map((compareValue) => {
                                                            return (
                                                                <option key={compareValue.id} value={compareValue.name}>{ compareValue.name }</option>
                                                            )
                                                        })
                                                    }
                                                </Input>
                                            </Col>
                                            <Col md={3}>
                                                <Input type="text" name="value" value={ itemFilter.value }
                                                onChange={this.handleChangeFilterDataEdit(itemFilter.key)}  />
                                            </Col>
                                        </FormGroup>
                                    ))}
                                    {   
                                        dataEdit.filterCustomers.length != customerFilterCategory.length &&
                                        <Row>
                                            <Col className="d-flex align-items-center justify-content-end" md={3}>
                                                <Button className="btn-filter" onClick={this.addFilterCustomer} ><i className="fa fa-plus"/></Button>
                                            </Col>
                                        </Row>
                                    }
                                </FormGroup>
                            }
                        </Col>

                        <Col md={4}>
                            <FormGroup className="shadowBox">
                                <h4 className="text-center">
                                    <Label className="font-weight-bold">Xem nội dung trước</Label>
                                </h4>
                                <Row className="text-center">
                                    <Col className="ml-5 mr-5">
                                        { 
                                            this.state.file && 
                                            <CardImg src={this.state.file} id="img_upload_id" width="100%" height="200" style={{  borderRadius: '20px', objectFit: "contain", padding: "10px" }} />
                                        }
                                    </Col>
                                </Row>
                                <Label className="text-center" style={{ 'wordWrap': 'normal', fontSize: 'large' }}>
                                    <h5> {dataEdit.title} </h5>
                                </Label>
                                
                                <div dangerouslySetInnerHTML={this.renderEditor()}
                                style={{
                                    margin: 'auto', maxWidth: '325px',
                                    height: '380px', 'overflowX': 'hidden', 'overflowY': 'hidden'
                                }}
                                >
                                </div>
                            </FormGroup>
                        </Col>
                    </Row>
                </CardBody>
                <CardFooter className="btn-footer">
                    <Button color="primary" onClick={() => this.props.history.goBack()} name="handle">Quay lại</Button>
                    <Button color="danger" onClick={this.onUpdateNews}>Lưu</Button>{' '}
                </CardFooter>
            </Card>
        )
	}
}

const mapStateToProps = (state) => {
	return {
        defaultData: state.defaultData
	}
}
const mapDispatchToProps = (dispatch, props) => {
	return {
        
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsEditForm)