import React, { Component } from 'react';
import {
  Row, Label, Button, Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class NewsDeleteForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render () {
		const { isOpenModalDelete, propsDelete }  = this.props
		return (
            <Modal isOpen={isOpenModalDelete}>
                <ModalHeader>Xác nhận xóa tin tức</ModalHeader>
                <ModalBody>
                    <Row>
                        <Label sm={3}>Tiêu đề</Label>
                        <Label sm={9}>{propsDelete.title}</Label>
                        <Label sm={3}>Nội dung</Label>
                        <Label sm={9}>{propsDelete.contentBrief}</Label>
                    </Row>
                    <Row>
                        <Label sm={3}></Label>
                        <Label sm={9} style={{ fontWeight: 'bold', color: 'red', fontSize: '14px' }}>Xác nhận xóa tin tức ?</Label>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => this.props.onDeleteNews()}>Xóa</Button>
                    <Button color="secondary" onClick={() => this.props.toggleDelete(false)}>Hủy bỏ</Button>{' '}
                </ModalFooter>
            </Modal>
		)
	}
}

export default NewsDeleteForm;