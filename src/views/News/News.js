import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Button, Table,
  Input, CardFooter, Pagination, PaginationItem, PaginationLink, FormGroup } from 'reactstrap'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import "react-datepicker/dist/react-datepicker.css"
import "./../style/vinh_style.css"
import { EditorState } from 'draft-js'
import { Link } from 'react-router-dom'
// import './../PreferentialInformation/styles/PreferentialInformation.scss'
import { connect } from 'react-redux'
import * as actions from './actions/news.action'
import ModalDelete from './components/NewsDeleteForm'

class News extends Component {
  constructor(props) {
    super(props)

    this.state = {
      listData: [],
      imagePreviewUrl: '',
      modalDelete: false,

      /* editor draft */ 
      editorHTML: '',
      editorState: EditorState.createEmpty(),

      listPagination: [1, 2, 3, 4, 5],
      keyWordSearch: '',
      typeSearch: 0,
      propsDelete: {
        title: '',
        contentBrief: ''
      },
      isOpenModalDelete: false,
      pageSize: 5
    }
  }

  componentDidMount () {
    // const newsList = this.props.newsData.newsList
    // if (newsList.length == 0)
    // {
      // const listPath = this.props.location.pathname.split('/')
      // const statePath = listPath[listPath.length - 1]
      // const pageNum = statePath != 'news' && statePath != '1' ? statePath : 1
      this.props.changePagination(1)
      this.onInit(1)
    // }
    // else
    //   this.setState({ listData: newsList })
    
  }

  componentWillReceiveProps(nextProps) {
    const { pageNum, total } = nextProps.newsData
    const { pageSize } = this.state
    if (this.props.newsData.pageNum != pageNum)
      this.onInit(pageNum)
    else {
      if (nextProps.newsData && nextProps.newsData.newsList)
        this.setState({ listData: nextProps.newsData.newsList })
    }
    const totalPage = Math.round(total / pageSize)
    let list = new Set
    for (let i = 1; i <= totalPage; i++)
      list.add(i)
    this.setState({
      listPagination:list
    })
  }

  onInit(pageNum = 1, keyWordSearch = '', type = 0) {
    const data = {
      direction: "",
      keyWord: keyWordSearch,
      oderBy: "",
      pageNum: pageNum,
      pageSize: this.state.pageSize,
      type: type
    }
    this.props.onLoadNews(data)
  }

  onHandlechange = e => {
    const { value, name } = e.target
    this.setState({
      [name]: value
    })
  }

  /* modal delete toggle */
  toggleDelete = (state) => {
    this.setState({
      isOpenModalDelete: state
    });
  }

  /* delete news */
  deleteNews = (data) => () => {
    this.setState({
      propsDelete: data
    }, () => {
      this.toggleDelete(true)
    })
  }

  submitDeleteNews = async () => {
    await actions.deleteNews(this.state.propsDelete.id).then(res => {
      if (res) {
        this.onInit(1)
        this.toggleDelete(false)
      }
    })
  }

  onChangePagination = pageNum => () => {
    if (pageNum > 0 && pageNum <= this.state.listPagination.size) {
      // this.props.history.push(`/management/news/${pageNum}`)
      this.props.changePagination(pageNum)
    }
  }

  onSearchNews = () => {
    const { pageNum }= this.props.newsData
    const { keyWordSearch, typeSearch } = this.state
    this.onInit(pageNum, keyWordSearch, typeSearch)
  }

  render() {    
    const { listData, keyWordSearch, isOpenModalDelete, propsDelete } = this.state
    const renderRow = listData.map((data, index) => {
      const link = `/news/edit/${data.id}`
      return (
          <tr key={index}>
            <td className="text-center">{index + 1}</td>
            <td>{data.title}</td>
            <td dangerouslySetInnerHTML={{ __html: data.content }} />
            <td className="text-center"><img src="https://img.lemde.fr/2019/04/22/0/191/1619/1079/688/0/60/0/e39da8d_2FIads9h8wB-0SwSgxVaVWsp.jpg" alt="Smiley face" width="30" height="30" /></td>
            <td className="text-center">{data.type}</td>
            <td className="text-center">
              <Link color="warning" className="btn-edit" to={link}><i className="fa fa-edit"></i></Link>
            </td>
            <td className="text-center">
              <Button color="danger" onClick={this.deleteNews(data)}><i className="fa fa-trash-o"></i></Button>
            </td>
          </tr>
        )
      })
    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>
            <h3>Tin tức</h3>
          </CardHeader>
          <CardBody className="pb-0">
            <FormGroup className="form-search">
              <div className="type-search">
                <label>Từ khóa:</label>
                <Input value={keyWordSearch} onChange={this.onHandlechange} type="text" name="keyWordSearch"></Input>
              </div>
              <div className="type-search input-search">
                <Input name="typeSearch" onChange={this.onHandlechange} type="select">
                  <option value="0">Tất cả</option>
                  <option value="1">Thông tin HDSaiGon</option>
                  <option value="2">Thông tin chung</option>
                </Input>
              </div>
              <div className="btn-link">
                <Button color="success" onClick={this.onSearchNews}>Tìm kiếm</Button>
                <Link className="btn-create" to="/news/create">Thêm mới</Link>
              </div>
            </FormGroup>
            <Table className="table-outline" bordered hover striped variant="dark">
              <thead>
                <tr className="text-center">
                  <th style={{ width: "5%" }}>STT</th>
                  <th style={{ width: "30%" }}>Tiêu đề</th>
                  <th style={{ width: "35%" }}>Nội dung</th>
                  <th style={{ width: "10%" }}>Hình ảnh</th>
                  <th style={{ width: "10%" }}>Loại</th>
                  <th style={{ width: "5%" }}>Sửa</th>
                  <th style={{ width: "5%" }}>Xóa</th>
                </tr>
              </thead>
              <tbody>
                { renderRow }
              </tbody>
            </Table>
          </CardBody>
          <CardFooter className="pb-0">
            <Pagination className="pagination-news" aria-label="Page navigation example">
              <PaginationItem>
                <PaginationLink previous onClick={this.onChangePagination(this.props.newsData.pageNum - 1)} />
              </PaginationItem>
              {
                this.state.listPagination.map((e, i) => {
                  return (
                    <PaginationItem key={i} disabled={this.props.newsData.pageNum == e}>
                      <PaginationLink onClick={this.onChangePagination(e)}>{e}</PaginationLink>
                    </PaginationItem>
                  )
                })
              }
              <PaginationItem>
                <PaginationLink next onClick={this.onChangePagination(this.props.newsData.pageNum + 1)} />
              </PaginationItem>
            </Pagination>
          </CardFooter>
        </Card>

        <ModalDelete
          isOpenModalDelete={isOpenModalDelete}
          propsDelete={propsDelete}
          toggleDelete={this.toggleDelete}
          onDeleteNews={this.submitDeleteNews}
        >
        </ModalDelete>
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    newsData: state.newsData
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onLoadNews: (param) => {
      dispatch(actions.getListNews(param))
    },

    changePagination: pageNum => {
      dispatch(actions.changePagination(pageNum))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(News)