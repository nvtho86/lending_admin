import * as types from './../constants/ActionTypes';

const initialState = {
  newsList: [],
  pageNum: 1,
  total: 0
};

var newsData = (state = initialState, action) => {
  switch (action.type) {
    case types.LIST_ALL_NEWS:
      return {
        ...state,
        newsList: action.data.list,
        total: action.data.total
      }
    case types.CHANGE_PAGINATION:
      return {
        ...state,
        pageNum: action.pageNum
      }
    default:
      return state;
  }
}
export default newsData;
