import axios from "axios";
import * as Config from "./../constants/Config";

export default function callApi(endpoint, method = "GET", body) {
  let getToken = null;
  if (localStorage && localStorage.getItem("token")) {
    getToken = localStorage.getItem("token");
  }
  return axios({
    headers: {
      "Content-Type": "application/json",
      "x-api-key": "aa86719bb53d3a8fc470210d7e7a1b4388da4fa2",
      Authorization: "Bearer " + getToken,
      "x-environment": "WEB-ADMIN"
    },
    method: method,
    url: `${Config.API_URL}/${endpoint}`,
    data: body
  }).catch(error => {
    console.log(error);
  });
}
