import React, { Component } from 'react';

class Logout extends Component {
  
  
  render() {
    return localStorage.clear()+this.props.history.push("/login");
  }
}

export default Logout;
