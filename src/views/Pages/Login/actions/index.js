import axios from "axios";
import apiCaller from './../../../../_helpers/apiCaller';

export const SET_LOGIN_PENDING = "SET_LOGIN_PENDING";
export const SET_LOGIN_SUCCESS = "SET_LOGIN_SUCCESS";
export const SET_LOGIN_ERROR = "SET_LOGIN_ERROR";

const setLoginPending = isLoginPending => {
  return {
    type: SET_LOGIN_PENDING,
    isLoginPending
  };
};

const setLoginSuccess = (isLoginSuccess, token) => {
  return {
    type: SET_LOGIN_SUCCESS,
    isLoginSuccess,
    token
  };
};

const setLoginError = loginError => {
  return {
    type: SET_LOGIN_ERROR,
    loginError
  };
};

export const login = (local, depart, email, password) => {
  return dispatch => {
    dispatch(setLoginPending(true));
    dispatch(setLoginSuccess(false));
    dispatch(setLoginError(null));

    // POST request
    return apiCaller('staff','signin', "POST", JSON.stringify({
      "payload": {
        "email": email,
        "password": password,
        "ou": ["HQ", local, depart]
      }
    }))
    .then(response => {
        // console.log("token", response.data.payload.token);
        // console.log("idUser", response.data.payload.staff.id);
        // console.log("name", response.data.payload.staff.fullName);
        localStorage.setItem("token", response.data.payload.token);
        localStorage.setItem("idUser", response.data.payload.staff.id);
        localStorage.setItem("name", response.data.payload.staff.fullName);
        dispatch(setLoginSuccess(true, response.data.payload.token));
      })
      .catch(error => {
        dispatch(setLoginError(true));
      });
  };
};
