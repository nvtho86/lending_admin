import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import logo from '../../../../assets/img/logo.png';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  Row,
  FormGroup,
  Label,
  // InputGroupAddon,
  InputGroup
} from "reactstrap";
import { connect } from "react-redux";
import { login } from "../actions";
import "../style/index.css";
import ReeValidate from "ree-validate";

class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.validator = new ReeValidate({
      email: "required|email",
      password: "required|min:1"
    });

    this.state = {
      formData: {
        local: "HCMC",
        depart: "IT",
        email: "",
        password: ""
      },
      stateDiff: null,
      errors: this.validator.errors
    };
  }

  handleChange = e => {
    let { name, value } = e.target;
    this.setState({ formData: { ...this.state.formData, [name]: value } });
    if (document.getElementById("location").value === "SIP") {
      this.setState({
        stateDiff: true
      });
    } else {
      this.setState({
        stateDiff: false
      });
    }
  };

  handleChange_1 = e => {
    let { name, value } = e.target;
    const { errors } = this.validator;
    errors.remove(name);
    this.setState({ formData: { ...this.state.formData, [name]: value } });
  };

  validateAndSubmit = e => {
    e.preventDefault();
    const { formData } = this.state;
    const { errors } = this.validator;
    this.validator.validateAll(formData).then(success => {
      if (success) {
        this.props.fetchLogin(
          this.state.formData.local,
          this.state.formData.depart,
          this.state.formData.email,
          this.state.formData.password
        );
      } else {
        this.setState({ errors });
      }
    });
  };

  render() {
    const { isLoginState } = this.props;

    if (isLoginState.isLoginSuccess && localStorage.getItem("token")) {
      return <Redirect to="/" />;
    }

    const { errors } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="7">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <div className="login_head">
                      <div className="circle-logo d-flex justify-content-center align-items-center">
                        <img src={logo} alt="HD Sài gòn"/>
                      </div>
                      <div className="login_title">
                        <h3>HDSaison Admin Portal</h3>
                        <h6 className="note_title">
                          Đăng nhập bằng tài khoản email của HDSaison
                        </h6>
                      </div>
                    </div>
                    <Form className="form-login">
                      <FormGroup row className="ml-1">
                        <Input
                          type="select"
                          bsSize="lg"
                          id="location"
                          name="local"
                          onChange={this.handleChange}
                          ref="myInput"
                          defaultValue="local"
                        >
                          {" "}
                          <option disabled value="local">
                            Khu vực
                          </option>
                          <option value="HCMC">TpHCM</option>
                          <option value="HaNoi">Hà Nội</option>
                          <option value="SIP">Khác</option>
                        </Input>
                      </FormGroup>
                      <FormGroup row className="ml-1">
                        {/* <Label sm={2} size="lg" className="label_color">
                          Phòng ban
                        </Label> */}
                        <Input
                          type="select"
                          bsSize="lg"
                          id="department"
                          name="depart"
                          onChange={this.handleChange}
                          defaultValue="room"
                        >
                          <option disabled value="room">
                            Phòng ban
                          </option>

                          {!this.state.stateDiff && (
                            <option value="IT">IT</option>
                          )}
                          {!this.state.stateDiff && (
                            <option value="Sales">Kinh doanh</option>
                          )}
                          {!this.state.stateDiff && (
                            <option value="Finance">Dự án/Tài chính</option>
                          )}
                          {this.state.stateDiff && (
                            <option value="POS-Mail">
                              Nhân viên ngoài thị trường
                            </option>
                          )}
                        </Input>
                      </FormGroup>
                      <FormGroup row className="ml-1">
                        <InputGroup>
                          {/* <InputGroupAddon sm={2} addonType="prepend">
                            @
                          </InputGroupAddon> */}
                          <Input
                            placeholder="Email"
                            type="email"
                            name="email"
                            bsSize="lg"
                            onBlur={this.handleChange_1}
                            value={this.state.email}
                          />
                        </InputGroup>
                        {errors.has("email") && (
                          <Label row style={{ color: "red", marginTop: "1%" }}>
                            (*) Email không phù hợp
                          </Label>
                        )}
                      </FormGroup>
                      <FormGroup row className="ml-1">
                        <InputGroup>
                          {/* <InputGroupAddon sm={2} addonType="prepend">
                            @
                          </InputGroupAddon> */}
                          <Input
                            sm={10}
                            placeholder="Mật khẩu"
                            type="password"
                            name="password"
                            bsSize="lg"
                            onBlur={this.handleChange_1}
                            value={this.state.password}
                          />
                        </InputGroup>
                        {errors.has("password") && (
                          <Label row style={{ color: "red", marginTop: "1%" }}>
                            (*) Mật khẩu không được để trống
                          </Label>
                        )}
                      </FormGroup>
                      <FormGroup>
                        <Row>
                          <Col xs="12" className="text-center">
                            <Button
                              className="btn_style px-4 ml-1"
                              bsSize="lg"
                              onClick={this.validateAndSubmit}
                            >
                              Đăng nhập
                            </Button>
                          </Col>
                          {/* <Col xs="12" className="text-left">
                            <h6 className="note_title">
                              (*) Vui lòng đăng nhập bằng tài khoản email của
                              HDSaison
                            </h6>
                          </Col> */}
                          {isLoginState.loginError && (
                            <Col xs="12" className="text-right">
                              <h6 className="note_title_err">
                                (*) Đăng nhập không thành công
                              </h6>
                            </Col>
                          )}
                        </Row>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoginState: state.LoginReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchLogin: (local, depart, email, password) => {
      dispatch(login(local, depart, email, password));
    }
  };
};

const LoginComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

export default LoginComponent;
