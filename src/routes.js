import React from "react";
const Dashboard = React.lazy(() => import("./views/Dashboard"));

const Users = React.lazy(() => import("./views/Users/Users"));
const User = React.lazy(() => import("./views/Users/User"));

const Management = React.lazy(() => import("./views/Management"));
const Customers = React.lazy(() =>
  import("./views/Management/Customers/Customers")
);
const Administration = React.lazy(() =>
  import("./views/Management/Administration")
);
const Contracts = React.lazy(() => import("./views/Contracts"));
const ContractLists = React.lazy(() =>
  import("./views/Contracts/ContractLists")
);
const ApprovalSchedules = React.lazy(() =>
  import("./views/Contracts/ApprovalSchedules")
);
const CheckProfiles = React.lazy(() =>
  import("./views/Contracts/CheckProfiles")
);
const Profiles = React.lazy(() =>
  import("./views/Configurations/CheckProfiles")
);
const Configurations = React.lazy(() => import("./views/Configurations"));
const Role = React.lazy(() => import("./views/Configurations/Role"));
const RoleEdit = React.lazy(() =>
  import("./views/Configurations/Role/components/RoleEdit")
);
const RoleNew = React.lazy(() =>
  import("./views/Configurations/Role/components/RoleNew")
);

const Config = React.lazy(() => import("./views/Configurations/Config"));
const SmsTemplates = React.lazy(() =>
  import("./views/Configurations/SmsTemplates/SmsTemplates")
);
const SmsTemplatesNewUpdate = React.lazy(() =>
  import("./views/Configurations/SmsTemplates/components/SmsTemplateNewUpdate")
);
const Templates = React.lazy(() =>
  import("./views/Configurations/Templates/Templates")
);
const TemplateNewEdit = React.lazy(() =>
  import("./views/Configurations/Templates/components/TemplateNewEdit")
);

const TypesOfIncentives = React.lazy(() =>
  import("./views/Configurations/TypesOfIncentives")
);
const TypesOfIncentivesNewEdit = React.lazy(() =>
  import(
    "./views/Configurations/TypesOfIncentives/components/TypesOfIncentivesNewEdit"
  )
);
// const NewTypesOfIncentives = React.lazy(() =>
//   import(
//     "./views/Configurations/TypesOfIncentives/components/NewTypesOfIncentives"
//   )
// );
const News = React.lazy(() => import("./views/News/News"));
const NewsCreateForm = React.lazy(() =>
  import("./views/News/components/NewsCreateForm")
);
const NewsEditForm = React.lazy(() =>
  import("./views/News/components/NewsEditForm")
);
const Notification = React.lazy(() =>
  import("./views/Management/Notification")
);
const PreferentialInformation = React.lazy(() =>
  import("./views/PreferentialInformation/PreferentialInformation")
);
const PreferentialCreateForm = React.lazy(() =>
  import(
    "./views/PreferentialInformation/components/PreferentialCreateForm"
  )
);
const PreferentialEditForm = React.lazy(() =>
  import(
    "./views/PreferentialInformation/components/PreferentialEditForm"
  )
);

const Accounts = React.lazy(() => import("./views/Accounts"));
const Logout = React.lazy(() => import("./views/Pages/Logout"));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, name: "Trang chủ" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/management",
    exact: true,
    name: "Management",
    component: Management
  },
  {
    path: "/management/administration",
    name: "Administration",
    component: Administration
  },
  { path: "/management/customer", name: "Quản lý tài khoản", component: Customers },
  { path: "/contracts", exact: true, name: "Hợp đồng", component: ContractLists },
  {
    path: "/contracts/lists",
    name: "Danh sách hợp đồng",
    component: ContractLists
  },
  {
    path: "/contracts/check-profile",
    name: "Kiểm tra hồ sơ",
    component: CheckProfiles
  },
  {
    path: "/contracts/approval-schedule",
    name: "Duyệt hợp đồng",
    component: ApprovalSchedules
  },
  {
    path: "/configurations",
    exact: true,
    name: "Cấu hình",
    component: Config
  },
  { path: "/configurations/role", name: "Phân quyền", component: Role },
  {
    path: "/configurations/edit_role/:id",
    name: "Chỉnh sửa phân quyền",
    component: RoleEdit
  },
  {
    path: "/configurations/new_role",
    name: "Chỉnh sửa phân quyền",
    component: RoleNew
  },
  {
    path: "/configurations/check-profile",
    name: "Kiểm tra hồ sơ",
    component: Profiles
  },
  { path: "/configurations/config", name: "Cấu hình chung", component: Config },
  {
    path: "/configurations/sms-template",
    name: "Nội dung SMS mẫu",
    component: SmsTemplates
  },
  {
    path: "/configurations/sms-template-new",
    name: "Nội dung SMS mẫu",
    component: SmsTemplatesNewUpdate
  },
  {
    path: "/configurations/sms-template-edit/:id",
    name: "Nội dung SMS mẫu",
    component: SmsTemplatesNewUpdate
  },
  {
    path: "/configurations/template",
    name: "Nội dung thông báo mẫu",
    component: Templates
  },
  {
    path: "/configurations/template-new",
    name: "Nội dung thông báo mẫu",
    component: TemplateNewEdit
  },
  {
    path: "/configurations/template-edit/:id",
    name: "Nội dung thông báo mẫu",
    component: TemplateNewEdit
  },
  {
    path: "/configurations/types-of-incentives",
    name: "Loại ưu đãi",
    component: TypesOfIncentives
  },
  {
    path: "/configurations/types-of-incentives-edit/:id",
    name: "Loại ưu đãi",
    component: TypesOfIncentivesNewEdit
  },
  {
    path: "/configurations/types-of-incentives-new",
    name: "Loại ưu đãi",
    component: TypesOfIncentivesNewEdit
  },

  {
    path: "/account",
    name: "Quản lý tài khoản",
    component: Accounts
  },
  
  {
    path: "/news",
    exact: true,
    name: "Tin tức",
    component: News
  },
  {
    path: "/news/create",
    exact: true,
    name: "Tạo tin tức",
    component: NewsCreateForm
  },
  // {
  //   path: "/management/news/:page",
  //   exact: true,
  //   name: "News",
  //   component: News
  // },
  
  {
    path: "/news/edit/:id",
    exact: true,
    name: "Sửa tin tức",
    component: NewsEditForm
  },
  {
    path: "/management/notification",
    name: "Notification",
    component: Notification
  },
  {
    path: "/preferential-information",
    exact: true,
    name: "Thông tin ưu đãi ",
    component: PreferentialInformation
  },
  {
    path: "/preferential-information/create",
    exact: true,
    name: "Tạo thông tin ưu đãi",
    component: PreferentialCreateForm
  },
  {
    path: "/preferential-information/:id",
    exact: true,
    name: "Sửa thông tin ưu đãi",
    component: PreferentialEditForm
  },
 
  
  // { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  // { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  // { path: '/notifications/badges', name: 'Badges', component: Badges },
  // { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: "/users", exact: true, name: "Nhân viên", component: Users },
  { path: "/users/:id", exact: true, name: "User Details", component: User },
  { path: "/logout", name: "Logout", component: Logout }
];

export default routes;
