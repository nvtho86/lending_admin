export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    
    
    {
      name: 'Quản lý nhân viên',
      url: '/users',
      icon: 'icon-people',
    },
    {
      name: 'Quản lý tài khoản',
      url: '/account',
      icon: 'icon-user',
    },
    {
      name: 'Hợp đồng',
      url: '/contracts',
      icon: 'icon-notebook',
      children: [
        {
          name: 'Danh sách hợp đồng',
          url: '/contracts/lists',
          // icon: 'icon-notebook',
        },
        {
          name: 'Kiểm tra hồ sơ',
          url: '/contracts/check-profile',
          // icon: 'icon-notebook',
        },
        {
          name: 'Duyệt phụ lục',
          url: '/contracts/approval-schedule',
          // icon: 'icon-notebook',
        }
      ]
    },

    {
      name: 'Tin Tức',
      url: '/news',
      icon: 'icon-book-open',
    },
    {
      name: 'Thông tin ưu đãi',
      url: '/preferential-information',
      icon: 'icon-tag',
    },
    // {
    //   name: 'Thông báo',
    //   url: '/management/notification',
    //   icon: 'icon-bell',
    // },
    {
      name: 'Cấu hình',
      // url: '/management/configuration',
      icon: 'icon-settings',
      children: [

        {
          name: 'Cấu hình chung',
          url: '/configurations/config',
          // icon: 'icon-settings',
        },
        {
          name: 'Kiểm tra hồ sơ',
          url: '/configurations/check-profile',
          // icon: 'icon-settings',
        },
        {
          name: 'Nội dung SMS mẫu',
          url: '/configurations/sms-template',
          // icon: 'icon-settings',
        },
        {
          name: 'Thông báo mẫu',
          url: '/configurations/template',
          // icon: 'icon-settings',
        },
        {
          name: 'Loại ưu đãi',
          url: '/configurations/types-of-incentives',
          // icon: 'icon-settings',
        },
        {
          name: 'Phân quyền',
          url: '/configurations/role',
          // icon: 'icon-user',
        }
      ]
    },
    {
      name: 'Đăng xuất',
      url: '/logout',
      icon: 'icon-logout',
    },

    
  ],
};
