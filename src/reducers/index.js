import { combineReducers } from "redux";
import userStaff from "./../views/Users/reducers/users.reducer";
// import editUser from './../views/Users/reducers/edit.reducer'
import accounts from "./../views/Accounts/reducers/account.reducer";
import contracts from "./../views/Contracts/ContractLists/reducers/contracts.reducer";
import LoginReducer from "../views/Pages/Login/reducers";
// import list from '../views/Configurations/Role/reducers'
import generalConfig from "../views/Management/Configuration/reducers/config.reducers";
import approvalSchedules from "../views/Contracts/ApprovalSchedules/reducers/approvalshedules.reducer";
// import smsTemplates from "../views/Configurations/SmsTemplates/reducers/smsTemplates.reducer";
// import Templates from "../views/Configurations/Templates/reducers/templates.reducer";
import preferentialData from "./../views/PreferentialInformation/reducers/preferential.reducers";
import configData from "../views/Configurations/Config/reducers/config.reducer";
import newsData from "../views/News/reducers/news.reducer";
import defaultData from "../containers/DefaultLayout/reducers/default.reducer"

const Reducer = combineReducers({
  LoginReducer,
  userStaff,
  accounts,
  // smsTemplates,
  // Templates,
  contracts,
  approvalSchedules,
  //   list,
  generalConfig,
  preferentialData,
  configData,
  newsData,
  defaultData
});

export default Reducer;
